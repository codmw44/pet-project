## Overview
This is my home project where I test new technologies and approaches in my free time.
There are some links that fly off, and when you put them back, no changes appear. In order not to do extra work, I will send the whole project together with the Library

## General:

- PetServer is a .net core server using magiconion (grpc+messagepack), swagger, opentelemetry + prometheus, mongo
- PetServer.Tests - server stub tests to integrate into pipeline tests
- .helm - folder for helm templates for deployment to k8s.
- CI/CD is configured in .gitlab-ci.yml and werf.yaml. Therefore, for the build machine, a gitlab runner is used on an external server in the cloud, where docker containers are built via werf and loaded into the gitlab registry and then deployed to kubernetes, each branch in a separate environment
- PetProject-Client - unity client using Entitas, Zenject, Addressables, Odin Inspector etc
- \PetProject.Client\Assets\Scripts\Defenitions\Defenitions.csproj - for messagePack, shared models and interfaces are needed that are used for client-server interaction
- client EntryPoint - PetPro
ject.Client\Assets\Scripts\EntryPoint.cs (spawns the ProjectContext prefab from /Resources)

## Where it is most interesting to look:

PetProject.Client\Assets\Scripts\Ecs - there is the EC- S system itself, its components, generated code and systems
- EcsSerializationController is a system that saves all entities and the value of their components by the Serializable attribute. This allows you not to write a model and not change it every time the data changes. This attribute marks the most necessary components so that other systems themselves add visual and other components
- PetProject.Client\Assets\Scripts\GameStates - the architecture is based on states as separate and unrelated units
- PetProject.Client\Assets\Scripts\AssetProvider - an interface for loading assets, in particular loading and caching them through addressables
- PetProject.Client\Assets\Scripts\Controllers\Finance - an interface for working with currencies
- PetProject.Client\Assets\Scripts\Commands - a command patt
ern was chosen for client-server interaction