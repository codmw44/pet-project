using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Grpc.Core;
using MagicOnion.Client;
using Models.Price;
using Models.Rewards;
using NUnit.Framework;
using NBomber.Contracts;
using NBomber.CSharp;
using PetServer.Balancer.Models.Balance;
using PetServer.Services;
using Tests;

namespace PetServer.Tests.LoadTesting
{
	[TestFixture]
	public class LoadTest
	{
		private int _num = 0;

		private Scenario BuildScenario()
		{
			var step = Step.Create("auth", async context =>
			{
				try
				{
					_num++;

					var name = _num.ToString();

					var channel = await GetChannel();
					context.Data.Add("channel", channel);

					// var authService = MagicOnionClient.Create<IAuthService>(channel);
					//
					// await authService.ProcessGuestAuth(name, new ClientSpecModel() {ClientVersion = "1.0"});
					//
					// var guestAuth = await authService.ProcessGuestAuth(name, new ClientSpecModel() {ClientVersion = "1.0"});

					// context.Data.Add("session", guestAuth.SessionToken);
				}
				catch (Exception e)
				{
					return Response.Fail(e);
				}

				return Response.Ok(sizeBytes: 1024);
			});

			var step2 = Step.Create("reward", async context =>
			{
				try
				{
					var channel = context.Data["channel"] as Channel;
					// var sessionToken = context.Data["session"] as SessionToken;
					var financeService = MagicOnionClient.Create<IFinanceService>(channel);

					// await financeService.ProcessReward(sessionToken, new HardCurrencyReward(5));
					// await financeService.ProcessReward(sessionToken, new SoftCurrencyReward(34));
				}
				catch (Exception e)
				{
					return Response.Fail(e);
				}

				return Response.Ok(sizeBytes: 1024);
			}, 10);

			var step3 = Step.Create("price", async context =>
			{
				try
				{
					var channel = context.Data["channel"] as Channel;
					// var sessionToken = context.Data["session"] as SessionToken;
					// var financeService = MagicOnionClient.Create<IFinanceService>(channel);
					//
					// await financeService.ProcessPrice(sessionToken, new HardCurrencyPrice(2));
					// await financeService.ProcessPrice(sessionToken, new SoftCurrencyPrice(10));
				}
				catch (Exception e)
				{
					return Response.Fail(e);
				}

				return Response.Ok(sizeBytes: 1024);
			}, 10);

			return ScenarioBuilder.CreateScenario("magiconion load test", new[]
			{
				step,
				step2,
				step3,
				step2
			});
		}

		// [Test]
		public void Test()
		{
			var scenario = BuildScenario()
				.WithOutWarmUp()
				.WithLoadSimulations(new[] {Simulation.KeepConcurrentScenarios(copiesCount: 10, during: TimeSpan.FromSeconds(60))});

			var nodeStats = NBomberRunner.RegisterScenarios(new[] {scenario}).RunTest();
			nodeStats.Should().NotBeNull();
		}

		private static readonly List<Channel> _channels = new List<Channel>(128);

		private static int index;

		private static async Task<Channel> GetChannel()
		{
			if (_channels.Count >= 60)
			{
				index++;
				return _channels[index % _channels.Count];
			}

			var channel = new Channel(UnitTestSetUp.Host, UnitTestSetUp.ServerCrd);
			await channel.ConnectAsync(DateTime.UtcNow.AddSeconds(12));

			_channels.Add(channel);
			return channel;
		}
	}
}