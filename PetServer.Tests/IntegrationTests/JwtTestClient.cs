using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Defenitions.Models.Auth;
using Defenitions.Services;
using Defenitions.StreamingHubs;
using Grpc.Core;
using MagicOnion.Client;
using MongoDB.Driver;
using Moq;
using NUnit.Framework;
using PetServer.Balancer.Models.Balance;
using PetServer.Controllers.Database.Infrastructure;
using PetServer.Models.Users;
using PetServer.Services;
using Tests;

namespace PetServer.Tests.IntegrationTests
{
	[TestFixture]
	public class JwtTestClient : ITimerHubReceiver
	{
		[Test]
		public void TestDB()
		{
			var mockRepo = new Mock<IRepository<UserModel>>();
			var mockDbContext = new Mock<IDbContext>();
			mockDbContext.Setup(db => db.Users).Returns(mockRepo.Object);

			Expression<Func<UserModel, bool>> expression = model=> model.FinanceModel.Hard==300;
			mockDbContext.Object.Users.CountAsync(expression);

			mockRepo.Verify(m => m.CountAsync(expression));
		}

		[Test]
		public async Task MainCore()
		{
			var channel = new Channel(UnitTestSetUp.Host, UnitTestSetUp.ServerCrd);

			var auth = new AttachedCustomSignInModel() {Id = "jwt test id", SignInType = SignInEnum.GameCenterId};

			var clientFilters = new[] {new WithAuthenticationFilter("Device id mac test jwr", channel, null),};

			// 1. Call an API without an authentication token.
			{
				var accountClient = MagicOnionClient.Create<IAccountService>(channel);
				var user = await accountClient.GetCurrentUserNameAsync();
				Console.WriteLine($@"[IAccountService.GetCurrentUserNameAsync] Current User: UserId={user.UserId}; IsAuthenticated={user.IsAuthenticated
					}; Name={user.Name}");
				try
				{
					var greeterClientAnon = MagicOnionClient.Create<IPingService>(channel);
					Console.WriteLine($"[IGreeterService.HelloAsync] {await greeterClientAnon.PingConnection()}");
				}
				catch (RpcException e)
				{
					Console.WriteLine($"[IGreeterService.HelloAsync] Exception: {e.Message}");
				}
			}

			// 4. Get the user information using the authentication token.
			{
				var accountClient = MagicOnionClient.Create<IAccountService>(channel, clientFilters);
				var user = await accountClient.GetCurrentUserNameAsync();
				Console.WriteLine($@"[IAccountService.GetCurrentUserNameAsync] Current User: UserId={user.UserId}; IsAuthenticated={user.IsAuthenticated
					}; Name={user.Name}");

				// 5. Call an API with the authentication token.
				var greeterClient = MagicOnionClient.Create<IPingService>(channel, clientFilters);
				Console.WriteLine($"[IGreeterService.HelloAsync] {await greeterClient.PingConnection()}");
			}
			// 5. Call StreamingHub with authentication
			{
				var timerHubClient = StreamingHubClient.Connect<ITimerHub, ITimerHubReceiver>(channel, this,
					option: new CallOptions().WithHeaders(new Metadata() {{"auth-token-bin", AuthenticationTokenStorage.Current.Token}}));
				await timerHubClient.SetAsync(TimeSpan.FromSeconds(5));
				// await Task.Yield(); // NOTE: Release the gRPC's worker thread here.
			}
			// 6. Insufficient privilege (The current user is not in administrators role).
			{
				var accountClient = MagicOnionClient.Create<IAccountService>(channel, clientFilters);
				try
				{
					var res = await accountClient.DangerousOperationAsync();
					Console.WriteLine("[Dangerous operation] " + res);
				}
				catch (Exception e)
				{
					Console.WriteLine($"[IAccountService.DangerousOperationAsync] Exception: {e.Message}");
				}
			}

			// 7. Refresh the token before calling an API.
			{
				await Task.Delay(1000 * 6); // The server is configured a token expiration set to 5 seconds.
				var greeterClient = MagicOnionClient.Create<IPingService>(channel, clientFilters);
				Console.WriteLine($"[IGreeterService.HelloAsync] {await greeterClient.PingConnection()}");
			}
		}

		void ITimerHubReceiver.OnTick(string message)
		{
			Console.WriteLine($"[ITimerHubReceiver.OnTick] {message}");
		}
	}

	// NOTE: This implementation is for demonstration purpose only. DO NOT USE THIS IN PRODUCTION.
	class WithAuthenticationFilter : IClientFilter
	{
		private readonly string _deviceId;
		private readonly AttachedCustomSignInModel? _customSignInRequestModel;
		private readonly Channel _channel;

		public WithAuthenticationFilter(string deviceId, Channel channel, AttachedCustomSignInModel? customSignInRequestModel)
		{
			_deviceId = deviceId;
			_customSignInRequestModel = customSignInRequestModel;
			_channel = channel ?? throw new ArgumentNullException(nameof(channel));
		}

		public async ValueTask<ResponseContext> SendAsync(RequestContext context, Func<RequestContext, ValueTask<ResponseContext>> next)
		{
			if (AuthenticationTokenStorage.Current.IsExpired)
			{
				Console.WriteLine($@"[WithAuthenticationFilter/IAccountService.SignInAsync] Try signing in as '{_customSignInRequestModel}'... ({
					(AuthenticationTokenStorage.Current.Token == null ? "FirstTime" : "RefreshToken")})");

				var client = MagicOnionClient.Create<IAccountService>(_channel);
				var signInRequest = new SignInRequestModel
				{
					DeviceId = _deviceId,
					ClientSpecModel = new ClientSpecModel {ClientVersion = "1.0"},
					CustomSignInRequestModel = _customSignInRequestModel
				};
				var authResult = await client.SignInAsync(signInRequest);
				if (authResult.HasError)
				{
					throw new Exception("Failed to sign-in on the server. " + authResult.ErrorEnum);
				}

				Console.WriteLine($@"[WithAuthenticationFilter/IAccountService.SignInAsync] User authenticated as {authResult.Name} (UserId:{authResult.UserId
					})");

				AuthenticationTokenStorage.Current.Update(authResult.Token,
					authResult.Expiration); // NOTE: You can also read the token expiration date from JWT.

				context.CallOptions.Headers.Remove(new Metadata.Entry("auth-token-bin", Array.Empty<byte>()));
			}

			if (!context.CallOptions.Headers.Contains(new Metadata.Entry("auth-token-bin", Array.Empty<byte>())))
			{
				context.CallOptions.Headers.Add("auth-token-bin", AuthenticationTokenStorage.Current.Token);
			}

			return await next(context);
		}
	}

	// When the authentication filter acquires an authentication token, the token is stored in somewhere. (e.g. In-memory, JSON, PlayerPrefs, etc ...)
	// The token may be used repeatedly by multiple clients (MagicOnionClient or StreamingHubClient).
	class AuthenticationTokenStorage
	{
		public static AuthenticationTokenStorage Current { get; } = new AuthenticationTokenStorage();

		private readonly object _syncObject = new object();

		public byte[] Token { get; private set; }
		public DateTimeOffset Expiration { get; private set; }

		public bool IsExpired => Token == null || Expiration < DateTimeOffset.Now;

		public void Update(byte[] token, DateTimeOffset expiration)
		{
			lock (_syncObject)
			{
				Token = token ?? throw new ArgumentNullException(nameof(token));
				Expiration = expiration;
			}
		}
	}
}