using System;
using System.Threading.Tasks;
using FluentAssertions;
using Grpc.Core;
using MagicOnion.Client;
using Models.Price;
using Models.Rewards;
using NUnit.Framework;
using PetServer.Balancer.Models.Balance;
using PetServer.Defenitions.Models.Response;
using PetServer.Models.Users;
using PetServer.Services;

namespace Tests
{
	[TestFixture]
	public class TestClient
	{
		private static string TestDeviceId = "UnitTestUser";

		private async Task<Channel> CreateChannel()
		{
			var channel = new Channel(UnitTestSetUp.Host, UnitTestSetUp.ServerCrd);
			await channel.ConnectAsync(DateTime.UtcNow.AddSeconds(UnitTestSetUp.ConnectionDeadLineSec));

			channel.State.Should().NotBe(ChannelState.Shutdown).And.NotBe(ChannelState.TransientFailure);
			return channel;
		}

		private async Task<FinanceModel> GetFinanceData()
		{
			var channel = await CreateChannel();
			var financeService = MagicOnionClient.Create<IFinanceService>(channel);
			var financeModelResponse = await financeService.GetFinanceModel().ResponseAsync;
			financeModelResponse.ErrorEnum.Should().Be(ResponseErrorEnum.None);
			financeModelResponse.HasError.Should().BeFalse();
			financeModelResponse.FinanceModel.Should().NotBeNull();
			return financeModelResponse.FinanceModel;
		}

		[Test, Order(0), MaxTime(2000)]
		public void TestCreateChannel()
		{
			// CreateChannel().Wait();
		}

		[Test, Order(1), MaxTime(2000)]
		public void TestUserAuth()
		{
			// GetSessionToken();
		}

		[Test, Order(2), MaxTime(2000)]
		public void TestPriceProcess()
		{
			return;
			// var channel = CreateChannel().Result;
			// var sessionToken = GetSessionToken();
			// var financeService = MagicOnionClient.Create<IFinanceService>(channel);
			// financeService.Should().NotBeNull();
			// var financeDataBefore = GetFinanceData(sessionToken).Result;
			//
			// var resultBuy = financeService.ProcessPrice(sessionToken, new HardCurrencyPrice(5))
			// 	.ResponseAsync.Result;
			//
			// resultBuy.Should().NotBeNull();
			// resultBuy.IsSucceed.Should().BeTrue();
			// resultBuy.HasError.Should().BeFalse();
			// var financeDataAfter = GetFinanceData(sessionToken).Result;
			// (financeDataBefore.Hard - 5).Should().Be(financeDataAfter.Hard);
			// financeDataBefore.Soft.Should().Be(financeDataAfter.Soft);
		}

		[Test, Order(3), MaxTime(2000)]
		public void TestRewardProcess()
		{
			return;
			
			// var channel = CreateChannel().Result;
			// var sessionToken = GetSessionToken();
			// var financeService = MagicOnionClient.Create<IFinanceService>(channel);
			// financeService.Should().NotBeNull();
			// var financeDataBefore = GetFinanceData(sessionToken).Result;
			//
			// var resultPrice = financeService.ProcessReward(sessionToken, new SoftCurrencyReward(3))
			// 	.ResponseAsync.Result;
			//
			// resultPrice.Should().NotBeNull();
			// resultPrice.IsSucceed.Should().BeTrue();
			// resultPrice.HasError.Should().BeFalse();
			// var financeDataAfter = GetFinanceData(sessionToken).Result;
			// (financeDataBefore.Soft + 3).Should().Be(financeDataAfter.Soft);
			// financeDataBefore.Hard.Should().Be(financeDataAfter.Hard);
		}

		[Test, Order(4), MaxTime(2000)]
		public void TestDeleteExistUserAuth()
		{
			return;
			
			// var channel = CreateChannel().Result;
			// var userId = GetSessionToken();
			// var authService = MagicOnionClient.Create<IAuthService>(channel);
			// authService.Should().NotBeNull();
			// var resultRemoved = authService.RemoveExistUser(userId).ResponseAsync.Result;
			// resultRemoved.Should().BeTrue();
		}

		[Test, MaxTime(2000)]
		public void TestCleanUpSessions()
		{
			return;
			
			// var channel = CreateChannel().Result;
			// var sessionsService = MagicOnionClient.Create<ISessionsService>(channel);
			// sessionsService.Should().NotBeNull();
			// var resultRemoved = sessionsService.RemoveTimeOutSessions().ResponseAsync.Result;
			// resultRemoved.Should().NotBeNull();
			// resultRemoved.HasError.Should().BeFalse();
		}
	}
}