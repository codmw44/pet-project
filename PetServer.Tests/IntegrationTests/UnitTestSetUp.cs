using System;
using System.Text;
using Grpc.Core;
using NUnit.Framework;

namespace Tests
{
	[SetUpFixture]
	public class UnitTestSetUp
	{
		public static readonly SslCredentials ServerCrd = new SslCredentials(Encoding.Default.GetString(Convert.FromBase64String(tls)));

		private const string tls =
			"LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSURXRENDQWtBQ0NRREhYUEk1WnhBcUJ6QU5CZ2txaGtpRzl3MEJBUVVGQURCdU1Rc3dDUVlEVlFRR0V3SlMKVlRFUE1BMEdBMVVFQ0F3R1RXOXpZMjkzTVE4d0RRWURWUVFIREFaTmIzTmpiM2N4RXpBUkJnTlZCQW9NQ2xCbApkRkJ5YjJwbFkzUXhEREFLQmdOVkJBc01BMFJsZGpFYU1CZ0dBMVVFQXd3UktpNWpiMlJ0ZHpRMFpHVjJMbk5wCmRHVXdIaGNOTWpBd05ERXlNVGt3TkRReldoY05OREF3TkRBM01Ua3dORFF6V2pCdU1Rc3dDUVlEVlFRR0V3SlMKVlRFUE1BMEdBMVVFQ0F3R1RXOXpZMjkzTVE4d0RRWURWUVFIREFaTmIzTmpiM2N4RXpBUkJnTlZCQW9NQ2xCbApkRkJ5YjJwbFkzUXhEREFLQmdOVkJBc01BMFJsZGpFYU1CZ0dBMVVFQXd3UktpNWpiMlJ0ZHpRMFpHVjJMbk5wCmRHVXdnZ0VpTUEwR0NTcUdTSWIzRFFFQkFRVUFBNElCRHdBd2dnRUtBb0lCQVFESVFac3JJSUF2MlhNRTlNNzQKSVZzT0dYMG0zcFZJVlVLYU9YRzgyQTBuS2p0TEZzSk9nTmJHUEJtTzAyWGNYQ2RFNG4zWTkwamVqUGhkYzdDKwpPYUhMSGp1OWNSRDVrcWhCZHJ6bEt4TXBMSUlxWmtNN1BRUEJmWHl3Q3YrKzZ5MkkxUm1uSkZ6Q1BqMGtQZm05CjRWeWQ1emxRT1F4dWF0SHhXenpqZGlSdnE5Tjhsdjc3TVhIRVR6a0QwUFBjU1Z6ak1UR1N0ZWVVYXNnYzJrbXIKdUh4blpTUk52NDBwT2RmS2gwMmx3WHp2U09QL1Ivc0k2OHlhYWxaWVRCY090S2MyNkRVT213eXJjdlZQenlzKwp2TXgwa1c1bW0vMVppUC92MTBzWWlmQXd2VnF6NmZCY1VEUmR3cDFVc1VkNUg4THlsckNZbVhDbU5oNzBFUEd0Cis1V0hBZ01CQUFFd0RRWUpLb1pJaHZjTkFRRUZCUUFEZ2dFQkFHZEx2VDRVbzVTVUdkR1kyTXl0bVhIU0VOWk8Kek9hRzRtU3N4bkx3b2xlcjdLOFpESmhTMU50aXZOVUI2WGhDbC9NOXg1U29yM1JLTGVyREFRS3J0dmdXdVlrSAprVkhzK0tGS3FEZ2hCcWJsaW40ZDMyWDFoZm5JQVF6YWdnRjZRM2xKS2pZYWJwWGp0VU9OaWxiRmFUbnpIbnJYCjVFVWQ1RmtwR25wRVdjb0xjL3lvMm42VkNYSkFlT25HdEw2UGlMaUF3enlnL3NJcHk0L3hVaVRobkl5bUJJeW0KQlVTT1QxVXdJMTVDUEdSa1pBbWxsMFhnWGtDY3RGRzAxdVZJaEl4ME9TTkFrUFZJNC9mOW1TU0k4c2xQZjJJTwpJeGNqVkFkeHRDMXhLckltdUJFTXBaVTZVSEFWTjhkMHI2eFJZZmhRR09HWXN2bURtYW12NnM4MGlYVT0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQ==";

		public const string Host = "feature-add-jwt.codmw44dev.site";

		public const float ConnectionDeadLineSec = 2f;

		[OneTimeSetUp]
		public void Setup()
		{
		}
	}
}