using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Defenitions.Models.Auth;
using FluentAssertions;
using MongoDB.Driver;
using NUnit.Framework;
using PetServer.Models.Migration;
using PetServer.Models.Users;

namespace PetServer.Tests.MockDatabaseUtility
{
	[TestFixture]
	public class RepositoryMocTests
	{
		private static List<UserModel[]> Data = new List<UserModel[]>
		{
			new[]
			{
				new UserModel
				{
					Id = "existing id",
					AuthModel = new AuthModel {DeviceId = "dxz", AttachedSignIns = new[] {new AttachedCustomSignInModel() { },}},
					FinanceModel = new FinanceModel {Soft = 5000, Hard = 50},
					MigrationModel = new MigrationModel {MigrationVersion = 0}
				},
				new UserModel
				{
					Id = string.Empty,
					AuthModel = new AuthModel {DeviceId = "dxz", AttachedSignIns = new AttachedCustomSignInModel[0]},
					FinanceModel = new FinanceModel {Soft = 5000, Hard = 50},
					MigrationModel = new MigrationModel {MigrationVersion = 0}
				}
			}
		};

		[TestCaseSource(nameof(Data))]
		public async Task TestCountAsync(UserModel[] models)
		{
			var repository = new MockInMemoryRepository<UserModel>(string.Empty, models);

			var count = await repository.CountAsync(model => !string.IsNullOrEmpty(model.Id));
			count.Should().Be(models.Count(model => !string.IsNullOrEmpty(model.Id)));

			count = await repository.CountAsync(model => model.AuthModel.DeviceId.Length == 0);
			count.Should().Be(models.Count(model => model.AuthModel.DeviceId.Length == 0));
		}

		[TestCaseSource(nameof(Data))]
		public async Task TestFindFirstAsync(UserModel[] models)
		{
			var repository = new MockInMemoryRepository<UserModel>(string.Empty, models);

			var foundItem = await repository.FindFirstOrDefaultAsync(model => model.Id == string.Empty);

			foundItem.Should().NotBeNull();
			foundItem.Should().Be(models.First(model => model.Id == string.Empty));
		}

		[TestCaseSource(nameof(Data))]
		public async Task TestFindFirstAsync_NullCheck(UserModel[] models)
		{
			var repository = new MockInMemoryRepository<UserModel>(string.Empty, models);

			var foundItem = await repository.FindFirstOrDefaultAsync(model => model.Id == "Not exist id");

			foundItem.Should().BeNull();
		}

		[TestCaseSource(nameof(Data))]
		public async Task FindOneAndReplaceAsync(UserModel[] models)
		{
			var repository = new MockInMemoryRepository<UserModel>(string.Empty, models);

			var replacement = new UserModel();
			var foundItem = await repository.FindOneAndReplaceAsync(model => model.Id == string.Empty, replacement);

			foundItem.Should().NotBeNull();
			foundItem.Should().Be(replacement);

			var checkItem = await repository.FindFirstOrDefaultAsync(model => model == replacement);

			checkItem.Should().Be(replacement);
		}

		[TestCaseSource(nameof(Data))]
		public async Task InsertOneAsync(UserModel[] models)
		{
			var repository = new MockInMemoryRepository<UserModel>(string.Empty, models);

			var insert = new UserModel();
			await repository.InsertOneAsync(insert);

			var checkItem = await repository.FindFirstOrDefaultAsync(model => model == insert);

			checkItem.Should().Be(insert);
		}

		[TestCaseSource(nameof(Data))]
		public async Task UpdateOneAsync(UserModel[] models)
		{
			var repository = new MockInMemoryRepository<UserModel>(string.Empty, models);

			var replacement = new FinanceModel {Hard = 1, Soft = 10};

			var beforeCheckItem = await repository.FindFirstOrDefaultAsync(model => model.Id == "existing id");
			beforeCheckItem.Should().NotBeNull();
			beforeCheckItem.FinanceModel.Should().NotBe(replacement);

			await repository.UpdateOneAsync(model => model.Id == "existing id", Builders<UserModel>.Update.Set(model => model.FinanceModel, replacement));

			var checkItem = await repository.FindFirstOrDefaultAsync(model => model.Id == "existing id");
			checkItem.Should().NotBeNull();
			checkItem.FinanceModel.Should().Be(replacement);
		}

		[TestCaseSource(nameof(Data)), Ignore("It dont implement it now. TODO this")]
		public async Task UpdateOneAsync_SubFields(UserModel[] models)
		{
			//todo improve it on mock
			var repository = new MockInMemoryRepository<UserModel>(string.Empty, models);
			var replacement = new AttachedCustomSignInModel[3];

			var beforeCheckItem = await repository.FindFirstOrDefaultAsync(model => model.Id == "existing id");
			beforeCheckItem.Should().NotBeNull();
			beforeCheckItem.AuthModel.AttachedSignIns.Length.Should().NotBe(replacement.Length);

			await repository.UpdateOneAsync(model => model.Id == "existing id",
				Builders<UserModel>.Update.Set(model => model.AuthModel.AttachedSignIns, replacement));

			var checkItem = await repository.FindFirstOrDefaultAsync(model => model.Id == "existing id");
			checkItem.Should().NotBeNull();
			checkItem.AuthModel.AttachedSignIns.Length.Should().Be(replacement.Length);
		}

		[TestCaseSource(nameof(Data))]
		public async Task UpdateOneAsync_Null(UserModel[] models)
		{
			var repository = new MockInMemoryRepository<UserModel>(string.Empty, models);

			var replacement = new FinanceModel {Hard = 1, Soft = 10};

			var beforeCheckItem = await repository.FindFirstOrDefaultAsync(model => model.Id == "Not exist id");
			beforeCheckItem.Should().BeNull();

			try
			{
				await repository.UpdateOneAsync(model => model.Id == "Not exist id", Builders<UserModel>.Update.Set(model => model.FinanceModel, replacement));

				throw new Exception("Expect exception of null find filter");
			}
			catch (Exception e)
			{
				// ignored
			}
		}
	}
}