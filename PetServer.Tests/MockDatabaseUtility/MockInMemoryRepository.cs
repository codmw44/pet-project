using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using MongoDB.Driver;
using PetServer.Controllers.Database.Infrastructure;

namespace PetServer.Tests.MockDatabaseUtility
{
	public class MockInMemoryRepository<T> : IRepository<T>
	{
		public string CollectionName { get; }

		private readonly List<T> _data;

		public MockInMemoryRepository(string collectionName, IEnumerable<T> data)
		{
			_data = data.ToList();
			CollectionName = collectionName;
		}

		public Task<long> CountAsync(Expression<Func<T, bool>> filter)
		{
			return Task.FromResult(_data.Where(filter.Compile()).LongCount());
		}

		public Task<T> FindFirstOrDefaultAsync(Expression<Func<T, bool>> expression)
		{
			var func = expression.Compile();
			return Task.FromResult(_data.Where(func).FirstOrDefault());
		}

		public Task<T> FindOneAndReplaceAsync(Expression<Func<T, bool>> filter, T replacement)
		{
			var foundedItem = _data.Where(filter.Compile()).FirstOrDefault();
			if (foundedItem == null)
			{
				InsertOneAsync(replacement);
			}
			else
			{
				_data[_data.IndexOf(foundedItem)] = replacement;
			}

			return Task.FromResult(replacement);
		}

		public Task InsertOneAsync(T document)
		{
			_data.Add(document);
			return Task.CompletedTask;
		}

		public async Task UpdateOneAsync(Expression<Func<T, bool>> filter, UpdateDefinition<T> updateDefinition)
		{
			var item = await FindFirstOrDefaultAsync(filter);
			if (item == null)
			{
				throw new NoNullAllowedException("Dont found document by filter");
			}

			//OperatorUpdateDefinition is internal class and not support to typeof(OperatorUpdateDefinition)
			if (updateDefinition.GetType().FullName!.Contains("MongoDB.Driver.OperatorUpdateDefinition"))
			{
				object expressionField = updateDefinition.GetType()
						.GetField("_field", BindingFlags.NonPublic | BindingFlags.Instance)!
					.GetValue(updateDefinition)!;
				object valueToReplace = updateDefinition.GetType()
						.GetField("_value", BindingFlags.NonPublic | BindingFlags.Instance)!
					.GetValue(updateDefinition)!;

				var replacedNameForItem = GetReplacedNameForItem(expressionField);

				item.GetType().GetField(replacedNameForItem)?.SetValue(item, valueToReplace);
			}
			else
			{
				throw new NotSupportedException("This type updateDefinition dont support in mock. Please write your implementation for " +
					updateDefinition.GetType());
			}
		}

		private string GetReplacedNameForItem(object expressionField)
		{
			if (expressionField.GetType().FullName!.Contains("MongoDB.Driver.ExpressionFieldDefinition"))
			{
				dynamic expressionObj =
					expressionField.GetType().GetField("_expression", BindingFlags.NonPublic | BindingFlags.Instance)!.GetValue(expressionField)!;

				dynamic fieldNameFromExpression =
					expressionObj.Body.GetType().GetField("_field", BindingFlags.NonPublic | BindingFlags.Instance)!.GetValue(expressionObj.Body)!;

				return fieldNameFromExpression.Name;
			}

			throw new NotSupportedException("This type FieldDefinition dont support in mock. Please write your implementation for " +
				expressionField.GetType());
		}
	}
}