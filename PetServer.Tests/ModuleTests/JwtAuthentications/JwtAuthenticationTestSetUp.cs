using System;
using System.Collections.Generic;
using LitJWT;
using LitJWT.Algorithms;
using MagicOnion.Server.Authentication.Jwt;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using PetServer.Controllers.Authentication;

namespace PetServer.Tests.ModuleTests.JwtAuthentications
{
	public class JwtAuthenticationTestSetUp
	{
		public const float JwtExpirationInSec = 5;
		public const string JwtKey = "/Z8OkdguxFFbaxOIG1q+V9H4ujzMKg1n9gcAYB+x4QvhF87XcD8sQA4VsdwqKVuCmVrXWxReh/6dmVXrjQoo9Q==";

		public static IEnumerable<IJwtAuthenticationProvider> GetJwtAuthenticationProviders()
		{
			var results = new List<IJwtAuthenticationProvider>(4);

			var preSharedKey = Convert.FromBase64String(JwtKey);

			var jwtAlgorithm = new HS512Algorithm(preSharedKey);

			var jwtOptions = new JwtAuthenticationOptions()
			{
				Encoder = new JwtEncoder(jwtAlgorithm),
				Decoder = new JwtDecoder(new JwtAlgorithmResolver(jwtAlgorithm)),
				Expire = TimeSpan.FromSeconds(JwtExpirationInSec),
			};

			results.Add(new CustomJwtAuthenticationProvider(new OptionsWrapper<JwtAuthenticationOptions>(jwtOptions)));

			return results;
		}

		public static IEnumerable<TestCaseData> GetJwtProviderAndPayload()
		{
			var payloads = new[]
			{
				new CustomJwtAuthenticationPayload() {UserId = "userId", Name = "displayName"},
				new CustomJwtAuthenticationPayload() {UserId = "userId", Name = "displayName", Role = "TestRole"},
				new CustomJwtAuthenticationPayload() {UserId = null, Name = null}
			};

			foreach (var jwtAuthenticationProvider in GetJwtAuthenticationProviders())
			{
				foreach (var payload in payloads)
				{
					yield return new TestCaseData(jwtAuthenticationProvider, payload);
				}
			}
		}
	}
}