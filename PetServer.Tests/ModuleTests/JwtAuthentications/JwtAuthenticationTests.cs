using System;
using FluentAssertions;
using LitJWT;
using MagicOnion.Server.Authentication.Jwt;
using NUnit.Framework;
using PetServer.Controllers.Authentication;

namespace PetServer.Tests.ModuleTests.JwtAuthentications
{
	[TestFixture]
	public class JwtAuthenticationTests
	{
		[TestCaseSource(typeof(JwtAuthenticationTestSetUp), nameof(JwtAuthenticationTestSetUp.GetJwtProviderAndPayload))]
		public void TestCreateTokenFromPayload(IJwtAuthenticationProvider jwtAuthenticationProvider, CustomJwtAuthenticationPayload payload)
		{
			var invokeTime = DateTime.Now;
			var encodedPayload = jwtAuthenticationProvider.CreateTokenFromPayload(payload);
			encodedPayload.Should().NotBeNull();

			encodedPayload.Expiration.Should().BeCloseTo(invokeTime.AddSeconds(JwtAuthenticationTestSetUp.JwtExpirationInSec));

			encodedPayload.Token.Should().NotBeNull();
			encodedPayload.Token.Length.Should().BeGreaterThan(0);
		}

		[TestCaseSource(typeof(JwtAuthenticationTestSetUp), nameof(JwtAuthenticationTestSetUp.GetJwtProviderAndPayload))]
		public void TestCreatePrincipalFromToken(IJwtAuthenticationProvider jwtAuthenticationProvider, CustomJwtAuthenticationPayload payload)
		{
			var token = jwtAuthenticationProvider.CreateTokenFromPayload(payload).Token;

			var decodeResult = jwtAuthenticationProvider.TryCreatePrincipalFromToken(token, out var principal);
			decodeResult.Should().NotBeNull();
			decodeResult.Should().Be(DecodeResult.Success);

			principal.Should().NotBeNull();
			principal!.Identity.Should().NotBeNull();

			principal!.Identity!.Name.Should().Be(payload.Name);
			principal.Identity.IsAuthenticated.Should().Be(true);
		}
	}
}