using System;
using System.Collections.Generic;
using System.Linq;
using Defenitions.Models.Auth;
using Defenitions.Services;
using LitJWT;
using LitJWT.Algorithms;
using MagicOnion;
using MagicOnion.Server.Authentication.Jwt;
using Microsoft.Extensions.Options;
using Moq;
using PetServer.Balancer.Models.Balance;
using PetServer.Controllers.Authentication;
using PetServer.Controllers.Database.Infrastructure;
using PetServer.Defenitions.Models.Response;
using PetServer.Models.Migration;
using PetServer.Models.Users;
using PetServer.Services;
using PetServer.Tests.MockDatabaseUtility;

namespace PetServer.Tests.ModuleTests.AccountService
{
	public class AccountServiceTestsSetUp
	{
		public const float JwtExpirationInSec = 5;
		public const string JwtKey = "/Z8OkdguxFFbaxOIG1q+V9H4ujzMKg1n9gcAYB+x4QvhF87XcD8sQA4VsdwqKVuCmVrXWxReh/6dmVXrjQoo9Q==";

		public static List<UserModel[]> Data = new List<UserModel[]>
		{
			new[]
			{
				new UserModel
				{
					Id = "1",
					AuthModel = new AuthModel {DeviceId = "dxz", AttachedSignIns = new AttachedCustomSignInModel[0]},
					FinanceModel = new FinanceModel {Soft = 5000, Hard = 50},
					MigrationModel = new MigrationModel {MigrationVersion = 0}
				},
				new UserModel
				{
					Id = "2",
					AuthModel = new AuthModel {DeviceId = "existingDeviceId", AttachedSignIns = new AttachedCustomSignInModel[0]},
					FinanceModel = new FinanceModel {Soft = 532, Hard = 5},
					MigrationModel = new MigrationModel {MigrationVersion = 0}
				},
				new UserModel
				{
					Id = "3",
					AuthModel =
						new AuthModel
						{
							DeviceId = "existing id w custom",
							AttachedSignIns = new[] {new AttachedCustomSignInModel {Id = "customFbId", SignInType = SignInEnum.FacebookId},}
						},
					FinanceModel = new FinanceModel {Soft = 853, Hard = 70},
					MigrationModel = new MigrationModel {MigrationVersion = 0}
				},
				new UserModel
				{
					Id = "4",
					AuthModel =
						new AuthModel
						{
							DeviceId = "existing id w game center and fb",
							AttachedSignIns = new[]
							{
								new AttachedCustomSignInModel {Id = "customFbId2", SignInType = SignInEnum.FacebookId},
								new AttachedCustomSignInModel {Id = "customGameCenterId2", SignInType = SignInEnum.GameCenterId},
							}
						},
					FinanceModel = new FinanceModel {Soft = 23223, Hard = 0},
					MigrationModel = new MigrationModel {MigrationVersion = 0}
				}
			}
		};

		public static IEnumerable<SignInRequestModel> SignInRequestModels = new[]
		{
			new SignInRequestModel {ClientSpecModel = new ClientSpecModel {ClientVersion = "1.0", IsDevelopmentBuild = false}, DeviceId = "existingDeviceId"},
			new SignInRequestModel {ClientSpecModel = new ClientSpecModel {ClientVersion = "1.0", IsDevelopmentBuild = false}, DeviceId = "dont existing id"},
			new SignInRequestModel
			{
				ClientSpecModel = new ClientSpecModel {ClientVersion = "1.0", IsDevelopmentBuild = false},
				DeviceId = "existingDeviceId",
				CustomSignInRequestModel = new AttachedCustomSignInModel()
			},
			new SignInRequestModel
			{
				ClientSpecModel = new ClientSpecModel {ClientVersion = "1.0", IsDevelopmentBuild = false},
				DeviceId = "existingDeviceId",
				CustomSignInRequestModel = new AttachedCustomSignInModel {Id = "customFbId", SignInType = SignInEnum.FacebookId}
			},
			new SignInRequestModel
			{
				ClientSpecModel = new ClientSpecModel {ClientVersion = "1.0", IsDevelopmentBuild = false},
				DeviceId = "existing id w custom",
				CustomSignInRequestModel = new AttachedCustomSignInModel {Id = "customFbId", SignInType = SignInEnum.FacebookId}
			},
			new SignInRequestModel
			{
				ClientSpecModel = new ClientSpecModel {ClientVersion = "1.0", IsDevelopmentBuild = false}, DeviceId = "existing id w custom",
			},
		};

		public static IAccountService GetAccountService(IMock<IDbContext> mockDbContext)
		{
			var mockMigrationService = new Mock<IMigrationService>();
			mockMigrationService.Setup(service => service.IsValidMigrationVersion(It.IsNotNull<string>(), It.IsNotNull<ClientSpecModel>()))
				.Returns(new UnaryResult<Response>(new Response()));
			mockMigrationService.Setup(service => service.Migrate(It.IsNotNull<string>(), It.IsNotNull<ClientSpecModel>()))
				.Returns(new UnaryResult<Response>(new Response()));
			var jwtAuthProvider = GetJwtAuthenticationProvider();

			return new Services.Auth.AccountService(jwtAuthProvider, mockDbContext.Object, mockMigrationService.Object);
		}

		public static IJwtAuthenticationProvider GetJwtAuthenticationProvider()
		{
			var preSharedKey = Convert.FromBase64String(JwtKey);
			var algorithm = new HS512Algorithm(preSharedKey); // Use Symmetric algorithm (HMAC SHA-512)
			var jwtOptions = new JwtAuthenticationOptions
			{
				Encoder = new JwtEncoder(algorithm),
				Decoder = new JwtDecoder(new JwtAlgorithmResolver(algorithm)),
				Expire = TimeSpan.FromSeconds(JwtExpirationInSec),
			};

			return new CustomJwtAuthenticationProvider(new OptionsWrapper<JwtAuthenticationOptions>(jwtOptions));
		}

		public static Mock<IDbContext> GetDbMockWithData()
		{
			var userRepo = new MockInMemoryRepository<UserModel>("wde", Data.First());
			var mockDbContext = new Mock<IDbContext>();

			mockDbContext.Setup(db => db.Users).Returns(userRepo);

			return mockDbContext;
		}
	}
}