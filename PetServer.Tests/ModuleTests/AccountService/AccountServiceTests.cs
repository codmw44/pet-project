using System;
using System.Threading.Tasks;
using Defenitions.Models.Auth;
using FluentAssertions;
using NUnit.Framework;
using PetServer.Balancer.Models.Balance;
using PetServer.Defenitions.Models.Response;

namespace PetServer.Tests.ModuleTests.AccountService
{
	[TestFixture]
	public class AccountServiceTests
	{
		[Test]
		public void GetAccountService()
		{
			var mockDbContext = AccountServiceTestsSetUp.GetDbMockWithData();

			var accountService = AccountServiceTestsSetUp.GetAccountService(mockDbContext);
			accountService.Should().NotBeNull();
		}

		[TestCaseSource(typeof(AccountServiceTestsSetUp), nameof(AccountServiceTestsSetUp.SignInRequestModels))]
		public async Task TestSignIn_Enter(SignInRequestModel signInRequestModel)
		{
			var mockDbContext = AccountServiceTestsSetUp.GetDbMockWithData();

			var accountService = AccountServiceTestsSetUp.GetAccountService(mockDbContext);

			var result = await accountService.SignInAsync(signInRequestModel);
			result.Expiration.Should().BeCloseTo(DateTimeOffset.Now.AddSeconds(AccountServiceTestsSetUp.JwtExpirationInSec), 100);

			result.Should().NotBeNull();
			result.HasError.Should().BeFalse();
		}

		[Test]
		public async Task TestSignIn_NullDeviceId()
		{
			var mockDbContext = AccountServiceTestsSetUp.GetDbMockWithData();

			var accountService = AccountServiceTestsSetUp.GetAccountService(mockDbContext);

			var result = await accountService.SignInAsync(new SignInRequestModel
			{
				ClientSpecModel = new ClientSpecModel {ClientVersion = "1.0"}, DeviceId = null
			});

			result.Should().NotBeNull();
			result.ErrorEnum.Should().Be(ResponseErrorEnum.InvalidArgs);
		}

		[Test]
		public async Task TestSignIn_NullClientSpecs()
		{
			var mockDbContext = AccountServiceTestsSetUp.GetDbMockWithData();

			var accountService = AccountServiceTestsSetUp.GetAccountService(mockDbContext);

			var result = await accountService.SignInAsync(new SignInRequestModel {ClientSpecModel = null, DeviceId = "sc"});
			result.Should().NotBeNull();
			result.ErrorEnum.Should().Be(ResponseErrorEnum.InvalidArgs);
		}

		[Test]
		public async Task TestSignIn_CustomSignIn_FirstAddFacebookSignIn()
		{
			var mockDbContext = AccountServiceTestsSetUp.GetDbMockWithData();

			var accountService = AccountServiceTestsSetUp.GetAccountService(mockDbContext);

			var result = await accountService.SignInAsync(new SignInRequestModel
			{
				ClientSpecModel = new ClientSpecModel() {ClientVersion = "1.0"},
				DeviceId = "existingDeviceId",
				CustomSignInRequestModel = new AttachedCustomSignInModel() {Id = "dont exist id", SignInType = SignInEnum.FacebookId}
			});

			result.Should().NotBeNull();
			result.ErrorEnum.Should().Be(ResponseErrorEnum.None);

			result.UserId.Should().Be("2");
		}
		
		[Test, Ignore(reason: "Add conflict solver between two user saves. And write tests for it")]
		public async Task TestSignIn_CustomSignIn_AddNewFacebookIdToExistedUserWithFacebookId()
		{
			throw new NotImplementedException("");
			var mockDbContext = AccountServiceTestsSetUp.GetDbMockWithData();

			var accountService = AccountServiceTestsSetUp.GetAccountService(mockDbContext);

			var result = await accountService.SignInAsync(new SignInRequestModel
			{
				ClientSpecModel = new ClientSpecModel() {ClientVersion = "1.0"},
				DeviceId = "existing id w custom",
				CustomSignInRequestModel = new AttachedCustomSignInModel() {Id = "dont exist id", SignInType = SignInEnum.FacebookId}
			});

			result.Should().NotBeNull();
			result.ErrorEnum.Should().Be(ResponseErrorEnum.None);

			result.UserId.Should().Be("3");
		}
		
		[Test]
		public async Task TestSignIn_CustomSignIn_ConnectViaFacebook()
		{
			var mockDbContext = AccountServiceTestsSetUp.GetDbMockWithData();

			var accountService = AccountServiceTestsSetUp.GetAccountService(mockDbContext);

			var result = await accountService.SignInAsync(new SignInRequestModel
			{
				ClientSpecModel = new ClientSpecModel() {ClientVersion = "1.0"},
				DeviceId = "existingDeviceId",
				CustomSignInRequestModel = new AttachedCustomSignInModel() {Id = "customFbId", SignInType = SignInEnum.FacebookId}
			});

			result.Should().NotBeNull();
			result.ErrorEnum.Should().Be(ResponseErrorEnum.None);

			result.UserId.Should().Be("3");
		}

		[Test(Description = "Check what sign out from custom signin type user revert progress to exist")]
		public async Task TestSignIn_CustomSignIn_ConnectToFacebookAndSignOut()
		{
			//todo add conflict solver between two user saves. And write tests for it

			var mockDbContext = AccountServiceTestsSetUp.GetDbMockWithData();

			var accountService = AccountServiceTestsSetUp.GetAccountService(mockDbContext);

			var resultSignInFb = await accountService.SignInAsync(new SignInRequestModel
			{
				ClientSpecModel = new ClientSpecModel {ClientVersion = "1.0"},
				DeviceId = "existingDeviceId",
				CustomSignInRequestModel = new AttachedCustomSignInModel() {Id = "customFbId", SignInType = SignInEnum.FacebookId}
			});

			resultSignInFb.UserId.Should().Be("3");
			resultSignInFb.ErrorEnum.Should().Be(ResponseErrorEnum.None);

			var financeDataFbUser = mockDbContext.Object.Users.FindFirstOrDefaultAsync(model => model.Id == resultSignInFb.UserId).Result.FinanceModel;

			var result = await accountService.SignInAsync(new SignInRequestModel
			{
				ClientSpecModel = new ClientSpecModel() {ClientVersion = "1.0"}, DeviceId = "existingDeviceId", CustomSignInRequestModel = null
			});
			var financeDataDeviceUser = mockDbContext.Object.Users.FindFirstOrDefaultAsync(model => model.Id == result.UserId).Result.FinanceModel;

			result.Should().NotBeNull();
			result.ErrorEnum.Should().Be(ResponseErrorEnum.None);

			result.UserId.Should().Be("2");
			financeDataDeviceUser.Should().NotBe(financeDataFbUser);
		}

		[Test]
		public async Task TestSignIn_CustomSignIn_SignInWithGameCanterAndFacebook()
		{
			//todo add conflict solver between two user saves. And write tests for it

			var mockDbContext = AccountServiceTestsSetUp.GetDbMockWithData();

			var accountService = AccountServiceTestsSetUp.GetAccountService(mockDbContext);

			var resultSignInFb = await accountService.SignInAsync(new SignInRequestModel
			{
				ClientSpecModel = new ClientSpecModel {ClientVersion = "1.0"},
				DeviceId = "existing id w custom",
				CustomSignInRequestModel = new AttachedCustomSignInModel() {Id = "customFbId2", SignInType = SignInEnum.FacebookId}
			});

			resultSignInFb.UserId.Should().Be("4");
			resultSignInFb.ErrorEnum.Should().Be(ResponseErrorEnum.None);

			var resultSignInGameCenter = await accountService.SignInAsync(new SignInRequestModel
			{
				ClientSpecModel = new ClientSpecModel {ClientVersion = "1.0"},
				DeviceId = "existing id w custom",
				CustomSignInRequestModel = new AttachedCustomSignInModel() {Id = "customGameCenterId2", SignInType = SignInEnum.GameCenterId}
			});

			resultSignInGameCenter.Should().NotBeNull();
			resultSignInGameCenter.ErrorEnum.Should().Be(ResponseErrorEnum.None);

			resultSignInGameCenter.UserId.Should().Be("4");
		}
	}
}