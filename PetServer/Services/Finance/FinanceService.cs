using System;
using System.Linq;
using System.Threading.Tasks;
using MagicOnion;
using MagicOnion.Server;
using MagicOnion.Server.Authentication;
using Microsoft.Extensions.Logging;
using Models.Price;
using Models.Rewards;
using MongoDB.Driver;
using PetServer.Controllers.Authentication;
using PetServer.Controllers.Database.Infrastructure;
using PetServer.Defenitions.Models.Response;
using PetServer.Models.Users;

namespace PetServer.Services.Finance
{
	[Authorize]
	public class FinanceService : ServiceBase<IFinanceService>, IFinanceService
	{
		private readonly IDbContext _dbContext;
		private readonly ILogger<IFinanceService> _logger;

		public FinanceService(IDbContext dbContext, ILogger<IFinanceService> logger)
		{
			_dbContext = dbContext;
			this._logger = logger;
		}

		[Authorize]
		public async UnaryResult<FinanceOperationResponse> ProcessReward(IReward reward)
		{
			string userId;

			var identity = Context.GetPrincipal().Identity;
			if (identity is CustomJwtAuthUserIdentity customIdentity)
			{
				userId = customIdentity.UserId;
			}
			else
			{
				return new FinanceOperationResponse {ErrorEnum = ResponseErrorEnum.SessionError};
			}

			switch (reward)
			{
				case FreeReward freeReward:
					return new FinanceOperationResponse() {IsSucceed = true};

				case HardCurrencyReward hardCurrencyReward:
					var userModel = await GetFinanceData(userId);
					if (userModel.Hard + hardCurrencyReward.Amount < 0)
					{
						return
							new FinanceOperationResponse {ErrorEnum = ResponseErrorEnum.DontEnoughFinance};
					}

					userModel.Hard += hardCurrencyReward.Amount;

					await ReplaceUserData(userId, userModel);
					return new FinanceOperationResponse() {IsSucceed = true};

				case PackReward packReward:
					foreach (var packRewardReward in packReward.Rewards)
					{
						var result = await ProcessReward(packRewardReward);
						if (result.IsSucceed == false)
						{
							return result;
						}
					}

					return new FinanceOperationResponse() {IsSucceed = true};

				case SoftCurrencyReward softCurrencyReward:
					var softUserModel = await GetFinanceData(userId);
					if (softUserModel.Soft + softCurrencyReward.Amount < 0)
					{
						return
							new FinanceOperationResponse {ErrorEnum = ResponseErrorEnum.DontEnoughFinance};
					}

					softUserModel.Soft += softCurrencyReward.Amount;
					await ReplaceUserData(userId, softUserModel);
					return new FinanceOperationResponse() {IsSucceed = true};
			}

			_logger.LogError("Dont exist reward type", reward.GetType());
			return
				new FinanceOperationResponse {ErrorEnum = ResponseErrorEnum.InternalError};
		}

		[Authorize]
		public async UnaryResult<FinanceOperationResponse> ProcessPrice(IPrice price)
		{
			string userId;

			var identity = Context.GetPrincipal().Identity;
			if (identity is CustomJwtAuthUserIdentity customIdentity)
			{
				userId = customIdentity.UserId;
			}
			else
			{
				return new FinanceOperationResponse {ErrorEnum = ResponseErrorEnum.SessionError};
			}

			switch (price)
			{
				case HardCurrencyPrice hardCurrencyPrice:
					var userModel = await GetFinanceData(userId);
					if (userModel.Hard - hardCurrencyPrice.Amount < 0)
					{
						_logger.LogInformation("DontEnoughFinance hard");
						return
							new FinanceOperationResponse {ErrorEnum = ResponseErrorEnum.DontEnoughFinance};
					}

					userModel.Hard -= hardCurrencyPrice.Amount;
					await ReplaceUserData(userId, userModel);
					return new FinanceOperationResponse() {IsSucceed = true};

				case SoftCurrencyPrice softCurrencyPrice:
					userModel = await GetFinanceData(userId);
					if (userModel.Soft - softCurrencyPrice.Amount < 0)
					{
						_logger.LogInformation("DontEnoughFinance soft");
						return new FinanceOperationResponse {ErrorEnum = ResponseErrorEnum.DontEnoughFinance};
					}

					userModel.Soft -= softCurrencyPrice.Amount;
					await ReplaceUserData(userId, userModel);
					return new FinanceOperationResponse() {IsSucceed = true};
			}

			_logger.LogError("Dont exist price type", price.GetType());

			return new FinanceOperationResponse {ErrorEnum = ResponseErrorEnum.InternalError};
		}

		[Authorize]
		public async UnaryResult<FinanceModelResponse> GetFinanceModel()
		{
			var identity = Context.GetPrincipal().Identity;
			if (identity is CustomJwtAuthUserIdentity customIdentity)
			{
				var financeData = await GetFinanceData(customIdentity.UserId);
				return new FinanceModelResponse() {FinanceModel = financeData, ErrorEnum = ResponseErrorEnum.None};
			}

			return new FinanceModelResponse() {ErrorEnum = ResponseErrorEnum.SessionError};
		}

		private async Task<FinanceModel> GetFinanceData(string userId)
		{
			var userModel = await _dbContext.Users.FindFirstOrDefaultAsync(model => model.Id == userId);
			if (userModel == null)
			{
				throw new Exception($"Dont exist user {userId}");
			}

			return userModel.FinanceModel;
		}

		private async Task ReplaceUserData(string userId, FinanceModel updatedDocument)
		{
			var update = Builders<UserModel>.Update.Set(model => model.FinanceModel, updatedDocument);
			await _dbContext.Users.UpdateOneAsync(model => model.Id == userId, update);
		}
	}
}