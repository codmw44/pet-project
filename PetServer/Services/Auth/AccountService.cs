using System;
using System.Linq;
using System.Threading.Tasks;
using Defenitions.Models.Auth;
using Defenitions.Services;
using MagicOnion;
using MagicOnion.Server;
using MagicOnion.Server.Authentication;
using MagicOnion.Server.Authentication.Jwt;
using MongoDB.Bson;
using MongoDB.Driver;
using PetServer.Controllers.Authentication;
using PetServer.Controllers.Database.Infrastructure;
using PetServer.Defenitions.Models.Response;
using PetServer.Models.Migration;
using PetServer.Models.Users;

namespace PetServer.Services.Auth
{
	[Authorize]
	public class AccountService : ServiceBase<IAccountService>, IAccountService
	{
		private readonly IJwtAuthenticationProvider _jwtAuthProvider;
		private readonly IDbContext _dbContext;
		private readonly IMigrationService _migrationService;

		public AccountService(IJwtAuthenticationProvider jwtAuthProvider, IDbContext dbContext, IMigrationService migrationService)
		{
			_jwtAuthProvider = jwtAuthProvider;
			_dbContext = dbContext;
			_migrationService = migrationService;
		}

		[AllowAnonymous]
		public async UnaryResult<SignInResponse> SignInAsync(SignInRequestModel signInRequestModel)
		{
			var attachedCustomSignInModel = signInRequestModel.CustomSignInRequestModel;
			var deviceId = signInRequestModel.DeviceId;
			var clientSpecModel = signInRequestModel.ClientSpecModel;

			if (string.IsNullOrEmpty(deviceId))
			{
				return new SignInResponse() {ErrorEnum = ResponseErrorEnum.InvalidArgs};
			}

			if (clientSpecModel == null)
			{
				return new SignInResponse() {ErrorEnum = ResponseErrorEnum.InvalidArgs};
			}

			var userId = await GetIdExistGuestUser(deviceId) ?? await CreateNewGuestUser(deviceId);

			if (attachedCustomSignInModel != null)
			{
				userId = await TryGetUserIdOrAttachSignInToExistUser(userId, attachedCustomSignInModel);
			}

			var isValidMigrationVersionResponse = await _migrationService.IsValidMigrationVersion(userId, clientSpecModel);

			if (isValidMigrationVersionResponse.HasError)
			{
				return new SignInResponse() {ErrorEnum = isValidMigrationVersionResponse.ErrorEnum};
			}

			var migrateResponse = await _migrationService.Migrate(userId, clientSpecModel);

			if (migrateResponse.HasError)
			{
				return new SignInResponse() {ErrorEnum = migrateResponse.ErrorEnum};
			}

			var displayName = userId;
			var role = clientSpecModel.IsDevelopmentBuild ? "Developer" : string.Empty;
			var encodedPayload =
				_jwtAuthProvider.CreateTokenFromPayload(new CustomJwtAuthenticationPayload() {UserId = userId, Name = displayName, Role = role});

			Logger.Debug($"auth: {deviceId} db id {userId} role: {role}");

			return new SignInResponse(userId,
				displayName,
				encodedPayload.Token,
				encodedPayload.Expiration);
		}

		[AllowAnonymous]
		public async UnaryResult<CurrentUserResponse> GetCurrentUserNameAsync()
		{
			var identity = Context.GetPrincipal().Identity;
			if (!(identity is CustomJwtAuthUserIdentity customIdentity))
			{
				return CurrentUserResponse.Anonymous;
			}

			if (customIdentity.IsAuthenticated)
			{
				var userId = customIdentity.UserId;
				var name = await GetName(userId);
				if (name == null)
				{
					return new CurrentUserResponse() {IsAuthenticated = false, UserId = userId};
				}

				return new CurrentUserResponse() {IsAuthenticated = true, UserId = userId, Name = name};
			}

			return CurrentUserResponse.Anonymous;
		}

		[Authorize(Roles = new[] {"Administrator", "Developer"})]
		public async UnaryResult<string> DangerousOperationAsync()
		{
			await Task.Delay(1); // some workloads...

			return "succeed operation";
		}

		private async Task<string> TryGetUserIdOrAttachSignInToExistUser(string userIdFromDeviceIdAuth, AttachedCustomSignInModel customSignInRequestModel)
		{
			var userId = await GetIdExistGuestUser(customSignInRequestModel);
			if (userId == null)
			{
				await AttachSignInToUser(userIdFromDeviceIdAuth, customSignInRequestModel);
				userId = userIdFromDeviceIdAuth;
			}

			return userId;
		}

		private async Task<string?> GetIdExistGuestUser(AttachedCustomSignInModel customSignInRequestModel)
		{
			var userModel = await _dbContext.Users.FindFirstOrDefaultAsync(model =>
				model.AuthModel.AttachedSignIns != null && model.AuthModel.AttachedSignIns.Any(inModel => inModel.Equals(customSignInRequestModel)));
			return userModel?.Id;
		}

		private async Task AttachSignInToUser(string userId, AttachedCustomSignInModel customSignInRequestModel)
		{
			var userModel = await _dbContext.Users.FindFirstOrDefaultAsync(model => model.Id == userId);

			var existThisSignIn = userModel.AuthModel.AttachedSignIns.Any(model => model.SignInType == customSignInRequestModel.SignInType);

			if (existThisSignIn)
			{
				throw new NotImplementedException("Override custom sign in not supported yet. Todo this");
				//todo implement it 
				//if existing sign ins creditional exist on other accounts, then just override on account
				//if existing sign ins creditional exist only on this account, then duplicate account with previous data (without device id) and override sign in model
				//	at current account model
			}

			var attachedSignIns = userModel.AuthModel.AttachedSignIns?.Concat(new[]
				{
					new AttachedCustomSignInModel() {Id = customSignInRequestModel.Id, SignInType = customSignInRequestModel.SignInType},
				})
				.ToArray();

			await _dbContext.Users.UpdateOneAsync(model => model.Id == userId,
				Builders<UserModel>.Update.Set(model => model.AuthModel,
					new AuthModel() {DeviceId = userModel.AuthModel.DeviceId, AttachedSignIns = attachedSignIns}));
		}

		private async Task<string?> GetIdExistGuestUser(string deviceId)
		{
			var userModel = await _dbContext.Users.FindFirstOrDefaultAsync(model => model.AuthModel.DeviceId == deviceId);
			return userModel?.Id;
		}

		private async Task<string> CreateNewGuestUser(string deviceId)
		{
			var id = ObjectId.GenerateNewId().ToString();

			var user = new UserModel
			{
				Id = id,
				AuthModel = new AuthModel() {DeviceId = deviceId},
				FinanceModel = new FinanceModel {Soft = 5000, Hard = 50},
				MigrationModel =
					new MigrationModel
					{
						MigrationVersion = 0 //todo create migration version from current version number by ClientSpecModel from MigrationService.GetMigrationId
					}
			};

			await _dbContext.Users.InsertOneAsync(user);
			return id;
		}

		private async Task<string?> GetName(string userId)
		{
			var userModel = await _dbContext.Users.FindFirstOrDefaultAsync(model => model.Id == userId);
			return userModel?.AuthModel.ToString();
		}
	}
}