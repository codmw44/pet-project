using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using PetServer.Controllers;
using PetServer.Controllers.Database;
using PetServer.Controllers.Database.Infrastructure;
using PetServer.Models.Migration;
using PetServer.Services.Migration.VersionMigrations.Infrastructure;

namespace PetServer.Services.Migration.VersionMigrations
{
	public class VerMigrationsV3 : VersionMigration
	{
		public override int Version => 3;

		private readonly ILogger<IVerMigration> _logger;

		public VerMigrationsV3(IDbContext dbContext, ILogger<IVerMigration> logger) : base(dbContext)
		{
			_logger = logger;
		}

		public override async Task<bool> Migrate(string userId)
		{
			try
			{
				var userData = await GetUserData(userId);
				
				var authModel = userData.GetElement("AuthModel").Value.AsBsonDocument;
				authModel.Set("GameCenterId", new BsonString("Test migration 3"));
				userData.Set("AuthModel", authModel);
				
				await ReplaceUserData(userId, userData);
				await ReplaceMigrationData(userId, new MigrationModel() {MigrationVersion = Version});

				_logger.LogDebug($"Succeed migration user {userId} to {Version}");

				return true;
			}
			catch (Exception e)
			{
				_logger.LogCritical($"Failed migration user {userId} to {Version} with exception: {e}");
				return false;
			}
		}
	}
}