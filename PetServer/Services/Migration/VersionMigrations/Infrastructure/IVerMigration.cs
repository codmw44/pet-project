using System.Threading.Tasks;

namespace PetServer.Services.Migration.VersionMigrations.Infrastructure
{
	public interface IVerMigration
	{
		public abstract int Version { get; }

		bool NeedApply(int playerMigrationId);
		Task<bool> Migrate(string userId);
	}
}