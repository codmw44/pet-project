using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using PetServer.Controllers.Database.Infrastructure;
using PetServer.Models.Migration;
using PetServer.Models.Users;

namespace PetServer.Services.Migration.VersionMigrations.Infrastructure
{
	public abstract class VersionMigration : IVerMigration
	{
		private readonly IDbContext _dbContext;
		public abstract int Version { get; }

		protected VersionMigration(IDbContext dbContext)
		{
			_dbContext = dbContext;
		}

		public bool NeedApply(int playerMigrationId)
		{
			return Version > playerMigrationId;
		}

		public abstract Task<bool> Migrate(string userId);

		protected async Task<BsonDocument?> GetUserData(string userId)
		{
			var user = await _dbContext.UsersAsBson.FindFirstOrDefaultAsync(model => model["_id"].AsString == userId);
			return user;
		}

		protected async Task ReplaceUserData(string userId, BsonDocument updatedDocument)
		{
			await _dbContext.UsersAsBson.FindOneAndReplaceAsync(model => model["_id"].AsString == userId, updatedDocument);
		}

		protected async Task ReplaceMigrationData(string userId, MigrationModel updatedDocument)
		{
			var update = Builders<UserModel>.Update.Set(model => model.MigrationModel, updatedDocument);

			await _dbContext.Users.UpdateOneAsync(model => model.Id == userId, update);
		}
	}
}