using Autofac;

namespace PetServer.Services.Migration.VersionMigrations
{
	public class MigrationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<VerMigrationsV2>().AsImplementedInterfaces();
			builder.RegisterType<VerMigrationsV3>().AsImplementedInterfaces();
		}
	}
}