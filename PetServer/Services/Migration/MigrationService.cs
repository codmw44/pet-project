using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MagicOnion;
using MagicOnion.Server;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using PetServer.Balancer.Models.Balance;
using PetServer.Controllers.Database.Infrastructure;
using PetServer.Defenitions.Models.Response;
using PetServer.Models.Migration;
using PetServer.Services.Migration.Models;
using PetServer.Services.Migration.VersionMigrations.Infrastructure;

namespace PetServer.Services.Migration
{
	public class MigrationService : ServiceBase<IMigrationService>, IMigrationService
	{
		private readonly IDbContext _dbContext;
		private readonly ILogger<IMigrationService> _logger;
		private readonly IEnumerable<IVerMigration> _verMigrations;

		private MigrationConfigurationModel _configurationModel;

		public MigrationService(IDbContext dbContext, ILogger<MigrationService> logger,
			IOptionsMonitor<MigrationConfigurationModel> migrationConfiguration, IEnumerable<IVerMigration> verMigrations)
		{
			_dbContext = dbContext;
			_logger = logger;
			_verMigrations = verMigrations.OrderBy(migration => migration.Version).ToArray();

			migrationConfiguration.OnChange(UpdateConfiguration);
			UpdateConfiguration(migrationConfiguration.CurrentValue);
		}

		private void UpdateConfiguration(MigrationConfigurationModel model)
		{
			_configurationModel = model;
			_logger.LogDebug($"Update configuration: {_configurationModel.ToJson()}");
		}

		public async UnaryResult<Response> IsValidMigrationVersion(string userId, ClientSpecModel clientSpecModel)
		{
			try
			{
				var playerData = await GetPlayerMigrationData(userId);
				var migrationId = GetMigrationId(clientSpecModel);

				if (playerData.MigrationVersion > migrationId)
				{
					_logger.LogDebug($"Player: {userId} has higher migration version then client. Required update client");
					return new Response {ErrorEnum = ResponseErrorEnum.RequireUpdateClient};
				}

				return new Response();
			}
			catch (Exception e)
			{
				_logger.LogError($"Dont exist user data with id: {userId} with exception: {e}");
				return new Response {ErrorEnum = ResponseErrorEnum.DontExistUserId};
			}
		}

		public async UnaryResult<Response> Migrate(string userId, ClientSpecModel clientSpecModel)
		{
			var playerData = await GetPlayerMigrationData(userId);
			var requiredMigrateId = GetMigrationId(clientSpecModel);

			foreach (var migration in _verMigrations.Where(migrationVer =>
				migrationVer.Version <= requiredMigrateId && migrationVer.NeedApply(playerData.MigrationVersion)))
			{
				var result = await migration.Migrate(userId);
				if (!result)
				{
					return new Response {ErrorEnum = ResponseErrorEnum.InternalError};
				}
			}

			_logger.LogDebug($"Player: {userId} has been succeed migrate from {playerData.MigrationVersion} to {requiredMigrateId} version");

			return new Response();
		}

		private int GetMigrationId(ClientSpecModel clientSpecModel)
		{
			var versionModel = _configurationModel.Versions.FirstOrDefault(model => model.ClientVersion == clientSpecModel.ClientVersion);

			if (versionModel == null)
			{
				_logger.LogCritical($"Dont exist configuration client version: {clientSpecModel.ClientVersion}");
				return -1;
			}

			return versionModel.MigrationVersion;
		}

		private async Task<MigrationModel> GetPlayerMigrationData(string userId)
		{
			var userModel = await _dbContext.Users.FindFirstOrDefaultAsync(model=> model.Id==userId);

			if (userModel == null)
			{
				throw new Exception($"Dont exist user {userId}");
			}

			return userModel.MigrationModel;
		}
	}
}