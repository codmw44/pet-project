namespace PetServer.Services.Migration.Models
{
	public class MigrationConfigurationModel
	{
		public MigrationVersionModel[] Versions { get; set; }
	}
}