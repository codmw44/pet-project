namespace PetServer.Services.Migration.Models
{
	public class MigrationVersionModel
	{
		public string ClientVersion { get; set; }
		public int MigrationVersion { get; set; }
	}
}