using MagicOnion;
using MagicOnion.Server;
using PetServer.Controllers;
using PetServer.Controllers.Database;

namespace PetServer.Services
{
	public class PingService : ServiceBase<IPingService>, IPingService
	{
		private readonly DatabaseLivenessController _databaseLivenessController;

		public PingService(DatabaseLivenessController databaseLivenessController)
		{
			_databaseLivenessController = databaseLivenessController;
		}

		public UnaryResult<bool> PingConnection()
		{
			return new UnaryResult<bool>(true);
		}

		public UnaryResult<bool> LivenessProbe()
		{
			return new UnaryResult<bool>(_databaseLivenessController.IsAvailableDb());
		}

		public UnaryResult<bool> ReadinessProbe()
		{
			return new UnaryResult<bool>(_databaseLivenessController.IsAvailableDb());
		}
	}
}