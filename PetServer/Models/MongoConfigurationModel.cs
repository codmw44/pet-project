namespace PetServer.Models
{
	public class MongoConfigurationModel
	{
		public string ConnectionString { get; set; }
		public string DatabaseName { get; set; }
	}
}