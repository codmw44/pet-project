using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using MagicOnion.Server;
using MagicOnion.Server.Hubs;
using OpenTelemetry.Exporter.Prometheus;
using OpenTelemetry.Metrics;
using OpenTelemetry.Metrics.Configuration;
using OpenTelemetry.Metrics.Export;
using OpenTelemetry.Trace;

namespace MagicOnion.OpenTelemetry
{
	public class MagicOnionDefaultLogger : IMagicOnionLogger
	{
		private const string MagiconionKeysMethodLabel = "MagicOnion_keys_Method";
		private readonly List<KeyValuePair<string, string>> _globalTags;
		private static readonly string MethodKey = "MagicOnion/keys/Method";

		#region BuildServiceDefinition

		static readonly string BuildServiceDefinitionName = "MagicOnion/measure/BuildServiceDefinition";
		private readonly Meter _buildServiceDefinitionMeter;
		private readonly CounterMetric<double> _buildServiceDefinitionMeasure;

		#endregion

		#region UnaryRequest

		static readonly string UnaryRequestCountName = "MagicOnion/measure/UnaryRequest";
		private readonly Meter _unaryRequestCountMeter;
		private readonly CounterMetric<long> _unaryRequestCountMeasure;

		#endregion

		#region UnaryResponseSize

		static readonly string UnaryResponseSizeName = "MagicOnion/measure/UnaryResponseSize";
		private readonly Meter _unaryResponseSizeMeter;
		private readonly CounterMetric<long> _unaryResponseSizeMeasure;

		#endregion

		#region UnaryErrorCount

		static readonly string UnaryErrorCountName = "MagicOnion/measure/UnaryErrorCount";
		private Meter _unaryErrorCountMeter;
		private CounterMetric<long> _unaryErrorCountMeasure;

		#endregion

		#region UnaryElapsed

		static readonly string UnaryElapsedName = "MagicOnion/measure/UnaryElapsed";
		private readonly Meter _unaryElapsedMeter;
		private readonly CounterMetric<double> _unaryElapsedMeasure;

		#endregion

		#region StreamingHubErrorCount

		static readonly string StreamingHubErrorCountName = "MagicOnion/measure/StreamingHubErrorCount";

		#endregion

		#region StreamingHubElapsed

		static readonly string StreamingHubElapsedName = "MagicOnion/measure/StreamingHubElapsed";

		#endregion

		#region StreamingHubResponseSize

		static readonly string StreamingHubResponseSizeName = "MagicOnion/measure/StreamingHubResponseSize";

		#endregion

		#region ConnectCount

		static readonly string ConnectCountName = "MagicOnion/measure/Connect";

		#endregion

		#region DisconnectCount

		static readonly string DisconnectCountName = "MagicOnion/measure/Disconnect";

		#endregion

		#region StreamingRequest

		static readonly string StreamingHubRequestCountName = "MagicOnion/measure/StreamingRequest";

		#endregion

		private readonly SpanContext _defaultContext;

		public MagicOnionDefaultLogger(PrometheusExporter prometheusExporter, List<KeyValuePair<string, string>> globalTags, string version)
		{
			_globalTags = globalTags;
			var simpleProcessor = new UngroupedBatcher(prometheusExporter, TimeSpan.FromSeconds(5));

			_defaultContext = default(SpanContext);

			_buildServiceDefinitionMeter = MeterFactory.Create(simpleProcessor).GetMeter("Build ServiceDefinition elapsed time(ms)", version);
			_buildServiceDefinitionMeasure = _buildServiceDefinitionMeter.CreateDoubleCounter(BuildServiceDefinitionName);

			_unaryRequestCountMeter = MeterFactory.Create(simpleProcessor).GetMeter("Request count for Unary request");
			_unaryRequestCountMeasure = _buildServiceDefinitionMeter.CreateInt64Counter(UnaryRequestCountName);

			_unaryResponseSizeMeter = MeterFactory.Create(simpleProcessor).GetMeter("Response size for Unary response.");
			_unaryResponseSizeMeasure = _buildServiceDefinitionMeter.CreateInt64Counter(UnaryResponseSizeName);

			_unaryElapsedMeter = MeterFactory.Create(simpleProcessor).GetMeter("Elapsed time for Unary request.", version);
			_unaryElapsedMeasure = _buildServiceDefinitionMeter.CreateDoubleCounter(UnaryElapsedName);

			_unaryErrorCountMeter = MeterFactory.Create(simpleProcessor).GetMeter("Error count for Unary request.", version);
			_unaryErrorCountMeasure = _buildServiceDefinitionMeter.CreateInt64Counter(UnaryErrorCountName);

			Task.Run(() => RepeatedCollect(5));
			// Create Views
			// var streamingHubErrorCountView = View.Create(
			//     name: ViewName.Create(StreamingHubErrorCountName),
			//     description: "Error count for Streaminghub request.",
			//     measure: StreamingHubErrorCount,
			//     aggregation: Sum.Create(),
			//     columns: TagKeys);
			// var streamingHubElapsedView = View.Create(
			//     name: ViewName.Create(StreamingHubElapsedName),
			//     description: "Elapsed time for Streaminghub request.",
			//     measure: StreamingHubElapsed,
			//     aggregation: Sum.Create(),
			//     columns: TagKeys);
			// var streamingHubRequestCountView = View.Create(
			//     name: ViewName.Create(StreamingHubRequestCountName),
			//     description: "Request count for Streaminghub request.",
			//     measure: StreamingHubRequestCount,
			//     aggregation: Sum.Create(),
			//     columns: TagKeys);
			// var streamingHubResponseSizeView = View.Create(
			//     name: ViewName.Create(StreamingHubResponseSizeName),
			//     description: "Response size for Streaminghub request.",
			//     measure: StreamingHubResponseSize,
			//     aggregation: Sum.Create(),
			//     columns: TagKeys);
			// var connectCountView = View.Create(
			//     name: ViewName.Create(ConnectCountName),
			//     description: "Connect count for Streaminghub request. ConnectCount - DisconnectCount = current connect count. (successfully disconnected)",
			//     measure: ConnectCount,
			//     aggregation: Sum.Create(),
			//     columns: TagKeys);
			// var disconnectCountView = View.Create(
			//     name: ViewName.Create(DisconnectCountName),
			//     description: "Disconnect count for Streaminghub request. ConnectCount - DisconnectCount = current connect count. (successfully disconnected)",
			//     measure: DisconnectCount,
			//     aggregation: Sum.Create(),
			//     columns: TagKeys);
		}

		async Task RepeatedCollect(float interval)
		{
			while (true)
			{
				Collect();

				await Task.Delay(TimeSpan.FromSeconds(interval));
			}
			// ReSharper disable once FunctionNeverReturns
		}

		public void Collect()
		{
			((MeterSdk)_buildServiceDefinitionMeter).Collect();
			((MeterSdk)_unaryRequestCountMeter).Collect();
			((MeterSdk)_unaryElapsedMeter).Collect();
			((MeterSdk)_unaryErrorCountMeter).Collect();
			((MeterSdk)_unaryResponseSizeMeter).Collect();
		}

		public void BeginBuildServiceDefinition()
		{
		}

		public void EndBuildServiceDefinition(double elapsed)
		{
			_buildServiceDefinitionMeasure.Add(_defaultContext, elapsed,
				_buildServiceDefinitionMeter.GetLabelSet(GetGlobalLabelsWithMethodName("BuildServiceDefinition")));

			((MeterSdk)_buildServiceDefinitionMeter).Collect();
		}

		public void BeginInvokeMethod(ServiceContext context, byte[] request, Type type)
		{
			if (context.MethodType == MethodType.DuplexStreaming && context.CallContext.Method.EndsWith("/Connect"))
			{
				// statsRecorder.NewMeasureMap().Put(ConnectCount, 1).Record(CreateTag(context));
			}
			else if (context.MethodType == MethodType.Unary)
			{
				_unaryRequestCountMeasure.Add(_defaultContext, 1,
					_unaryRequestCountMeter.GetLabelSet(GetGlobalLabelsWithMethodName(context.CallContext.Method)));
			}
		}

		public void EndInvokeMethod(ServiceContext context, byte[] response, Type type, double elapsed, bool isErrorOrInterrupted)
		{
			if (context.MethodType == MethodType.DuplexStreaming && context.CallContext.Method.EndsWith("/Connect"))
			{
				// statsRecorder.NewMeasureMap().Put(DisconnectCount, 1).Record(CreateTag(context));
			}
			else if (context.MethodType == MethodType.Unary)
			{
				// var map = statsRecorder.NewMeasureMap();

				var labels = GetGlobalLabelsWithMethodName(context.CallContext.Method);

				_unaryElapsedMeasure.Add(_defaultContext, elapsed,
					_unaryElapsedMeter.GetLabelSet(labels));

				_unaryResponseSizeMeasure.Add(_defaultContext, response.LongLength,
					_unaryResponseSizeMeter.GetLabelSet(labels));
				if (isErrorOrInterrupted)
				{
					_unaryErrorCountMeasure.Add(_defaultContext, 1,
						_unaryErrorCountMeter.GetLabelSet(labels));
				}
			}
		}

		public void BeginInvokeHubMethod(StreamingHubContext context, ReadOnlyMemory<byte> request, Type type)
		{
			// statsRecorder.NewMeasureMap().Put(StreamingHubRequestCount, 1).Record(CreateTag(context));
		}

		public void EndInvokeHubMethod(StreamingHubContext context, int responseSize, Type type, double elapsed, bool isErrorOrInterrupted)
		{
			// var map = statsRecorder.NewMeasureMap();
			//
			// map.Put(StreamingHubElapsed, elapsed);
			// map.Put(StreamingHubResponseSize, responseSize);
			// if (isErrorOrInterrupted)
			// {
			// 	map.Put(StreamingHubErrorCount, 1);
			// }
			//
			// map.Record(CreateTag(context));
		}

		public void InvokeHubBroadcast(string groupName, int responseSize, int broadcastGroupCount)
		{
			// TODO:require more debugging aid(broadcast methodName).
		}

		public void ReadFromStream(ServiceContext context, byte[] readData, Type type, bool complete)
		{
		}

		public void WriteToStream(ServiceContext context, byte[] writeData, Type type)
		{
		}

		private IEnumerable<KeyValuePair<string, string>> GetGlobalLabelsWithMethodName(string buildservicedefinition)
		{
			return _globalTags.Concat(new[] {new KeyValuePair<string, string>(MagiconionKeysMethodLabel, buildservicedefinition),});
		}
	}

	/// <summary>
	/// Global filter. Handle Unary and most outside logging.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
	public class OpenTelemetryCollectorFilterAttribute : Attribute, IMagicOnionFilterFactory<MagicOnionFilterAttribute>
	{
		public int Order { get; set; }

		public MagicOnionFilterAttribute CreateInstance(IServiceLocator serviceLocator)
		{
			return new OpenTelemetryCollectorFilter();
		}
	}

	internal class OpenTelemetryCollectorFilter : MagicOnionFilterAttribute
	{
		// readonly ITracer tracer;
		// readonly ISampler sampler;
		//
		// public OpenTelemetryCollectorFilter(ITracer tracer, ISampler sampler)
		// {
		// 	this.tracer = tracer;
		// 	this.sampler = sampler;
		// }

		public override async ValueTask Invoke(ServiceContext context, Func<ServiceContext, ValueTask> next)
		{
			// https://github.com/open-telemetry/opentelemetry-specification/blob/master/semantic-conventions.md#grpc

			// span name must be `$package.$service/$method` but MagicOnion has no $package.
			// var spanBuilder = tracer.SpanBuilder(context.CallContext.Method).SetSpanKind(SpanKind.Server);
			//
			// if (sampler != null)
			// {
			// 	spanBuilder.SetSampler(sampler);
			// }
			//
			// var span = spanBuilder.StartSpan();
			// try
			// {
			// 	span.SetAttribute("component", "grpc");
			// 	//span.SetAttribute("request.size", context.GetRawRequest().LongLength);
			//
			await next(context);
			//
			// 	//span.SetAttribute("response.size", context.GetRawResponse().LongLength);
			// 	span.SetAttribute("status_code", (long)context.CallContext.Status.StatusCode);
			// 	span.Status = OpenTelemetrygRpcStatusHelper.ConvertStatus(context.CallContext.Status.StatusCode)
			// 		.WithDescription(context.CallContext.Status.Detail);
			// }
			// catch (Exception ex)
			// {
			// 	span.SetAttribute("exception", ex.ToString());
			//
			// 	span.SetAttribute("status_code", (long)context.CallContext.Status.StatusCode);
			// 	span.Status = OpenTelemetrygRpcStatusHelper.ConvertStatus(context.CallContext.Status.StatusCode)
			// 		.WithDescription(context.CallContext.Status.Detail);
			// 	throw;
			// }
			// finally
			// {
			// 	span.End();
			// }
		}
	}

	/// <summary>
	/// StreamingHub Filter. Handle Streaming Hub logging.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
	public class OpenTelemetryHubCollectorFilterAttribute : Attribute, IMagicOnionFilterFactory<StreamingHubFilterAttribute>
	{
		public int Order { get; set; }

		public StreamingHubFilterAttribute CreateInstance(IServiceLocator serviceLocator)
		{
			return new OpenTelemetryHubCollectorFilter();
		}
	}

	internal class OpenTelemetryHubCollectorFilter : StreamingHubFilterAttribute
	{
		// readonly ITracer tracer;
		// readonly ISampler sampler;
		//
		// public OpenTelemetryHubCollectorFilter(ITracer tracer, ISampler sampler)
		// {
		// 	this.tracer = tracer;
		// 	this.sampler = sampler;
		// }

		public override async ValueTask Invoke(StreamingHubContext context, Func<StreamingHubContext, ValueTask> next)
		{
			// https://github.com/open-telemetry/opentelemetry-specification/blob/master/semantic-conventions.md#grpc

			// span name must be `$package.$service/$method` but MagicOnion has no $package.
			// var spanBuilder = tracer.SpanBuilder(context.ServiceContext.CallContext.Method).SetSpanKind(SpanKind.Server);
			//
			// if (sampler != null)
			// {
			// 	spanBuilder.SetSampler(sampler);
			// }
			//
			// var span = spanBuilder.StartSpan();
			// try
			// {
			// 	span.SetAttribute("component", "grpc");
			// 	//span.SetAttribute("request.size", context.GetRawRequest().LongLength);
			//
			await next(context);
			//
			// 	//span.SetAttribute("response.size", context.GetRawResponse().LongLength);
			// 	span.SetAttribute("status_code", (long)context.ServiceContext.CallContext.Status.StatusCode);
			// 	span.Status = OpenTelemetrygRpcStatusHelper.ConvertStatus(context.ServiceContext.CallContext.Status.StatusCode)
			// 		.WithDescription(context.ServiceContext.CallContext.Status.Detail);
			// }
			// catch (Exception ex)
			// {
			// 	span.SetAttribute("exception", ex.ToString());
			//
			// 	span.SetAttribute("status_code", (long)context.ServiceContext.CallContext.Status.StatusCode);
			// 	span.Status = OpenTelemetrygRpcStatusHelper.ConvertStatus(context.ServiceContext.CallContext.Status.StatusCode)
			// 		.WithDescription(context.ServiceContext.CallContext.Status.Detail);
			// 	throw;
			// }
			// finally
			// {
			// 	span.End();
			// }
		}
	}

	public static class OpenTelemetrygRpcStatusHelper
	{
		// gRPC StatusCode and OpenTelemetry.CanonicalCode is same.
		public static global::OpenTelemetry.Trace.Status ConvertStatus(StatusCode code)
		{
			switch (code)
			{
				case StatusCode.OK:
					return global::OpenTelemetry.Trace.Status.Ok;

				case StatusCode.Cancelled:
					return global::OpenTelemetry.Trace.Status.Cancelled;

				case StatusCode.Unknown:
					return global::OpenTelemetry.Trace.Status.Unknown;

				case StatusCode.InvalidArgument:
					return global::OpenTelemetry.Trace.Status.InvalidArgument;

				case StatusCode.DeadlineExceeded:
					return global::OpenTelemetry.Trace.Status.DeadlineExceeded;

				case StatusCode.NotFound:
					return global::OpenTelemetry.Trace.Status.NotFound;

				case StatusCode.AlreadyExists:
					return global::OpenTelemetry.Trace.Status.AlreadyExists;

				case StatusCode.PermissionDenied:
					return global::OpenTelemetry.Trace.Status.PermissionDenied;

				case StatusCode.Unauthenticated:
					return global::OpenTelemetry.Trace.Status.Unauthenticated;

				case StatusCode.ResourceExhausted:
					return global::OpenTelemetry.Trace.Status.ResourceExhausted;

				case StatusCode.FailedPrecondition:
					return global::OpenTelemetry.Trace.Status.FailedPrecondition;

				case StatusCode.Aborted:
					return global::OpenTelemetry.Trace.Status.Aborted;

				case StatusCode.OutOfRange:
					return global::OpenTelemetry.Trace.Status.OutOfRange;

				case StatusCode.Unimplemented:
					return global::OpenTelemetry.Trace.Status.Unimplemented;

				case StatusCode.Internal:
					return global::OpenTelemetry.Trace.Status.Internal;

				case StatusCode.Unavailable:
					return global::OpenTelemetry.Trace.Status.Unavailable;

				case StatusCode.DataLoss:
					return global::OpenTelemetry.Trace.Status.DataLoss;

				default:
					// custom status code? use Unknown.
					return global::OpenTelemetry.Trace.Status.Unknown;
			}
		}
	}
}