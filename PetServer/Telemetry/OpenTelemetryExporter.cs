using System;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using OpenTelemetry.Exporter.Prometheus;

namespace PetServer.Telemetry
{
	public class OpenTelemetryExporter : IDisposable
	{
		private readonly PrometheusExporter _promExporter;
		private readonly PrometheusExporterMetricsHttpServer _httpServer;

		public PrometheusExporter PrometheusExporter => _promExporter;

		public OpenTelemetryExporter()
		{
			var config = new ConfigurationBuilder().AddEnvironmentVariables().Build();
			var exporterHost = config.GetValue("PROMETHEUS_EXPORTER_HOST", "localhost");
			var exporterPort = config.GetValue("PROMETHEUS_EXPORTER_PORT", "9182");

			var promOptions = new PrometheusExporterOptions {Url = $"http://{exporterHost}:{exporterPort}/metrics/"};
			_promExporter = new PrometheusExporter(promOptions);

			_httpServer = new PrometheusExporterMetricsHttpServer(_promExporter);

			Console.WriteLine("httpServer.Start();");

			_httpServer.Start();
		}

		public async Task CheckExporterState()
		{
			var prevStatue = TaskStatus.Canceled;

			while (true)
			{
				try
				{
					var copyThread =
						_httpServer.GetType().GetField("workerThread", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(_httpServer) as Task;

					if (copyThread.Exception != null)
					{
						Console.WriteLine("[Exporter] Thread.Exception " + copyThread.Exception);
					}

					if (prevStatue != copyThread.Status)
					{
						Console.WriteLine("[Exporter] Thread.Status " + copyThread.Status.ToString());

						prevStatue = copyThread.Status;

						if (prevStatue != TaskStatus.Running && prevStatue != TaskStatus.Created)
						{
							Console.WriteLine("httpServer.Start();");

							_httpServer.Stop();
							_httpServer.Start();
						}
					}
				}
				catch (Exception e)
				{
					Console.WriteLine(e);
				}

				await Task.Delay(10000);
			}
		}

		public void Dispose()
		{
			Console.WriteLine("httpServer.Stop();");
			_httpServer.Stop();
		}
	}
}