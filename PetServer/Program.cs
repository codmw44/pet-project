﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Grpc.Core;
using Grpc.Core.Logging;
using LitJWT;
using LitJWT.Algorithms;
using MagicOnion;
using MagicOnion.Hosting;
using MagicOnion.OpenTelemetry;
using MagicOnion.Server;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PetServer.Controllers;
using PetServer.Controllers.Authentication;
using PetServer.Controllers.Database;
using PetServer.Controllers.Database.Implementations;
using PetServer.Models;
using PetServer.Services.Migration;
using PetServer.Services.Migration.Models;
using PetServer.Services.Migration.VersionMigrations;
using PetServer.Telemetry;

namespace PetServer
{
	class Program
	{
		static async Task Main(string[] args)
		{
			GrpcEnvironment.SetLogger(new ConsoleLogger());
			NLogConfigurationController.SetDefaultConfiguration();
			
			var openTelemetryExporter = new OpenTelemetryExporter();

			var magicOnionHost = MagicOnionHost.CreateDefaultBuilder()
				.ConfigureAppConfiguration((context, builder) =>
				{
					var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
					builder.SetBasePath(AppContext.BaseDirectory)
						.AddJsonFile("appsettings.json", false)
						.AddJsonFile($"appsettings.{env}.json", true)
						.AddJsonFile("migrationconfig.json", false, true)
						.AddEnvironmentVariables();
				})
				.UseMagicOnion()
				.UseServiceProviderFactory(new AutofacServiceProviderFactory())
				.ConfigureContainer<ContainerBuilder>(builder =>
				{
					// registering services in the Autofac ContainerBuilder
					builder.RegisterModule<MigrationModule>();

					builder.RegisterType<DatabaseLivenessController>().SingleInstance();
					
					builder.RegisterType<DbContext>().AsImplementedInterfaces().SingleInstance();
					builder.RegisterType<MongoDatabaseFactory>().AsImplementedInterfaces().SingleInstance();
					builder.RegisterType<RepositoryFactory>().AsImplementedInterfaces().SingleInstance();
					
					builder.RegisterType<MigrationService>().AsImplementedInterfaces().SingleInstance();
					builder.RegisterType<CustomJwtAuthenticationProvider>().AsImplementedInterfaces().SingleInstance();
				})
				.ConfigureServices((hostContext, services) =>
				{
					services.AddMagicOnionJwtAuthentication<CustomJwtAuthenticationProvider>(options =>
					{
						var preSharedKey = Convert.FromBase64String(hostContext.Configuration.GetSection("Jwt:Secret").Value);
						var algorithm = new HS512Algorithm(preSharedKey); // Use Symmetric algorithm (HMAC SHA-512)
						options.Encoder = new JwtEncoder(algorithm);
						options.Decoder = new JwtDecoder(new JwtAlgorithmResolver(algorithm));
						options.Expire = TimeSpan.FromSeconds(5);
					});
					services.Configure<MagicOnionHostingOptions>(options =>
					{
						options.Service.GlobalFilters.Add(new OpenTelemetryCollectorFilterAttribute());
						options.Service.GlobalStreamingHubFilters.Add(new OpenTelemetryHubCollectorFilterAttribute());

						options.Service.MagicOnionLogger = new MagicOnionDefaultLogger(openTelemetryExporter.PrometheusExporter,
							new List<KeyValuePair<string, string>>(), Assembly.GetEntryAssembly().GetName().Version.ToString());
					});

					services.AddOptions<MongoConfigurationModel>().Bind(hostContext.Configuration.GetSection("Mongo"));
					services.AddOptions<MigrationConfigurationModel>().Bind(hostContext.Configuration.GetSection("Migration"));
				})
				.UseConsoleLifetime()
				.Build();

			var swaggerWebHost = new WebHostBuilder()
				.ConfigureServices(collection =>
				{
					collection.AddSingleton(magicOnionHost.Services.GetService<MagicOnionHostedServiceDefinition>()
						.ServiceDefinition);
					collection.Configure<KestrelServerOptions>(options => { options.AllowSynchronousIO = true; });
				})
				.UseKestrel()
				.UseStartup<Startup>()
				.UseUrls("http://0.0.0.0:5432/")
				.Build();

			// Run and wait both.
			await Task.WhenAll(swaggerWebHost.RunAsync(), magicOnionHost.RunAsync(), openTelemetryExporter.CheckExporterState());
		}
	}

	public class Startup
	{
		public void Configure(IApplicationBuilder app, MagicOnionServiceDefinition magicOnion)
		{
			var pathBase = Environment.GetEnvironmentVariable("SWAGGER_BASEPATH");

			if (!string.IsNullOrWhiteSpace(pathBase))
			{
				app.UsePathBase($"/{pathBase.TrimStart('/')}");
			}

			// app.UseMagicOnionSwagger(magicOnion.MethodHandlers,
				// new SwaggerOptions("MagicOnion.Server", "Swagger Integration Test", "/"));

			app.UseMagicOnionHttpGateway(magicOnion.MethodHandlers, new Channel("0.0.0.0:12345", ChannelCredentials.Insecure));
		}
	}
}