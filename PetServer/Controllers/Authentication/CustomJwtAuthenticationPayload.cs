namespace PetServer.Controllers.Authentication
{
    public class CustomJwtAuthenticationPayload
    {
        public string UserId { get; set; }
        public string Role { get; set; }
        public string Name { get; set; }
    }
}