using System.Security.Principal;

namespace PetServer.Controllers.Authentication
{
	public class CustomJwtAuthUserIdentity : IIdentity
	{
		public string UserId { get; }

		public bool IsAuthenticated => true;
		public string AuthenticationType => "Jwt";

		public string Name { get; }

		public CustomJwtAuthUserIdentity(string userId, string displayName)
		{
			UserId = userId;
			Name = displayName;
		}
	}
}