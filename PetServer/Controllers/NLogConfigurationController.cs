using NLog;
using NLog.Config;
using NLog.Targets;

namespace PetServer.Controllers
{
    public class NLogConfigurationController
    {
        public static void SetDefaultConfiguration()
        {
            // Step 1. Create configuration object 
            var config = new LoggingConfiguration();

            // Step 2. Create targets
            
            var consoleTarget = new ColoredConsoleTarget("target1")
            {
                Layout = @"${date:format=HH\:mm\:ss\.ffffff} ${level} ${message} ${exception}"
            };
            config.AddTarget(consoleTarget);
            
            config.AddRuleForAllLevels(consoleTarget); // all to console

            // Step 4. Activate the configuration
            SetLogConfiguration(config);
        }

        private static void SetLogConfiguration(LoggingConfiguration config)
        {
            LogManager.Configuration = config;
        }
    }
}