using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using PetServer.Controllers.Database.Infrastructure;
using PetServer.Models;

namespace PetServer.Controllers.Database
{
	public class DatabaseLivenessController
	{
		private readonly ILogger<DatabaseLivenessController> _logger;

		private IMongoDatabase _database;

		private bool _isInitialized;

		public DatabaseLivenessController(IOptionsMonitor<MongoConfigurationModel> configurationModel, ILogger<DatabaseLivenessController> logger)
		{
			_logger = logger;
			configurationModel.OnChange(Initialize);
			Initialize(configurationModel.CurrentValue);
		}

		private void Initialize(MongoConfigurationModel config)
		{
			var client = new MongoClient(config.ConnectionString);
			_database = client.GetDatabase(config.DatabaseName);
			_isInitialized = true;
		}

		public bool IsAvailableDb()
		{
			//exception throws for return error code on html for kubernetes
			if (!_isInitialized)
			{
				throw new Exception("Database isn't ready");
			}

			try
			{
				_database.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(TimeSpan.FromSeconds(5));
			}
			catch (Exception e)
			{
				_logger.LogError(e.ToString());
				throw new Exception("Database isn't ready");
			}

			return true;
		}
	}
}