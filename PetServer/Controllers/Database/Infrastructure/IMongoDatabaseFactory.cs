using MongoDB.Driver;

namespace PetServer.Controllers.Database.Infrastructure
{
	public interface IMongoDatabaseFactory
	{
		IMongoDatabase Connect(RepositoryOptions repositoryOptions);
	}
}