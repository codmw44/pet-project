using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace PetServer.Controllers.Database.Infrastructure
{
	public interface IRepository<T>
	{
		string CollectionName { get; }
		Task<long> CountAsync(Expression<Func<T, bool>> filter);
		Task<T> FindFirstOrDefaultAsync(Expression<Func<T, bool>> expression);
		Task<T> FindOneAndReplaceAsync(Expression<Func<T, bool>> filter, T replacement);
		Task InsertOneAsync(T document);
		Task UpdateOneAsync(Expression<Func<T, bool>> filter, UpdateDefinition<T> updateDefinition);
	}
}