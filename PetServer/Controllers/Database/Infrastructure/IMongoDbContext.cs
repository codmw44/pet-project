using MongoDB.Driver;

namespace PetServer.Controllers.Database
{
	public interface IMongoDbContext
	{
		IMongoCollection<T> GetCollection<T>(string name);
	}
}