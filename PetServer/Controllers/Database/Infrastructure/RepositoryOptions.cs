namespace PetServer.Controllers.Database.Infrastructure
{
	public class RepositoryOptions
	{
		public string ConnectionString { get; set; }
		public string DatabaseName { get; set; }
		public string CollectionName { get; set; }
	}
}