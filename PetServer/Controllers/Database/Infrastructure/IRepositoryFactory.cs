using PetServer.Models;

namespace PetServer.Controllers.Database.Infrastructure
{
	public interface IRepositoryFactory
	{
		IRepository<TEntity> Create<TEntity>(RepositoryOptions options);
	}
}