using MongoDB.Bson;
using PetServer.Models.Users;

namespace PetServer.Controllers.Database.Infrastructure
{
	public interface IDbContext
	{
		IRepository<UserModel> Users { get; }
		IRepository<BsonDocument> UsersAsBson { get; }
	}
}