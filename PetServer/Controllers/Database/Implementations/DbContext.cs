using System;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using PetServer.Controllers.Database.Infrastructure;
using PetServer.Models;
using PetServer.Models.Users;

namespace PetServer.Controllers.Database.Implementations
{
	public class DbContext : IDbContext
	{
		public DbContext(IRepositoryFactory repoFactory, IOptions<MongoConfigurationModel> mongoConfigurationModel)
		{
			if (mongoConfigurationModel == null)
			{
				throw new ArgumentNullException(nameof(mongoConfigurationModel));
			}

			var configurationModel = mongoConfigurationModel.Value;

			var userCollection = new RepositoryOptions {ConnectionString = configurationModel.ConnectionString, CollectionName = "Players", DatabaseName = configurationModel.DatabaseName};
			
			Users = repoFactory.Create<UserModel>(userCollection);
			UsersAsBson = repoFactory.Create<BsonDocument>(userCollection);
		}

		public IRepository<UserModel> Users { get; }
		public IRepository<BsonDocument> UsersAsBson { get; }
	}
}