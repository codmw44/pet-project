using System.Linq;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;
using PetServer.Controllers.Database.Infrastructure;

namespace PetServer.Controllers.Database.Implementations
{
	public class MongoDatabaseFactory : IMongoDatabaseFactory
	{
		private readonly ILogger<IMongoDatabaseFactory> _logger;

		public MongoDatabaseFactory(ILogger<IMongoDatabaseFactory> logger)
		{
			_logger = logger;
		}

		public IMongoDatabase Connect(RepositoryOptions repositoryOptions)
		{
			_logger.LogDebug($"Server instance with options {repositoryOptions.ToJson()}");

			var client = new MongoClient(repositoryOptions.ConnectionString);
			var database = client.GetDatabase(repositoryOptions.DatabaseName);
			
			CreateDatabaseIfNotExist(database, client, repositoryOptions.DatabaseName);
			return database;
		}

		private void CreateDatabaseIfNotExist(IMongoDatabase database, IMongoClient client, string databaseName)
		{
			if (IsContainsTargetDatabase(client, databaseName))
			{
				return;
			}

			database.CreateCollection("InitCollection");

			_logger.LogInformation($"Don't contains {databaseName} database and create one");
		}

		private bool IsContainsTargetDatabase(IMongoClient client, string databaseName)
		{
			var dbList = client.ListDatabases().ToList();

			return dbList.Any(db => db["name"] == databaseName);
		}
	}
}