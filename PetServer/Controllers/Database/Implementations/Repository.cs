using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using PetServer.Controllers.Database.Infrastructure;

namespace PetServer.Controllers.Database.Implementations
{
	public class Repository<TEntity> : IRepository<TEntity>
	{
		private readonly IMongoCollection<TEntity> _collection;

		public Repository(IMongoCollection<TEntity> collection)
		{
			_collection = collection ?? throw new ArgumentNullException(nameof(collection));

			CollectionName = collection.CollectionNamespace.CollectionName;
		}

		public string CollectionName { get; private set; }

		public Task<long> CountAsync(Expression<Func<TEntity, bool>> filter)
		{
			return _collection.CountDocumentsAsync(filter);
		}

		public async Task<TEntity> FindFirstOrDefaultAsync(Expression<Func<TEntity, bool>> expression)
		{
			return await _collection.AsQueryable().Where(expression).FirstOrDefaultAsync();
		}

		public Task<TEntity> FindOneAndReplaceAsync(Expression<Func<TEntity, bool>> filter, TEntity replacement)
		{
			return _collection.FindOneAndReplaceAsync(filter, replacement);
		}

		public Task InsertOneAsync(TEntity document)
		{
			return _collection.InsertOneAsync(document);
		}

		public Task UpdateOneAsync(Expression<Func<TEntity, bool>> filter, UpdateDefinition<TEntity> updateDefinition)
		{
			return _collection.UpdateOneAsync(filter, updateDefinition);
		}
	}
}