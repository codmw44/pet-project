using System;
using PetServer.Controllers.Database.Infrastructure;

namespace PetServer.Controllers.Database.Implementations
{
	public class RepositoryFactory : IRepositoryFactory
	{
		private readonly IMongoDatabaseFactory _dbFactory;

		public RepositoryFactory(IMongoDatabaseFactory dbFactory)
		{
			_dbFactory = dbFactory ?? throw new ArgumentNullException(nameof(dbFactory));
		}

		public IRepository<TEntity> Create<TEntity>(RepositoryOptions options)
		{
			var db = _dbFactory.Connect(options);
			return new Repository<TEntity>(db.GetCollection<TEntity>(options.CollectionName));
		}
	}
}