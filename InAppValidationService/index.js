const http = require("http");
const url = require('url');
const iap = require('in-app-purchase');
const Prometheus = require('prom-client');

var isReady = false;

let prometheusEnabled = (process.env.PROMETHEUS_EXPORTER_ENABLED === 'true');
let prometheusPort = process.env.PROMETHEUS_EXPORTER_PORT;
let googleKey = process.env.GOOGLE_API_KEY;
let testEnv = (process.env.TEST === 'true');
let verboseEnv = (process.env.VERBOSE === 'true');
let portEnv = process.env.PORT;
if (portEnv === undefined) {
    portEnv = 3000;
}


const collectDefaultMetrics = Prometheus.collectDefaultMetrics;
const defaultLabels = {app: 'iap_validation'};
Prometheus.register.setDefaultLabels(defaultLabels);
collectDefaultMetrics();


const validationDuration = new Prometheus.Histogram({
    name: 'iap_validation_duration_process_validation',
    help: 'Duration of validation requests in ms',
    labelNames: ['result'],
    // buckets for response time from 0.1ms to 500ms
    buckets: [0.10, 5, 15, 50, 100, 200, 300, 400, 500]
})
const countInvalidRequests = new Prometheus.Gauge({
    name: 'iap_validation_count_invalid_requests',
    help: 'Count invalid parameters requests to /',
})
const countErrors = new Prometheus.Gauge({
    name: 'iap_validation_count_errors',
    help: 'Count internal errors',
})
const countUnverifiedTransaction = new Prometheus.Gauge({
    name: 'iap_validation_count_unverified_transactions',
    labelNames: ['transactionId', 'reason'],
    help: 'Count unverified transactions',
})
const countVerifiedTransaction = new Prometheus.Gauge({
    name: 'iap_validation_count_verified_transactions',
    labelNames: ['transactionId'],
    help: 'Count verified transactions',
})


iap.config({

    /* Configurations for Apple */
    appleExcludeOldTransactions: true, // if you want to exclude old transaction, set this to true. Default is false

    /* Configurations for Google Play */
    // googlePublicKeyPath: 'path/to/public/key/directory/', // this is the path to the directory containing iap-sanbox/iap-live files
    googlePublicKeyStrSandBox: googleKey, // this is the google iap-sandbox public key string
    googlePublicKeyStrLive: googleKey, // this is the google iap-live public key string

    /* Configurations all platforms */
    test: testEnv, // For Apple and Googl Play to force Sandbox validation only
    verbose: verboseEnv // Output debug logs to stdout stream
});

iap.setup()
    .then(() => {
        isReady = true;
    })
    .catch((error) => {
        console.error("Error on setup iap" + error)
    });


if (prometheusEnabled === true) {
    http.createServer(function (request, response) {
        if (request.url === '/metrics') {
            response.end(Prometheus.register.metrics())
            countInvalidRequests.set(0);
            countErrors.set(0);
            countUnverifiedTransaction.set(0);
            countVerifiedTransaction.set(0)
        }
    }).listen(prometheusPort);
}

http.createServer(function (request, response) {
    let start_time = new Date().getTime();

    if (request.url === '/healthz') {

        if (isReady) {
            response.write("Ready");
        } else {
            response.statusCode = 500;
        }

        response.end();
        return;
    } else {
        if (!isReady) {
            response.statusCode = 500;

            response.end()
            console.error("Application dont ready")
            return;
        }

        let recieptFromRequest = getRecieptFromRequest(request);
        let productId = getProductIdFromRequest(request);

        if (undefined === recieptFromRequest || productId === undefined) {
            response.end("Require ?reciept= &productId=");

            countInvalidRequests.inc();
            console.error("Failed get parameters from request: " + request.url);
            return;
        }

        try {
            console.info("recieptFromRequest: " + recieptFromRequest)

            iap.validate(recieptFromRequest).then(function (validatedData) {
                onSuccess(validatedData, response, recieptFromRequest, productId);
                validationDuration.labels("succeeded").observe(new Date().getTime() - start_time);
            }).catch((error) => {
                onError(error, response);
                countUnverifiedTransaction.labels("transaction id", error.toString()).inc();//todo change "transaction id" to real id 
                validationDuration.labels("failed").observe(new Date().getTime() - start_time);
            });
        } catch (e) {
            countErrors.inc();
            response.statusCode = 500;
            onError(e, response);
        }
    }


}).listen(portEnv);

function getRecieptFromRequest(request) {

    var query = url.parse(request.url, true).query;
    return query["reciept"];
}

function getProductIdFromRequest(request) {

    var query = url.parse(request.url, true).query;
    return query["productId"];
}

function onSuccess(validatedData, response, reciept, productId) {
    // validatedData: the actual content of the validated receipt
    // validatedData also contains the original receipt
    var options = {
        ignoreCanceled: true, // Apple ONLY (for now...): purchaseData will NOT contain cancceled items
        ignoreExpired: true // purchaseData will NOT contain exipired subscription items
    };
    // validatedData contains sandbox: true/false for Apple and Amazon
    var purchaseData = iap.getPurchaseData(validatedData, options);

    reciept = JSON.parse(reciept);

    console.log(JSON.stringify(purchaseData))

    if (reciept.Store === 'AppleAppStore') {
        if (reciept.TransactionID !== purchaseData[0].transactionId) {
            countUnverifiedTransaction.labels(recieptJson.TransactionID, error.message).inc();
            throw new Error("Dosent match transaction ids!");
        }
        if (productId !== purchaseData[0].productId) {
            throw new Error("Dosent match productIds!");
        }

        countVerifiedTransaction.labels(purchaseData[0].transactionId).inc();
    } else if (reciept.Store === 'GooglePlay') {

        countVerifiedTransaction.labels(purchaseData[0].orderId).inc();
    }

    response.write("true");
    response.end();
}

function onError(error, response) {
    console.error(error);

    response.write(error.toString())
    response.end();
}