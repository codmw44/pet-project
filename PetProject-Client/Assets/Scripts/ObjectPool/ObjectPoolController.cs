using System.Collections.Generic;
using UnityEngine;

namespace ObjectPool
{
	public static class ObjectPoolController
	{
		public const int PoolBufferSize = 50;
		public const int InitialPoolSize = 3;

		private static readonly Queue<int> _pooledId = new Queue<int>();
		private static readonly HashSet<int> _idIndex = new HashSet<int>();

		public static void CreatePool<T>(T component) where T : Component
		{
			var instanceId = component.GetInstanceID();
			if (_idIndex.Contains(instanceId))
			{
				return;
			}

			ObjectPool.CreatePool(component, InitialPoolSize);
			_pooledId.Enqueue(instanceId);
			_idIndex.Add(instanceId);

			while (_pooledId.Count > PoolBufferSize)
			{
				var idToRemove = _pooledId.Dequeue();
				_idIndex.Remove(idToRemove);
				ObjectPool.RemovePool(idToRemove);
			}
		}

		public static void Clear(bool destroyObjects)
		{
			foreach (var id in _pooledId)
			{
				ObjectPool.RemovePool(id, destroyObjects);
			}

			_pooledId.Clear();
			_idIndex.Clear();
		}
	}
}