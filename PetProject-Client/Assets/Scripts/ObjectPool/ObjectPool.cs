﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace ObjectPool
{
	public class ObjectPool : SingletonBehaviour<ObjectPool>
	{
		private static Transform _cachedTransform;

		// Prefab ID - Component
		private readonly Dictionary<int, Queue<Component>> _pooledComponents = new Dictionary<int, Queue<Component>>();
		private readonly Dictionary<int, Queue<GameObject>> _pooledGameObjects = new Dictionary<int, Queue<GameObject>>();
		private readonly Dictionary<int, Queue<Transform>> _pooledTransforms = new Dictionary<int, Queue<Transform>>();

		// Instantiated ID - Prefab ID
		private readonly Dictionary<int, int> _spawnedObjects = new Dictionary<int, int>();

		public static Transform CachedTransform
		{
			get
			{
				if (_cachedTransform == null)
				{
					_cachedTransform = Instance.transform;
				}

				return _cachedTransform;
			}
		}

		public static void CreatePool<T>(T component, int initialPoolSize = 0) where T : Component
		{
			var instanceId = component.GetInstanceID();
			var poolInstance = Instance;
			var hasPool = poolInstance._pooledComponents.ContainsKey(instanceId);
			if (hasPool)
			{
				return;
			}

			var objList = new Queue<Component>();
			poolInstance._pooledComponents.Add(instanceId, objList);

			var gameObjList = new Queue<GameObject>();
			poolInstance._pooledGameObjects.Add(instanceId, gameObjList);

			var trList = new Queue<Transform>();
			poolInstance._pooledTransforms.Add(instanceId, trList);

			if (initialPoolSize <= 0)
			{
				return;
			}

			var go = component.gameObject;
			var tr = component.transform;

			var active = go.activeSelf;
			go.SetActive(false);

			for (var i = 0; i < initialPoolSize; ++i)
			{
				var t = InstantiateObject(tr, CachedTransform);

				objList.Enqueue(t.GetComponent<T>());
				gameObjList.Enqueue(t.gameObject);
				trList.Enqueue(t);
			}

			go.SetActive(active);
		}

		public static void RemovePool<T>(T component, bool destroyObjects = true) where T : Component
		{
			RemovePool(component.GetInstanceID(), destroyObjects);
		}

		public static void RemovePool(int instanceId, bool destroyObjects = true)
		{
			var poolInstance = Instance;
			var hasPool = poolInstance._pooledComponents.ContainsKey(instanceId);
			if (!hasPool)
			{
				return;
			}

			if (destroyObjects)
			{
				var goQueue = poolInstance._pooledGameObjects[instanceId];
				while (goQueue.Count > 0)
				{
					var go = goQueue.Dequeue();
					if (poolInstance._spawnedObjects.ContainsKey(instanceId))
					{
						poolInstance._spawnedObjects.Remove(instanceId);
					}

					Destroy(go);
				}
			}

			Instance._pooledComponents.Remove(instanceId);
			Instance._pooledGameObjects.Remove(instanceId);
			Instance._pooledTransforms.Remove(instanceId);
		}

		public static T Spawn<T>(T component, Transform parent = null, Vector3 position = default, Quaternion rotation = default) where T : Component
		{
			var instanceId = component.GetInstanceID();
			var poolInstance = Instance;
			T result;
			var hasPool = poolInstance._pooledComponents.ContainsKey(instanceId);
			if (hasPool)
			{
				Object tmp;
				GameObject g;
				Transform t;
				Vector3 scale;
				if (poolInstance._pooledComponents[instanceId].Count > 0)
				{
					tmp = poolInstance._pooledComponents[instanceId].Dequeue();
					g = poolInstance._pooledGameObjects[instanceId].Dequeue();
					t = poolInstance._pooledTransforms[instanceId].Dequeue();
					if (tmp != null)
					{
						result = (T)tmp;
						scale = t.localScale;
						t.SetParent(parent, false);
						t.localScale = scale;
						t.localPosition = position;
						t.localRotation = rotation;
						g.SetActive(true);
						poolInstance._spawnedObjects[result.GetInstanceID()] = instanceId;
						return result;
					}
				}

				t = InstantiateObject(component.transform, parent, position, rotation);
				g = t.gameObject;
				result = g.GetComponent<T>();
				poolInstance._spawnedObjects[result.GetInstanceID()] = instanceId;
			}
			else
			{
				result = InstantiateObject(component.transform, parent, position, rotation).GetComponent<T>();
			}

			return result;
		}

		private static Transform InstantiateObject(Transform tr, Transform parent = null, Vector3 position = default, Quaternion rotation = default)
		{
			var trans = Instantiate(tr, position, rotation);
			if (trans != null)
			{
				var scale = trans.localScale;
				trans.SetParent(parent, false);
				trans.localScale = scale;
				trans.localPosition = position;
				trans.localRotation = rotation;
			}

			return trans;
		}

		public static void Recycle(Component component, float delay = 0)
		{
			var instanceId = component.GetInstanceID();
			int prefabId;
			if (Instance._spawnedObjects.TryGetValue(instanceId, out prefabId))
			{
				TweenCallback recycle = () =>
				{
					var go = component.gameObject;
					var tr = component.transform;

					Instance._pooledComponents[prefabId].Enqueue(component);
					Instance._pooledGameObjects[prefabId].Enqueue(go);
					Instance._pooledTransforms[prefabId].Enqueue(tr);

					Instance._spawnedObjects.Remove(instanceId);
					tr.SetParent(CachedTransform, false);
					go.SetActive(false);
				};
				if (delay > 0)
				{
					DOVirtual.DelayedCall(delay, recycle);
				}
				else
				{
					recycle();
				}
			}
			else
			{
				if (component != null)
				{
					Destroy(component.gameObject, delay);
				}
			}
		}

		private static void RecycleObject(Component component, int prefabID, int instanceID)
		{
			var go = component.gameObject;
			var tr = component.transform;

			Instance._pooledComponents[prefabID].Enqueue(component);
			Instance._pooledGameObjects[prefabID].Enqueue(go);
			Instance._pooledTransforms[prefabID].Enqueue(tr);

			Instance._spawnedObjects.Remove(instanceID);
			tr.SetParent(CachedTransform, false);
			go.SetActive(false);
		}

		protected void SingletonInstanceCreated()
		{
			CachedTransform.localPosition = Vector3.zero;
			CachedTransform.localRotation = Quaternion.identity;
			CachedTransform.localScale = Vector3.one;
		}
	}
}