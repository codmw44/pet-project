﻿using UnityEngine;

namespace ObjectPool
{
	public static class ObjectPoolExtentions
	{
		public static void CreatePool<T>(this T obj, int initialPoolSize = 0) where T : Component
		{
			ObjectPool.CreatePool(obj, initialPoolSize);
		}

		public static void RemovePool<T>(this T obj) where T : Component
		{
			ObjectPool.RemovePool(obj);
		}

		public static T Spawn<T>(this T component, Transform parent = null, Vector3 position = default, Quaternion rotation = default) where T : Component
		{
			return ObjectPool.Spawn(component, parent, position, rotation);
		}

		public static void Recycle(this Component obj, float delay = 0)
		{
			ObjectPool.Recycle(obj, delay);
		}

		public static void Clear(this Transform tr)
		{
			var childCount = tr.childCount;
			for (var i = childCount - 1; i >= 0; --i)
			{
				var t = tr.GetChild(i);
				if (t == null)
				{
					continue;
				}

				t.SetParent(null);
				t.Recycle();
			}

			tr.DetachChildren();
		}
	}
}