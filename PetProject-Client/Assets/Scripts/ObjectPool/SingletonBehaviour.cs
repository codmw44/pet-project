﻿using UnityEngine;

namespace ObjectPool
{
	public class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
	{
		private static readonly object _lock = new object();

		protected static T _instance;
		[SerializeField] private bool _isPersistant;

		public static T Instance
		{
			get
			{
				lock (_lock)
				{
					if (_instance == null)
					{
						_instance = (T)FindObjectOfType(typeof(T));
						if (_instance == null)
						{
							var singleton = new GameObject(string.Format("_{0}", typeof(T)));
							_instance = singleton.AddComponent<T>();
							singleton.SendMessage("SingletonInstanceCreated", SendMessageOptions.DontRequireReceiver);
						}
					}

					return _instance;
				}
			}
		}

		public static bool HasInstance()
		{
			return _instance != null;
		}

		protected void Awake()
		{
			PersistantCheck();
		}

		protected void OnDestroy()
		{
			_instance = null;
		}

		protected void PersistantCheck()
		{
			if (_isPersistant)
			{
				gameObject.name = string.Format("_Persistant.{0}", typeof(T));
				if (_instance != null)
				{
					if (_instance.gameObject.GetInstanceID() != gameObject.GetInstanceID())
					{
						Destroy(gameObject);
					}
				}
				else
				{
					DontDestroyOnLoad(gameObject);
					_instance = gameObject.GetComponent<T>();
					SendMessage("SingletonInstanceCreated", SendMessageOptions.DontRequireReceiver);
				}
			}
		}
	}
}