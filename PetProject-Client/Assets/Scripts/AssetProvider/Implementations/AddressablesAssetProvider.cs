using System;
using System.Collections.Generic;
using System.Linq;
using AssetProvider.Infrastructure;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityFx.Async;
using UnityFx.Async.Extensions;
using UnityFx.Async.Promises;
using Object = UnityEngine.Object;


namespace AssetProvider.Implementations
{
	public class AddressablesAssetProvider : IAssetProvider
	{
		private readonly Dictionary<string, object> _cache = new Dictionary<string, object>();
		private readonly Dictionary<string, HashSet<string>> _cacheLabels = new Dictionary<string, HashSet<string>>();
		private readonly List<AsyncOperationHandle> _handles = new List<AsyncOperationHandle>();

		public IAsyncOperation Initialize()
		{
			return InitializeAddressables();
		}

		public void Dispose()
		{
			foreach (var handle in _handles)
			{
				Addressables.Release(handle);
			}

			_cacheLabels.Clear();
			_cache.Clear();
			_handles.Clear();
		}

		public IAsyncOperation<T> Load<T>(string key) where T : class
		{
			if (string.IsNullOrEmpty(key))
			{
				return AsyncResult.FromException<T>(new Exception("key is null or empty"));
			}

			if (_cache.ContainsKey(key))
			{
				return AsyncResult.FromResult((T) GetCachedItem(key));
			}


			var asyncOperationHandle = Addressables.LoadAssetAsync<T>(key);
			var asyncOperation = asyncOperationHandle.Task.ToAsync();

			_handles.Add(asyncOperationHandle);

			return asyncOperation.Then((result) =>
			{
				AddToCache(result, key);
				return AsyncResult.FromResult(result);
			});
		}

		public IAsyncOperation<IEnumerable<T>> LoadByLabel<T>(string label) where T : class
		{
			if (string.IsNullOrEmpty(label))
			{
				return AsyncResult.FromException<IEnumerable<T>>(new Exception("key is null or empty"));
			}

			if (_cacheLabels.ContainsKey(label))
			{
				return AsyncResult.FromResult(GetCachedItems<T>(label));
			}

			var asyncOperationHandle = Addressables.LoadAssetsAsync<T>(label, e => { });
			var asyncOperation = asyncOperationHandle.Task.ToAsync();

			_handles.Add(asyncOperationHandle);

			return asyncOperation.Then((result) =>
			{
				AddToCache(result, label);
				return AsyncResult.FromResult(result);
			});
		}

		public IAsyncOperation<IEnumerable<T>> Load<T>(IEnumerable<string> keys) where T : class
		{
			return AsyncResult.WhenAll(keys.Select(Load<T>));
		}

		public IAsyncOperation Preload(IEnumerable<string> keys)
		{
			if (keys == null || !keys.Any())
			{
				return AsyncResult.CompletedOperation;
			}

			return Addressables.DownloadDependenciesAsync(keys.Select(o => (object) o).ToList(), Addressables.MergeMode.Union).Task.ToAsync();
		}

		private IAsyncOperation InitializeAddressables()
		{
			var asyncOperation = Addressables.InitializeAsync();
			var asyncResult = asyncOperation.Task.ToAsync();
			return asyncResult;
		}

		private object GetCachedItem(string key)
		{
			if (_cache.TryGetValue(key, out var value))
			{
				return value;
			}

			return null;
		}

		private IEnumerable<T> GetCachedItems<T>(string label)
		{
			if (_cacheLabels.TryGetValue(label, out var listKeysInLabel))
			{
				return _cache.Where(o => listKeysInLabel.Contains(o.Key)).Select(o => (T) o.Value);
			}

			return null;
		}

		private void AddToCache(object obj, string key, string label = "")
		{
			if (_cache.ContainsKey(key))
			{
				_cache[key] = obj;
			}
			else
			{
				_cache.Add(key, obj);
			}

			if (!string.IsNullOrEmpty(label))
			{
				if (_cacheLabels.TryGetValue(label, out var listKeys))
				{
					if (!listKeys.Contains(key))
					{
						listKeys.Add(key);
					}
				}
				else
				{
					_cacheLabels.Add(label, new HashSet<string> {key});
				}
			}
		}

		private void AddToCache(IEnumerable<Object> obj, string label = "Unnamed")
		{
			foreach (var o in obj)
			{
				AddToCache(o, o.name, label);
			}
		}
	}
}