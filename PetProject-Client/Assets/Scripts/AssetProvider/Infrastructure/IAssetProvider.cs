using System;
using System.Collections.Generic;
using UnityFx.Async;

namespace AssetProvider.Infrastructure
{
	public interface IAssetProvider : IDisposable
	{
		IAsyncOperation Initialize();
		IAsyncOperation<T> Load<T>(string key) where T : class;
		IAsyncOperation<IEnumerable<T>> LoadByLabel<T>(string label) where T : class;
		IAsyncOperation<IEnumerable<T>> Load<T>(IEnumerable<string> keys) where T : class;
		IAsyncOperation Preload(IEnumerable<string> keys);
	}
}