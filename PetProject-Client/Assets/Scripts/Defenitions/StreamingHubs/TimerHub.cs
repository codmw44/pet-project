using System;
using System.Threading.Tasks;
using MagicOnion;

namespace Defenitions.StreamingHubs
{
	public interface ITimerHub : IStreamingHub<ITimerHub, ITimerHubReceiver>
	{
		Task SetAsync(TimeSpan interval);
	}

	public interface ITimerHubReceiver
	{
		void OnTick(string message);
	}
}