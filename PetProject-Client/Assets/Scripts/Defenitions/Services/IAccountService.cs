using Defenitions.Models.Auth;
using MagicOnion;
using PetServer.Balancer.Models.Balance;

namespace Defenitions.Services
{
	public interface IAccountService : IService<IAccountService>
	{
		UnaryResult<SignInResponse> SignInAsync(SignInRequestModel signInRequestModel);
		UnaryResult<CurrentUserResponse> GetCurrentUserNameAsync();
		UnaryResult<string> DangerousOperationAsync();
	}
}