using MagicOnion;
using Models.Price;
using Models.Rewards;
using PetServer.Defenitions.Models.Response;

namespace PetServer.Services
{
	public interface IFinanceService : IService<IFinanceService>
	{
		UnaryResult<FinanceOperationResponse> ProcessReward(IReward reward);
		UnaryResult<FinanceOperationResponse> ProcessPrice(IPrice price);
		UnaryResult<FinanceModelResponse> GetFinanceModel();
	}
}