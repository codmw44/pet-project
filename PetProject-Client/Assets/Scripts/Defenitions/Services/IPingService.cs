using MagicOnion;
using PetServer.Defenitions.Models.Response;

namespace PetServer.Services
{
	public interface IPingService : IService<IPingService>
	{
		UnaryResult<bool> PingConnection();
		UnaryResult<bool> LivenessProbe();
		UnaryResult<bool> ReadinessProbe();
	}
}