using MagicOnion;
using PetServer.Balancer.Models.Balance;
using PetServer.Defenitions.Models.Response;

namespace PetServer.Services
{
    public interface IMigrationService : IService<IMigrationService>
    {
        UnaryResult<Response> IsValidMigrationVersion(string userId, ClientSpecModel clientSpecModel);
        UnaryResult<Response> Migrate(string userId, ClientSpecModel clientSpecModel);
    }
}