using MessagePack;

namespace Models.Rewards
{
	[MessagePackObject]
	public class HardCurrencyReward : IReward
	{
		[Key(0)] public int Amount;

		public HardCurrencyReward()
		{
		}

		public HardCurrencyReward(int amount)
		{
			Amount = amount;
		}
	}
}