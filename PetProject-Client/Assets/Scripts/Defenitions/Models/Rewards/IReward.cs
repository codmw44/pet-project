using MessagePack;

namespace Models.Rewards
{
	[Union(0, typeof(FreeReward))]
	[Union(1, typeof(HardCurrencyReward))]
	[Union(2, typeof(InappReward))]
	[Union(3, typeof(RewardedVideoReward))]
	[Union(4, typeof(SoftCurrencyReward))]
	[Union(5, typeof(PackReward))]
	public interface IReward //Interface changed to class because  any abstract classes or interface broke swagger with  readiness probe
	{
	}

	[MessagePackObject]
	public class InappReward : IReward
	{
	}

	[MessagePackObject]
	public class RewardedVideoReward : IReward
	{
	}

	[MessagePackObject]
	public class FreeReward : IReward
	{
	}
}