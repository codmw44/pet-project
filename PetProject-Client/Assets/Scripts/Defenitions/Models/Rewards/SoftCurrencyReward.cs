using MessagePack;

namespace Models.Rewards
{
	[MessagePackObject()]
	public class SoftCurrencyReward : IReward
	{
		public SoftCurrencyReward()
		{
		}

		public SoftCurrencyReward(int amount)
		{
			Amount = amount;
		}

		[Key(0)] public int Amount;
	}
}