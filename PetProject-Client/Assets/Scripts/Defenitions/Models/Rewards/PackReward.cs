using MessagePack;

namespace Models.Rewards
{
	[MessagePackObject]
	public class PackReward : IReward
	{
		public PackReward()
		{
		}

		public PackReward(IReward[] rewards)
		{
			Rewards = rewards;
		}

		[Key(0)] public IReward[] Rewards = null!;
		[Key(1)] public string Term = null!;
	}
}