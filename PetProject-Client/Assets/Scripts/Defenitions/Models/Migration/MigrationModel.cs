using MessagePack;

namespace PetServer.Models.Migration
{
    [MessagePackObject()]
    public class MigrationModel
    {
        [Key(0)] public int MigrationVersion;
    }
}