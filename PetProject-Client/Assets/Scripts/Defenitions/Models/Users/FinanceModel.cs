using System;
using MessagePack;

namespace PetServer.Models.Users
{
	[MessagePackObject()]
	public class FinanceModel
	{
		public bool Equals(FinanceModel other)
		{
			return Hard == other.Hard && Soft == other.Soft;
		}

		public override bool Equals(object? obj)
		{
			return obj is FinanceModel other && Equals(other);
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(Hard, Soft);
		}

		[Key(1)] public int Hard;
		[Key(2)] public int Soft;
	}
}