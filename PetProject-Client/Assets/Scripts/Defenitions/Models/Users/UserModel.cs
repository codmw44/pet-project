using Defenitions.Models.Auth;
using MessagePack;
#if !UNITY_2017_1_OR_NEWER
using MongoDB.Bson.Serialization.Attributes;
#endif
using PetServer.Models.Migration;

namespace PetServer.Models.Users
{
    [MessagePackObject(true)]
    public class UserModel
    {
#if !UNITY_2017_1_OR_NEWER
        [BsonId]
#endif
        public string Id;

        public AuthModel AuthModel;
        public FinanceModel FinanceModel;
        public MigrationModel MigrationModel;
    }
}