using MessagePack;
using PetServer.Balancer.Models.Balance;

namespace Defenitions.Models.Auth
{
	[MessagePackObject(true)]
	public class SignInRequestModel
	{
		public string DeviceId;
		public ClientSpecModel ClientSpecModel;
		public AttachedCustomSignInModel? CustomSignInRequestModel = null;
	}
}