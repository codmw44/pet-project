using System;
using System.Collections.Generic;
using MessagePack;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Defenitions.Models.Auth
{
	[MessagePackObject(true)]
	public class AuthModel
	{
		public string DeviceId { get; set; }
		public string? Role { get; set; }
		public AttachedCustomSignInModel[]? AttachedSignIns { get; set; }

		public override string ToString()
		{
			return $"deviceId: {DeviceId} Role: {Role} AttachedSignIns.Count: {AttachedSignIns?.Length}";
		}
	}

	[MessagePackObject(true)]
	public class AttachedCustomSignInModel : IEquatable<AttachedCustomSignInModel>
	{
		[BsonRepresentation(BsonType.String)] public SignInEnum SignInType { get; set; }
		public string Id { get; set; }

		public override string ToString()
		{
			return $"{SignInType.ToString()}_{Id}";
		}

		public bool Equals(AttachedCustomSignInModel? other)
		{
			if (ReferenceEquals(null, other))
			{
				return false;
			}

			if (ReferenceEquals(this, other))
			{
				return true;
			}

			return SignInType == other.SignInType && Id == other.Id;
		}

		public override bool Equals(object? obj)
		{
			if (ReferenceEquals(null, obj))
			{
				return false;
			}

			if (ReferenceEquals(this, obj))
			{
				return true;
			}

			if (obj.GetType() != this.GetType())
			{
				return false;
			}

			return Equals((AttachedCustomSignInModel)obj);
		}

		public override int GetHashCode()
		{
			return HashCode.Combine((int)SignInType, Id);
		}
	}

	public enum SignInEnum
	{
		FacebookId = 1,
		GameCenterId = 2,
		PlayGoogleId = 3
	}
}