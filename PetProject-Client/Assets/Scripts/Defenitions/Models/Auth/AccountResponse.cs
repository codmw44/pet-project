using System;
using MessagePack;
using PetServer.Defenitions.Models.Response;

namespace Defenitions.Models.Auth
{
	[MessagePackObject(true)]
	public class SignInResponse : Response
	{
		public string UserId { get; set; }
		public string Name { get; set; }
		public byte[] Token { get; set; }
		public DateTimeOffset Expiration { get; set; }

		public static SignInResponse Failed { get; } = new SignInResponse() {ErrorEnum = ResponseErrorEnum.SignInFailed};

		public SignInResponse()
		{
		}

		public SignInResponse(string userId, string name, byte[] token, DateTimeOffset expiration)
		{
			UserId = userId;
			Name = name;
			Token = token;
			Expiration = expiration;
		}
	}

	[MessagePackObject(true)]
	public class CurrentUserResponse
	{
		public static CurrentUserResponse Anonymous { get; } = new CurrentUserResponse() {IsAuthenticated = false, Name = "Anonymous"};

		public bool IsAuthenticated { get; set; }
		public string Name { get; set; }
		public string UserId { get; set; }
	}
}