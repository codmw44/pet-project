using MessagePack;

namespace Models.Price
{
	[MessagePackObject]
	public class SoftCurrencyPrice : IPrice
	{
		public SoftCurrencyPrice()
		{
		}

		public SoftCurrencyPrice(int amount)
		{
			Amount = amount;
		}

		[Key(0)] public int Amount;
	}
}