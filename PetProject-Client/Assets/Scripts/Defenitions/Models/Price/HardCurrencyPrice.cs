using MessagePack;

namespace Models.Price
{
	[MessagePackObject]
	public class HardCurrencyPrice : IPrice
	{
		public HardCurrencyPrice()
		{
		}

		public HardCurrencyPrice(int amount)
		{
			Amount = amount;
		}

		[Key(0)] public int Amount;
	}
}