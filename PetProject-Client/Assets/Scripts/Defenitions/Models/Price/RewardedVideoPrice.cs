using MessagePack;

namespace Models.Price
{
	[MessagePackObject]
	public class RewardedVideoPrice : IPrice
	{
		public RewardedVideoPrice(string idPlace)
		{
			IdPlace = idPlace;
		}

		[Key(0)] public string IdPlace;
	}
}