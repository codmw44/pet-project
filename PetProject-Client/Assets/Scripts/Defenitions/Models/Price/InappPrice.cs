using MessagePack;

namespace Models.Price
{
	[MessagePackObject]
	public class InappPrice : IPrice
	{
		private const string NoProduct = "<None>";

		public InappPrice()
		{
		}

		public InappPrice(string productId)
		{
			ProductId = productId;
		}

		[Key(0)] public string ProductId = NoProduct;
	}
}