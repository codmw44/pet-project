using MessagePack;

namespace Models.Price
{
	[Union(0, typeof(FreePrice))]
	[Union(1, typeof(HardCurrencyPrice))]
	[Union(2, typeof(SoftCurrencyPrice))]
	[Union(3, typeof(InappPrice))]
	[Union(4, typeof(RewardedVideoPrice))]
	public interface IPrice //Interface changed to class because  any abstract classes or interface broke swagger with  readiness probe
	{
	}
}