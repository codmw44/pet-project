using MessagePack;

namespace PetServer.Defenitions.Models.Response
{
	[MessagePackObject]
	public class FinanceOperationResponse : Response
	{
		[Key(1)] public bool IsSucceed;
	}
}