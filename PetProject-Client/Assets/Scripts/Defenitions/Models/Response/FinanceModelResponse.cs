using MessagePack;
using PetServer.Models.Users;

namespace PetServer.Defenitions.Models.Response
{
	[MessagePackObject()]
	public class FinanceModelResponse : Response
	{
		[Key(1)] public FinanceModel FinanceModel;
	}
}