namespace PetServer.Defenitions.Models.Response
{
	public enum ResponseErrorEnum
	{
		None,
		SessionError,
		ServerShutdown,
		InternalError,
		DontEnoughFinance,
		RequireUpdateClient,
		ServerOnService,
		DontExistUserId,
		SignInFailed,
		InvalidArgs,
	}
}