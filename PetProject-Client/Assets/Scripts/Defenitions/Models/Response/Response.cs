using Defenitions.Models.Auth;
using MessagePack;
using PetServer.Balancer.Models.Balance;

namespace PetServer.Defenitions.Models.Response
{
	[Union(0, typeof(FinanceModelResponse))]
	[Union(1, typeof(FinanceOperationResponse))]
	[Union(4, typeof(BalanceResponse))]
	[Union(6, typeof(SignInResponse))]
	[MessagePackObject]
	public class Response
	{
		[Key(0)] public ResponseErrorEnum ErrorEnum;

		[IgnoreMember] public bool HasError => ErrorEnum != ResponseErrorEnum.None;
	}
}