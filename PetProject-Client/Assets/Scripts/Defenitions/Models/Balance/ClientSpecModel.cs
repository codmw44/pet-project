using MessagePack;

namespace PetServer.Balancer.Models.Balance
{
	[MessagePackObject(true)]
	public class ClientSpecModel
	{
		public string ClientVersion;
		public bool IsDevelopmentBuild;
	}
}