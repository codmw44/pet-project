using System.Collections.Generic;
using MessagePack;
using PetServer.Defenitions.Models.Response;

namespace PetServer.Balancer.Models.Balance
{
    [MessagePackObject()]
    public class BalanceResponse : Response
    {
        [Key(1)] public Dictionary<ChannelType, string> ServerAddresses;
    }
}