using System;

namespace Ecs.Serialization.Infrastructure
{
	public class LookupInfo
	{
		public Type ContextType;
		public Type LookupType;
	}
}