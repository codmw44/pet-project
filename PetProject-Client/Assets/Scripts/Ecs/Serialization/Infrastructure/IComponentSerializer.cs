using System;
using Entitas;

namespace Ecs.Serialization.Infrastructure
{
	public interface IComponentSerializer
	{
		object Serialize(IComponent component);
		IComponent Deserialize(object componentData, Type componentType);
	}
}