using System;
using System.Collections.Generic;

namespace Ecs.Serialization.Infrastructure
{
	public class ContextData
	{
		public List<List<ComponentData>> Entities;
		public Type[] Types;
	}
}