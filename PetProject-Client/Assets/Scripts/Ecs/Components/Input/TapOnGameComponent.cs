using Ecs.Components.Game.Infrastructure;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

namespace Ecs.Components.Input
{
	[Input, Unique]
	public class TapOnGameComponent : ValueComponent<Vector2>
	{
	}
}