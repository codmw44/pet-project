using System;
using Ecs.Components.Game.Infrastructure;
using Entitas.CodeGeneration.Attributes;

namespace Ecs.Components.Global.Player
{
	[Global, Unique, Serializable]
	public class HardCurrencyComponent : ValueComponent<int>
	{
	}
}