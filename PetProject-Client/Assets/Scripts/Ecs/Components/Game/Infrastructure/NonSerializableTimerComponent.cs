using Entitas;

namespace Ecs.Components.Game.Infrastructure
{
	[Game]
	public class NonSerializableTimerComponent : IComponent
	{
		public float Current;
		public float Max;
	}
}