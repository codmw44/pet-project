using Entitas;

namespace Ecs.Components.Game.Infrastructure
{
	public abstract class ValueComponent<T> : IComponent
	{
		public T Value;
	}
}