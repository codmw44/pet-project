using System;
using Entitas;

namespace Ecs.Components.Game.Infrastructure
{
	[Game, Serializable]
	public class TimerComponent : IComponent
	{
		public float Current;
		public float Max;
	}
}