using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace Ecs.Components.Game.Infrastructure
{
	[Game, Unique]
	public class TickComponent : IComponent
	{
		public float DeltaTime;
	}
}