using System;
using Entitas.CodeGeneration.Attributes;

namespace Ecs.Components.Game.Infrastructure
{
	[Game, Unique, Serializable]
	public class SerializationTimeComponent : ValueComponent<string>
	{
	}
}