using Ecs.Components.Game.Infrastructure;
using Entitas;

namespace Ecs.Components.Game.Infrastructure
{
	public interface CleanupEntity : IEntity, ICleanupEntity
	{
	}
}

public partial class GameEntity : CleanupEntity
{
}

public partial class InputEntity : CleanupEntity
{
}