using Entitas;
using Entitas.CodeGeneration.Attributes;
using Models.Levels;

namespace Ecs.Components.Game.Levels
{
    [Game, Unique]
    public class LevelModelComponent : IComponent
    {
        public LevelModel Model;
    }
}