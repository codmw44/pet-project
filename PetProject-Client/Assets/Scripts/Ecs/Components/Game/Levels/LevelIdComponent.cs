using System;
using Ecs.Components.Game.Infrastructure;
using Entitas.CodeGeneration.Attributes;

namespace Ecs.Components.Game.Levels
{
    [Game, Unique, Serializable]
    public class LevelIdComponent : ValueComponent<string>
    {
    }
}