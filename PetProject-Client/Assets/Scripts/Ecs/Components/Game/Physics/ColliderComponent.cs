using Entitas;
using UnityEngine;

namespace Ecs.Components.Game.Physics
{
	[Game]
	public class ColliderComponent : IComponent
	{
		public Collider Collider;
	}
}