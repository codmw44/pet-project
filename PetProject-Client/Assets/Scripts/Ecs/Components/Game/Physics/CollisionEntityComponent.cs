using Entitas;

namespace Ecs.Components.Game.Physics
{
	[Game]
	public class CollisionEntityComponent : IComponent
	{
		public GameEntity Entity;
	}
}