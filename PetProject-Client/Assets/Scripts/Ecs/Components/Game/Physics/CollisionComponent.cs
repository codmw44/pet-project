using Entitas;
using UnityEngine;

namespace Ecs.Components.Game.Physics
{
	[Game]
	public class CollisionComponent : IComponent
	{
		public Collision Collision;
	}
}