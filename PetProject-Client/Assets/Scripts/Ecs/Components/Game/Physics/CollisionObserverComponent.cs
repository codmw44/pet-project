using Controllers.Physics;
using Entitas;

namespace Ecs.Components.Game.Physics
{
	[Game]
	public class CollisionObserverComponent : IComponent
	{
		public CollisionObserver Observer;
	}
}