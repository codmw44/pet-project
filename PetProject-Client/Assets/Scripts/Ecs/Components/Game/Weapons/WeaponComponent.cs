using System;
using Entitas;

namespace Ecs.Components.Game.Weapons
{
    [Game, Serializable]
    public class WeaponComponent : IComponent
    {
        public string ViewId;
        public float ShootsPerSecond;
    }
}