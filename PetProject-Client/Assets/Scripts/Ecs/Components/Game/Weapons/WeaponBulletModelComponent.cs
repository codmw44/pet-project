using System;
using Entitas;
using Models.Weapons.Bullets;

namespace Ecs.Components.Game.Weapons
{
    [Game, Serializable]
    public class WeaponBulletModelComponent : IComponent
    {
        public string ViewId;
        public int Damage;
        public float Speed;
    }
}