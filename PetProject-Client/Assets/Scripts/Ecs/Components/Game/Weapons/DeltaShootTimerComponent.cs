using System;
using Entitas;

namespace Ecs.Components.Game.Weapons
{
    [Game, Serializable]
    public class DeltaShootTimerComponent : IComponent
    {
        public float Current;
        public float Max;
    }
}