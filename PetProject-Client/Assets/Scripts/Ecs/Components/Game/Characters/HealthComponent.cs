using System;
using Entitas;

namespace Ecs.Components.Game.Characters
{
    [Game, Serializable]
    public class HealthComponent : IComponent
    {
        public int Current;
        public int Max;
    }
}