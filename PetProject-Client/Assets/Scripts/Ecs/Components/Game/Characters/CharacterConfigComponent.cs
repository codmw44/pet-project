using Entitas;
using Models.Characters.Infrastructure;

namespace Ecs.Components.Game.Characters
{
    [Game]
    public class CharacterConfigComponent : IComponent
    {
        public BaseCharacterModel Model;
    }
}