using Entitas;
using UnityEngine;

namespace Ecs.Components.Game.Characters
{
	[Game]
	public class CharacterViewComponent : IComponent
	{
		public GameObject View;
		public CharacterController CharacterController;
	}
}