using System;
using Entitas;

namespace Ecs.Components.Game.Characters
{
    [Game, Serializable]
    public class CharacterComponent : IComponent
    {
        public string ViewId;
    }
}