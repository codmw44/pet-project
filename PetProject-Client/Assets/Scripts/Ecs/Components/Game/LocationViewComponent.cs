using Controllers.Location;
using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace Ecs.Components.Game
{
    [Game, Unique]
    public class LocationViewComponent : IComponent
    {
        public LocationController Value;
    }
}