using System;
using Entitas;
using UnityEngine;

namespace Ecs.Components.Game.Transform
{
    [Game, Serializable]
    public class MoveComponent : IComponent
    {
        public Vector3 Direction;
    }
}