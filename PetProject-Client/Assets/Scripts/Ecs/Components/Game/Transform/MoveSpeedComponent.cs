using System;
using Ecs.Components.Game.Infrastructure;

namespace Ecs.Components.Game.Transform
{
	[Game, Serializable]
	public class MoveSpeedComponent : ValueComponent<float>
	{
	}
}