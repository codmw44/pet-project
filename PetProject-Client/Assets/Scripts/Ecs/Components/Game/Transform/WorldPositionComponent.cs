using System;
using Ecs.Components.Game.Infrastructure;
using UnityEngine;

namespace Ecs.Components.Game.Transform
{
    [Game, Serializable]
    public class WorldPositionComponent : ValueComponent<Vector3>
    {
    }
}