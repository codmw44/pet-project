using Ecs.Components.Game.Infrastructure;
using UnityEngine;

namespace Ecs.Components.Game.Transform
{
    [Game]
    public class GridPositionComponent : ValueComponent<Vector2Int>
    {
    }
}