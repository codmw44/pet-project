using System;
using Entitas;
using UnityEngine;

namespace Ecs.Components.Game.Transform
{
    [Game, Serializable]
    public class WorldRotationComponent : IComponent
    {
        public Vector3 Rotation;
    }
}