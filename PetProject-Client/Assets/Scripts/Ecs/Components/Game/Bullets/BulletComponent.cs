using System;
using Entitas;
using Models.Weapons.Bullets;

namespace Ecs.Components.Game.Bullets
{
    [Game, Serializable]
    public class BulletComponent : IComponent
    {
        public string ViewId;
    }
}