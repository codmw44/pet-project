using Entitas;
using UnityEngine;

namespace Ecs.Components.Game.Bullets
{
    [Game]
    public class BulletViewComponent : IComponent
    {
        public GameObject View;
    }
}