using System;
using Ecs.Components.Game.Infrastructure;

namespace Ecs.Components.Game.Bullets
{
    [Game, Serializable]
    public class DamageComponent : ValueComponent<int>
    {
    }
}