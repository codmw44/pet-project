//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public Ecs.Components.Game.Physics.CollisionEntityComponent collisionEntity { get { return (Ecs.Components.Game.Physics.CollisionEntityComponent)GetComponent(GameComponentsLookup.CollisionEntity); } }
    public bool hasCollisionEntity { get { return HasComponent(GameComponentsLookup.CollisionEntity); } }

    public void AddCollisionEntity(GameEntity newEntity) {
        var index = GameComponentsLookup.CollisionEntity;
        var component = (Ecs.Components.Game.Physics.CollisionEntityComponent)CreateComponent(index, typeof(Ecs.Components.Game.Physics.CollisionEntityComponent));
        component.Entity = newEntity;
        AddComponent(index, component);
    }

    public void ReplaceCollisionEntity(GameEntity newEntity) {
        var index = GameComponentsLookup.CollisionEntity;
        var component = (Ecs.Components.Game.Physics.CollisionEntityComponent)CreateComponent(index, typeof(Ecs.Components.Game.Physics.CollisionEntityComponent));
        component.Entity = newEntity;
        ReplaceComponent(index, component);
    }

    public void RemoveCollisionEntity() {
        RemoveComponent(GameComponentsLookup.CollisionEntity);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherCollisionEntity;

    public static Entitas.IMatcher<GameEntity> CollisionEntity {
        get {
            if (_matcherCollisionEntity == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.CollisionEntity);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherCollisionEntity = matcher;
            }

            return _matcherCollisionEntity;
        }
    }
}
