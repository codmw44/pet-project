//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    static readonly Ecs.Components.Game.Characters.PlayerTargetComponent playerTargetComponent = new Ecs.Components.Game.Characters.PlayerTargetComponent();

    public bool isPlayerTarget {
        get { return HasComponent(GameComponentsLookup.PlayerTarget); }
        set {
            if (value != isPlayerTarget) {
                var index = GameComponentsLookup.PlayerTarget;
                if (value) {
                    var componentPool = GetComponentPool(index);
                    var component = componentPool.Count > 0
                            ? componentPool.Pop()
                            : playerTargetComponent;

                    AddComponent(index, component);
                } else {
                    RemoveComponent(index);
                }
            }
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherPlayerTarget;

    public static Entitas.IMatcher<GameEntity> PlayerTarget {
        get {
            if (_matcherPlayerTarget == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.PlayerTarget);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherPlayerTarget = matcher;
            }

            return _matcherPlayerTarget;
        }
    }
}
