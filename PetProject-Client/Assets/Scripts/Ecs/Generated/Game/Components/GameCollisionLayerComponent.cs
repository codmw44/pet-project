//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public Ecs.Components.Game.Physics.CollisionLayerComponent collisionLayer { get { return (Ecs.Components.Game.Physics.CollisionLayerComponent)GetComponent(GameComponentsLookup.CollisionLayer); } }
    public bool hasCollisionLayer { get { return HasComponent(GameComponentsLookup.CollisionLayer); } }

    public void AddCollisionLayer(int newValue) {
        var index = GameComponentsLookup.CollisionLayer;
        var component = (Ecs.Components.Game.Physics.CollisionLayerComponent)CreateComponent(index, typeof(Ecs.Components.Game.Physics.CollisionLayerComponent));
        component.Value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceCollisionLayer(int newValue) {
        var index = GameComponentsLookup.CollisionLayer;
        var component = (Ecs.Components.Game.Physics.CollisionLayerComponent)CreateComponent(index, typeof(Ecs.Components.Game.Physics.CollisionLayerComponent));
        component.Value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveCollisionLayer() {
        RemoveComponent(GameComponentsLookup.CollisionLayer);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherCollisionLayer;

    public static Entitas.IMatcher<GameEntity> CollisionLayer {
        get {
            if (_matcherCollisionLayer == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.CollisionLayer);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherCollisionLayer = matcher;
            }

            return _matcherCollisionLayer;
        }
    }
}
