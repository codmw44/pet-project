//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentContextApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class InputContext {

    public InputEntity tapOnGameEntity { get { return GetGroup(InputMatcher.TapOnGame).GetSingleEntity(); } }
    public Ecs.Components.Input.TapOnGameComponent tapOnGame { get { return tapOnGameEntity.tapOnGame; } }
    public bool hasTapOnGame { get { return tapOnGameEntity != null; } }

    public InputEntity SetTapOnGame(UnityEngine.Vector2 newValue) {
        if (hasTapOnGame) {
            throw new Entitas.EntitasException("Could not set TapOnGame!\n" + this + " already has an entity with Ecs.Components.Input.TapOnGameComponent!",
                "You should check if the context already has a tapOnGameEntity before setting it or use context.ReplaceTapOnGame().");
        }
        var entity = CreateEntity();
        entity.AddTapOnGame(newValue);
        return entity;
    }

    public void ReplaceTapOnGame(UnityEngine.Vector2 newValue) {
        var entity = tapOnGameEntity;
        if (entity == null) {
            entity = SetTapOnGame(newValue);
        } else {
            entity.ReplaceTapOnGame(newValue);
        }
    }

    public void RemoveTapOnGame() {
        tapOnGameEntity.Destroy();
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class InputEntity {

    public Ecs.Components.Input.TapOnGameComponent tapOnGame { get { return (Ecs.Components.Input.TapOnGameComponent)GetComponent(InputComponentsLookup.TapOnGame); } }
    public bool hasTapOnGame { get { return HasComponent(InputComponentsLookup.TapOnGame); } }

    public void AddTapOnGame(UnityEngine.Vector2 newValue) {
        var index = InputComponentsLookup.TapOnGame;
        var component = (Ecs.Components.Input.TapOnGameComponent)CreateComponent(index, typeof(Ecs.Components.Input.TapOnGameComponent));
        component.Value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceTapOnGame(UnityEngine.Vector2 newValue) {
        var index = InputComponentsLookup.TapOnGame;
        var component = (Ecs.Components.Input.TapOnGameComponent)CreateComponent(index, typeof(Ecs.Components.Input.TapOnGameComponent));
        component.Value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveTapOnGame() {
        RemoveComponent(InputComponentsLookup.TapOnGame);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class InputMatcher {

    static Entitas.IMatcher<InputEntity> _matcherTapOnGame;

    public static Entitas.IMatcher<InputEntity> TapOnGame {
        get {
            if (_matcherTapOnGame == null) {
                var matcher = (Entitas.Matcher<InputEntity>)Entitas.Matcher<InputEntity>.AllOf(InputComponentsLookup.TapOnGame);
                matcher.componentNames = InputComponentsLookup.componentNames;
                _matcherTapOnGame = matcher;
            }

            return _matcherTapOnGame;
        }
    }
}
