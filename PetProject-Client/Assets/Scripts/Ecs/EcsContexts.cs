using System.Diagnostics;
using Entitas;
using Entitas.VisualDebugging.Unity;

namespace Ecs
{
	public class EcsContexts : IContexts
	{
		public InputContext InputContext { get; private set; }
		public GameContext GameContext { get; private set; }
		public LobbyContext LobbyContext { get; private set; }
		public GlobalContext GlobalContext { get; private set; }
		public IContext[] allContexts => new IContext[] {InputContext, GameContext, LobbyContext, GlobalContext};

		public void Initialize()
		{
			InputContext = new InputContext();
			GameContext = new GameContext();
			LobbyContext = new LobbyContext();
			GlobalContext = new GlobalContext();
		}

		[Conditional("UNITY_EDITOR")]
		public void InitializeObserver()
		{
			new ContextObserver(InputContext);
			new ContextObserver(GameContext);
			new ContextObserver(LobbyContext);
			new ContextObserver(GlobalContext);
		}
	}
}