using System;
using System.Collections.Generic;
using Entitas;
using Zenject;

namespace Ecs
{
	public class EcsRunner : ITickable, IDisposable
	{
		private readonly Dictionary<string, SystemsContainer> _systems;
		private readonly SystemsContainer[] _systemsContainer;
		private readonly int _systemsCount;

		private bool _disposed = true;

		public EcsRunner(Dictionary<string, List<ISystem>> systems)
		{
			_systemsCount = systems.Count;
			_systemsContainer = new SystemsContainer[_systemsCount];
			_systems = new Dictionary<string, SystemsContainer>(_systemsCount);
			var i = 0;
			foreach (var systemsKey in systems.Keys)
			{
				_systemsContainer[i] = new SystemsContainer(systemsKey, systems[systemsKey]);
				_systems[systemsKey] = _systemsContainer[i];
				i++;
			}
		}

		public void Dispose()
		{
			if (_disposed)
			{
				return;
			}

			SetAvailable(false);

			for (var i = 0; i < _systemsCount; ++i)
			{
				_systemsContainer[i].Dispose();
			}
		}

		public void Tick()
		{
			if (_disposed)
			{
				return;
			}

			for (var i = 0; i < _systemsCount; ++i)
			{
				_systemsContainer[i].Update();
			}
		}

		public void Initialize()
		{
			for (var i = 0; i < _systemsCount; ++i)
			{
				_systemsContainer[i].Initialize();
			}
		}

		public void SetAvailable(bool state)
		{
			var isAvailable = !_disposed;
			if (isAvailable == state)
			{
				return;
			}

			_disposed = !state;
		}

		public SystemsContainer GetSystems(string key)
		{
			if (!_systems.TryGetValue(key, out var systemsContainer))
			{
				return null;
			}

			return systemsContainer;
		}

		public class SystemsContainer
		{
			public SystemsContainer(string key, List<ISystem> systems)
			{
				Key = key;

				Systems = new Feature(Key);

				var systemsCount = systems.Count;
				for (var i = 0; i < systemsCount; i++)
				{
					var system = systems[i];
					Systems.Add(system);
				}
			}

			public string Key { get; }

			public Feature Systems { get; }

			public void Initialize()
			{
				Systems.Initialize();
			}

			public void Update()
			{
				Systems.Execute();
				Systems.Cleanup();
			}

			public void Dispose()
			{
				Systems.TearDown();
				Systems.DeactivateReactiveSystems();
				Systems.ClearReactiveSystems();
			}
		}
	}
}