using Entitas;

namespace Ecs.Systems.Game
{
	public class MoveSystem : IExecuteSystem
	{
		private readonly GameContext _context;
		private IGroup<GameEntity> _group;

		public MoveSystem(GameContext context)
		{
			_context = context;
			_group = context.GetGroup(GameMatcher.AllOf(GameMatcher.Move, GameMatcher.WorldPosition));
		}

		public void Execute()
		{
			foreach (var gameEntity in _group.AsEnumerable())
			{
				var direction = gameEntity.move.Direction;
				if (gameEntity.hasMoveSpeed)
				{
					direction *= gameEntity.moveSpeed.Value;
				}

				direction *= _context.tick.DeltaTime;

				gameEntity.ReplaceWorldPosition(gameEntity.worldPosition.Value + direction);
			}
		}
	}
}