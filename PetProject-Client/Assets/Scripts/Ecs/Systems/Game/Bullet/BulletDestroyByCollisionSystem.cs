using System.Collections.Generic;
using Entitas;

namespace Ecs.Systems.Game.Bullet
{
	public class BulletDestroyByCollisionSystem : ReactiveSystem<GameEntity>
	{
		public BulletDestroyByCollisionSystem(GameContext context) : base(context)
		{
		}

		protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
		{
			return context.CreateCollector(GameMatcher.Collision);
		}

		protected override bool Filter(GameEntity entity)
		{
			return entity.hasBullet && entity.hasCollision;
		}

		protected override void Execute(List<GameEntity> entities)
		{
			foreach (var gameEntity in entities)
			{
				gameEntity.isCleanup = true;
			}
		}
	}
}