using System.Collections.Generic;
using AssetProvider.Infrastructure;
using Entitas;
using UnityEngine;
using UnityFx.Async.Promises;

namespace Ecs.Systems.Game.Bullet
{
    public class BulletRenderSystem : ReactiveSystem<GameEntity>
    {
        private readonly IAssetProvider _assetProvider;


        public BulletRenderSystem(GameContext context, IAssetProvider assetProvider) : base(context)
        {
            _assetProvider = assetProvider;
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Bullet.Added());
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasBullet && !entity.hasBulletView && entity.hasWorldPosition && entity.hasWorldRotation;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var gameEntity in entities)
            {
                _assetProvider.Load<GameObject>(gameEntity.bullet.ViewId).Done(prefab =>
                {
                    var go = GameObject.Instantiate(prefab, gameEntity.worldPosition.Value + Vector3.up, Quaternion.Euler(gameEntity.worldRotation.Rotation));
                    gameEntity.AddBulletView(go);
                    gameEntity.AddCollider(go.GetComponentInChildren<Collider>());
                    gameEntity.AddRigidbodyView(go.GetComponentInChildren<Rigidbody>());

                    if (gameEntity.hasCollisionLayer)
                    {
                        go.layer = gameEntity.collisionLayer.Value;
                    }
                });
            }
        }
    }
}