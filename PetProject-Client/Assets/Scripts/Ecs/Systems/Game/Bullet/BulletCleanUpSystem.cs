using Ecs.Components.Game.Bullets;
using Entitas;
using UnityEngine;

namespace Ecs.Systems.Game.Bullet
{
	public class BulletCleanUpSystem : IInitializeSystem, ITearDownSystem
	{
		private IGroup<GameEntity> _viewGroup;

		public BulletCleanUpSystem(GameContext context)
		{
			_viewGroup = context.GetGroup(GameMatcher.BulletView);
		}

		public void Initialize()
		{
			_viewGroup.OnEntityRemoved += OnViewRemoved;
		}

		public void TearDown()
		{
			_viewGroup.OnEntityRemoved -= OnViewRemoved;
		}

		private void OnViewRemoved(IGroup<GameEntity> @group, GameEntity entity, int index, IComponent component)
		{
			var view = (BulletViewComponent) component;
			GameObject.Destroy(view.View.gameObject);
		}
	}
}