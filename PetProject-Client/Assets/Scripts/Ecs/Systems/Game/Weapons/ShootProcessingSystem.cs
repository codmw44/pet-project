using System.Collections.Generic;
using Entitas;
using Models.Weapons;
using Models.Weapons.Infrastructure;
using UnityEngine;

namespace Ecs.Systems.Game.Weapons
{
    public class ShootProcessingSystem : ReactiveSystem<GameEntity>
    {
        private static int PlayerLayer => LayerMask.NameToLayer("BulletForEnemies");
        private static int EnemyLayer => LayerMask.NameToLayer("BulletForPlayer");

        private readonly GameContext _context;

        public ShootProcessingSystem(GameContext context) : base(context)
        {
            _context = context;
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.RequiredShoot);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.isRequiredShoot && entity.hasWeapon && entity.hasWorldPosition && entity.hasWorldRotation && entity.hasWeaponBulletModel;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var gameEntity in entities)
            {
                var bullet = _context.CreateEntity();
                var weaponModel = gameEntity.weaponBulletModel;

                var worldRotationRotation = gameEntity.worldRotation.Rotation;

                bullet.AddWorldPosition(gameEntity.worldPosition.Value);
                bullet.AddWorldRotation(worldRotationRotation);
                bullet.AddBullet(weaponModel.ViewId);
                bullet.AddDamage(weaponModel.Damage);
                bullet.AddMoveSpeed(weaponModel.Speed);

                var targetForward = Quaternion.Euler(worldRotationRotation) * Vector3.forward;
                bullet.AddMove(targetForward);

                gameEntity.isRequiredShoot = false;

                if (gameEntity.isPlayer)
                {
                    bullet.AddCollisionLayer(PlayerLayer);
                }
                else if (gameEntity.isEnemy)
                {
                    bullet.AddCollisionLayer(EnemyLayer);
                }
            }
        }
    }
}