using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace Ecs.Systems.Game.Weapons
{
    public class ShootByTimerSystem : ReactiveSystem<GameEntity>
    {
        public ShootByTimerSystem(GameContext context) : base(context)
        {
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.DeltaShootTimer.Removed());
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasWeapon && !entity.hasDeltaShootTimer && !(entity.hasMove && entity.move.Direction != Vector3.zero);
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var gameEntity in entities)
            {
                gameEntity.isRequiredShoot = true;

                var timer = 1f / gameEntity.weapon.ShootsPerSecond;
                gameEntity.ReplaceDeltaShootTimer(timer, timer);
            }
        }
    }
}