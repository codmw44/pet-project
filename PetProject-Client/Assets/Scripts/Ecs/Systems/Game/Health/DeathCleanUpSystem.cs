using System.Collections.Generic;
using Entitas;

namespace DefaultNamespace
{
	public class DeathCleanUpSystem : ReactiveSystem<GameEntity>
	{
		public DeathCleanUpSystem(GameContext context) : base(context)
		{
		}

		protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
		{
			return context.CreateCollector(GameMatcher.Health);
		}

		protected override bool Filter(GameEntity entity)
		{
			return entity.hasHealth;
		}

		protected override void Execute(List<GameEntity> entities)
		{
			foreach (var gameEntity in entities)
			{
				if (gameEntity.health.Current <= 0)
				{
					gameEntity.isCleanup = true;
				}
			}
		}
	}
}