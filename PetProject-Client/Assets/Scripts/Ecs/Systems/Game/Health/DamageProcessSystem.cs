using System.Collections.Generic;
using Entitas;

namespace Ecs.Systems.Game.Health
{
	public class DamageProcessSystem : ReactiveSystem<GameEntity>
	{
		public DamageProcessSystem(GameContext context) : base(context)
		{
		}

		protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
		{
			return context.CreateCollector(GameMatcher.AllOf(GameMatcher.Damage, GameMatcher.CollisionEntity));
		}

		protected override bool Filter(GameEntity entity)
		{
			return entity.hasDamage && entity.hasCollisionEntity;
		}

		protected override void Execute(List<GameEntity> entities)
		{
			foreach (var gameEntity in entities)
			{
				var collisionEntity = gameEntity.collisionEntity.Entity;
				if (collisionEntity.hasHealth)
				{
					var damage = gameEntity.damage.Value;
					collisionEntity.ReplaceHealth(collisionEntity.health.Current - damage, collisionEntity.health.Max);
				}
			}
		}
	}
}