using System.Collections.Generic;
using System.Linq;
using Entitas;
using UnityEngine;

namespace Ecs.Systems.Game.Physics
{
	public class CollisionToEntityProcessSystem : ReactiveSystem<GameEntity>
	{
		private readonly IGroup<GameEntity> _colliderGroup;

		public CollisionToEntityProcessSystem(GameContext context) : base(context)
		{
			_colliderGroup = context.GetGroup(GameMatcher.Collider);
		}

		protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
		{
			return context.CreateCollector(GameMatcher.Collision.AddedOrRemoved());
		}

		protected override bool Filter(GameEntity entity)
		{
			return true;
		}

		protected override void Execute(List<GameEntity> entities)
		{
			foreach (var gameEntity in entities)
			{
				if (gameEntity.hasCollisionEntity && !gameEntity.hasCollision)
				{
					gameEntity.RemoveCollisionEntity();
				}
				else if (gameEntity.hasCollision)
				{
					if (TryGetCollisionEntity(gameEntity, gameEntity.collision.Collision.contacts.First().thisCollider, out var entity))
					{
						gameEntity.ReplaceCollisionEntity(entity);
					}
				}
			}
		}

		private bool TryGetCollisionEntity(GameEntity selfEntity, Collider collider, out GameEntity gameEntity)
		{
			foreach (var entity in _colliderGroup.AsEnumerable())
			{
				if (entity == selfEntity) continue;
				
				if (entity.collider.Collider == collider)
				{
					gameEntity = entity;
					return true;
				}
			}

			gameEntity = null;
			return false;
		}
	}
}