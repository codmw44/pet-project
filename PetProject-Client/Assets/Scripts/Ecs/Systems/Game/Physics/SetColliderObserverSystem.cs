using System.Collections.Generic;
using Controllers.Physics;
using Entitas;

namespace Ecs.Systems.Game.Physics
{
	public class SetColliderObserverSystem : ReactiveSystem<GameEntity>
	{
		public SetColliderObserverSystem(GameContext context) : base(context)
		{
		}

		protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
		{
			return context.CreateCollector(GameMatcher.Collider);
		}

		protected override bool Filter(GameEntity entity)
		{
			return entity.hasCollider && !entity.hasCollisionObserver;
		}

		protected override void Execute(List<GameEntity> entities)
		{
			foreach (var gameEntity in entities)
			{
				 var observer = gameEntity.collider.Collider.gameObject.AddComponent<CollisionObserver>();
				 observer.SetEntity(gameEntity);
				 gameEntity.AddCollisionObserver(observer);
			}
		}
	}
}