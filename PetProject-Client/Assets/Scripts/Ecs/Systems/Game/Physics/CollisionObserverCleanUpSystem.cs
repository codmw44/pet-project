using Ecs.Components.Game.Physics;
using Entitas;
using UnityEngine;

namespace Ecs.Systems.Game.Physics
{
	public class CollisionObserverCleanUpSystem : IInitializeSystem, ITearDownSystem
	{
		private IGroup<GameEntity> _viewGroup;

		public CollisionObserverCleanUpSystem(GameContext context)
		{
			_viewGroup = context.GetGroup(GameMatcher.CollisionObserver);
		}

		public void Initialize()
		{
			_viewGroup.OnEntityRemoved += OnViewRemoved;
		}

		public void TearDown()
		{
			_viewGroup.OnEntityRemoved -= OnViewRemoved;
		}

		private void OnViewRemoved(IGroup<GameEntity> @group, GameEntity entity, int index, IComponent component)
		{
			var view = (CollisionObserverComponent) component;
			GameObject.Destroy(view.Observer);
		}
	}
}