using Entitas;
using UnityEngine;

namespace Ecs.Systems.Game.Character
{
	public class CharacterRotationSystem : IExecuteSystem
	{
		private readonly GameContext _context;
		private IGroup<GameEntity> _characterGroup;

		public CharacterRotationSystem(GameContext context)
		{
			_context = context;
			_characterGroup = context.GetGroup(GameMatcher.AllOf(GameMatcher.CharacterView, GameMatcher.WorldRotation));
		}

		public void Execute()
		{
			foreach (var gameEntity in _characterGroup.AsEnumerable())
			{
				gameEntity.characterView.View.transform.rotation = Quaternion.Lerp(gameEntity.characterView.View.transform.rotation, Quaternion.Euler(gameEntity.worldRotation.Rotation),
					15f * _context.tick.DeltaTime);
			}
		}
	}
}