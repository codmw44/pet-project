using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace Ecs.Systems.Game.Character
{
	public class PlayerRequiredShootSystem : ReactiveSystem<GameEntity>
	{
		public PlayerRequiredShootSystem(GameContext context) : base(context)
		{
		}

		protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
		{
			return context.CreateCollector(GameMatcher.Move.Added());
		}

		protected override bool Filter(GameEntity entity)
		{
			return entity.isPlayer && entity.hasWeapon;
		}

		protected override void Execute(List<GameEntity> entities)
		{
			foreach (var gameEntity in entities)
			{
				if (!gameEntity.hasMove || gameEntity.move.Direction == Vector3.zero)
				{
					if (!gameEntity.hasDeltaShootTimer)
					{
						var timer = 1f / gameEntity.weapon.ShootsPerSecond;
						gameEntity.ReplaceDeltaShootTimer(timer, timer);
					}
				}
				else
				{
					if (gameEntity.hasDeltaShootTimer)
					{
						gameEntity.RemoveDeltaShootTimer();
					}
				}
			}
		}
	}
}