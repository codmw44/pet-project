using AssetProvider.Infrastructure;
using Configs;
using Entitas;
using UnityFx.Async.Promises;

namespace Ecs.Systems.Game.Character
{
	public class InitializeCharactersSystem : IExecuteSystem
	{
		private const string PlayerConfigId = "PlayerCharacterConfig";

		private readonly GameContext _context;
		private readonly IAssetProvider _assetProvider;

		public InitializeCharactersSystem(GameContext context, IAssetProvider assetProvider)
		{
			_context = context;
			_assetProvider = assetProvider;
		}


		public void Execute()
		{
			if (_context.hasLevelModel && _context.isRequiredSetCharacters)
			{
				SetPlayer();
				SetEnemies();

				_context.isRequiredSetCharacters = false;
			}
		}

		private void SetEnemies()
		{
			var model = _context.levelModel.Model;

			foreach (var keyValuePair in model.Enemies)
			{
				var entity = _context.CreateEntity();
				entity.AddCharacterConfig(keyValuePair.Value.Item);
				entity.AddGridPosition(keyValuePair.Key.Pos);
			}
		}

		private void SetPlayer()
		{
			_assetProvider.Load<CharacterHolder>(PlayerConfigId).Done(holder =>
			{
				var entity = _context.CreateEntity();
				entity.AddCharacterConfig(holder.Item);
			});
		}
	}
}