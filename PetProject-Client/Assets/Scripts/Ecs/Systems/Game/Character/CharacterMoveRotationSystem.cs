using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace Ecs.Systems.Game.Character
{
	public class CharacterMoveRotationSystem : ReactiveSystem<GameEntity>
	{
		public CharacterMoveRotationSystem(GameContext context) : base(context)
		{
		}

		protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
		{
			return context.CreateCollector(GameMatcher.Move);
		}

		protected override bool Filter(GameEntity entity)
		{
			return entity.hasMove && entity.hasWorldRotation;
		}

		protected override void Execute(List<GameEntity> entities)
		{
			foreach (var gameEntity in entities)
			{
				var moveDirection = gameEntity.move.Direction;
				if (moveDirection == Vector3.zero)
				{
					continue;
				}

				var rotation = Quaternion.LookRotation(moveDirection);
				gameEntity.ReplaceWorldRotation(rotation.eulerAngles);
			}
		}
	}
}