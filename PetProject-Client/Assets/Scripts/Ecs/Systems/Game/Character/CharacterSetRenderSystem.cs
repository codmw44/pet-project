using System.Linq;
using AssetProvider.Infrastructure;
using Entitas;
using UnityEngine;
using UnityFx.Async;
using UnityFx.Async.Promises;

namespace Ecs.Systems.Game.Character
{
	public class CharacterSetRenderSystem : IExecuteSystem
	{
		private readonly IAssetProvider _assetProvider;
		private readonly IGroup<GameEntity> _characterGroup;

		public CharacterSetRenderSystem(GameContext context, IAssetProvider assetProvider)
		{
			_assetProvider = assetProvider;
			_characterGroup = context.GetGroup(GameMatcher.Character);
		}

		public void Execute()
		{
			foreach (var entity in _characterGroup.AsEnumerable().Where(entity => !entity.hasCharacterView && !entity.isProgress))
			{
				var view = SetCharacterView(entity.character.ViewId);
				view.Done(go =>
				{
					var characterController = go.GetComponentInChildren<CharacterController>();
					entity.AddCharacterView(go, characterController);
					go.transform.position = entity.worldPosition.Value;
					go.transform.rotation = Quaternion.Euler(entity.worldRotation.Rotation);
					entity.isProgress = false;
				});
				entity.isProgress = true;
			}
		}

		private IAsyncOperation<GameObject> SetCharacterView(string viewId)
		{
			return _assetProvider.Load<GameObject>(viewId).Then(go =>
			{
				var spawned = GameObject.Instantiate(go);
				return AsyncResult.FromResult(spawned);
			});
		}
	}
}