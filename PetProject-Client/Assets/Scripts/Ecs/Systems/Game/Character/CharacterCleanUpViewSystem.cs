using Ecs.Components.Game.Characters;
using Entitas;
using UnityEngine;

namespace Ecs.Systems.Game.Character
{
	public class CharacterCleanUpViewSystem : IInitializeSystem, ITearDownSystem
	{
		private IGroup<GameEntity> _viewGroup;

		public CharacterCleanUpViewSystem(GameContext context)
		{
			_viewGroup = context.GetGroup(GameMatcher.CharacterView);
		}

		public void Initialize()
		{
			_viewGroup.OnEntityRemoved += OnViewRemoved;
		}

		public void TearDown()
		{
			_viewGroup.OnEntityRemoved -= OnViewRemoved;
		}

		private void OnViewRemoved(IGroup<GameEntity> @group, GameEntity entity, int index, IComponent component)
		{
			var view = (CharacterViewComponent) component;
			GameObject.Destroy(view.View.gameObject);
		}
	}
}