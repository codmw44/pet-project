using Entitas;

namespace Ecs.Systems.Game.Character
{
	public class CharacterMoveSystem : IExecuteSystem
	{
		private readonly GameContext _context;
		private IGroup<GameEntity> _group;

		public CharacterMoveSystem(GameContext context)
		{
			_context = context;
			_group = context.GetGroup(GameMatcher.AllOf(GameMatcher.Move, GameMatcher.CharacterView));
		}

		public void Execute()
		{
			foreach (var gameEntity in _group.AsEnumerable())
			{
				var direction = gameEntity.move.Direction;
				if (gameEntity.hasMoveSpeed)
				{
					direction *= gameEntity.moveSpeed.Value;
				}

				direction *= _context.tick.DeltaTime;

				gameEntity.characterView.CharacterController.Move(direction);
				gameEntity.ReplaceWorldPosition(gameEntity.characterView.View.transform.position);
			}
		}
	}
}