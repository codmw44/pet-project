using Controllers.Location;
using Entitas;
using Models.Characters;
using Models.Characters.Infrastructure;
using Models.Levels;
using UnityEngine;

namespace Ecs.Systems.Game.Character
{
	public class CharacterSetSystem : IExecuteSystem
	{
		private readonly GameContext _context;
		private IGroup<GameEntity> _characterConfigGroup;

		public CharacterSetSystem(GameContext context)
		{
			_context = context;
			_characterConfigGroup = context.GetGroup(GameMatcher.CharacterConfig);
		}

		public void Execute()
		{
			if (!_context.hasLocationView)
			{
				return;
			}

			var location = _context.locationView.Value;

			foreach (var gameEntity in _characterConfigGroup.AsEnumerable())
			{
				var characterModel = gameEntity.characterConfig.Model;
				GameEntity entity;

				switch (characterModel)
				{
					case EnemyCharacterModel enemyCharacterModel:
						entity = CreateCharacterEntity(characterModel, location, gameEntity.hasGridPosition ? gameEntity.gridPosition.Value : Vector2Int.one);
						entity.isEnemy = true;
						break;
					
					case PlayerCharacterModel playerCharacterModel:
						var cellPos = new Vector2Int(LevelModel.SizeX / 2, 1);
						entity = CreateCharacterEntity(characterModel, location, cellPos);
						entity.isPlayer = true;
						break;
				}


				gameEntity.isCleanup = true;
			}
		}

		private GameEntity CreateCharacterEntity(BaseCharacterModel model, LocationController locationController, Vector2Int cellPos)
		{
			var entity = _context.CreateEntity();
			entity.ReplaceMoveSpeed(model.Speed);
			entity.ReplaceHealth(model.StartHealth, model.StartHealth);
			entity.ReplaceCharacter(model.ViewId);
			entity.ReplaceWeapon(model.WeaponModel.ViewId, model.WeaponModel.ShootsPerSecond);
			var bulletModel = model.WeaponModel.BulletModel;
			entity.AddWeaponBulletModel(bulletModel.ViewId, bulletModel.Damage, bulletModel.Speed);

			entity.AddWorldPosition(locationController.GetCenterCell(cellPos));
			entity.AddWorldRotation(Vector3.zero);
			return entity;
		}
	}
}