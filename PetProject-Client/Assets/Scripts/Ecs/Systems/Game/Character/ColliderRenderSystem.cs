using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace Ecs.Systems.Game.Character
{
	public class ColliderRenderSystem : ReactiveSystem<GameEntity>
	{
		public ColliderRenderSystem(GameContext context) : base(context)
		{
		}

		protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
		{
			return context.CreateCollector(GameMatcher.CharacterView);
		}

		protected override bool Filter(GameEntity entity)
		{
			return entity.hasCharacterView && !entity.hasCollider;
		}

		protected override void Execute(List<GameEntity> entities)
		{
			foreach (var gameEntity in entities)
			{
				gameEntity.AddCollider(gameEntity.characterView.View.GetComponent<Collider>());
			}
		}
	}
}