using System.Linq;
using AssetProvider;
using AssetProvider.Infrastructure;
using Configs;
using Entitas;
using UnityFx.Async.Promises;

namespace Ecs.Systems.Game.Level
{
	public class LevelModelInitializeSystem : IExecuteSystem
	{
		private readonly GameContext _context;
		private readonly IAssetProvider _assetProvider;

		public LevelModelInitializeSystem(GameContext context, IAssetProvider assetProvider)
		{
			_context = context;
			_assetProvider = assetProvider;
		}

		public void Execute()
		{
			if (_context.hasLevelId && !_context.hasLevelModel && !_context.levelIdEntity.isProgress)
			{
				_assetProvider.LoadByLabel<LevelHolder>(AddressablesLabel.Levels.ToString()).Done(holders =>
				{
					var targetHolder = holders.FirstOrDefault(holder => holder.Item.Id == _context.levelId.Value);
					_context.ReplaceLevelModel(targetHolder.Item);
					_context.levelIdEntity.isProgress = false;
				});
				_context.levelIdEntity.isProgress = true;
			}
		}
	}
}