using System.Collections.Generic;
using Entitas;

namespace Ecs.Systems.Game.Infrastructure
{
	public class TimerSystem : ReactiveSystem<GameEntity>
	{
		private readonly GameContext _context;
		private readonly IGroup<GameEntity> _nonSerializableTimerGroup;
		private readonly IGroup<GameEntity> _timerGroup;
		private readonly IGroup<GameEntity> _deltaShootTimerGroup;

		public TimerSystem(GameContext context) : base(context)
		{
			_context = context;
			_timerGroup = context.GetGroup(GameMatcher.Timer);
			_nonSerializableTimerGroup = context.GetGroup(GameMatcher.NonSerializableTimer);
			_deltaShootTimerGroup = context.GetGroup(GameMatcher.DeltaShootTimer);
		}

		protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
		{
			return context.CreateCollector(GameMatcher.Tick.Added());
		}

		protected override bool Filter(GameEntity entity)
		{
			return entity.hasTick;
		}

		protected override void Execute(List<GameEntity> entities)
		{
			var deltaTime = _context.tick.DeltaTime;
			foreach (var timerEntity in _timerGroup.GetEntities())
			{
				var lastTime = timerEntity.timer.Current - deltaTime;
				if (lastTime > 0)
				{
					timerEntity.ReplaceTimer(lastTime, timerEntity.timer.Max);
				}
				else
				{
					timerEntity.RemoveTimer();
				}
			}

			foreach (var timerEntity in _nonSerializableTimerGroup.GetEntities())
			{
				var lastTime = timerEntity.nonSerializableTimer.Current - deltaTime;
				if (lastTime > 0)
				{
					timerEntity.ReplaceNonSerializableTimer(lastTime, timerEntity.nonSerializableTimer.Max);
				}
				else
				{
					timerEntity.RemoveNonSerializableTimer();
				}
			}

			foreach (var timerEntity in _deltaShootTimerGroup.GetEntities())
			{
				var lastTime = timerEntity.deltaShootTimer.Current - deltaTime;
				if (lastTime > 0)
				{
					timerEntity.ReplaceDeltaShootTimer(lastTime, timerEntity.deltaShootTimer.Max);
				}
				else
				{
					timerEntity.RemoveDeltaShootTimer();
				}
			}
		}
	}
}