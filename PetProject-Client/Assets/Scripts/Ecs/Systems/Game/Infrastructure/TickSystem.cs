using Entitas;
using UnityEngine;

namespace Ecs.Systems.Game.Infrastructure
{
	public class TickSystem : IInitializeSystem, IExecuteSystem
	{
		private readonly GameContext _gameContext;

		public TickSystem(GameContext gameContext)
		{
			_gameContext = gameContext;
		}

		public void Execute()
		{
			if (_gameContext.isPause)
			{
				return;
			}

			_gameContext.ReplaceTick(Time.deltaTime);
		}

		public void Initialize()
		{
			_gameContext.ReplaceTick(0);
		}
	}
}