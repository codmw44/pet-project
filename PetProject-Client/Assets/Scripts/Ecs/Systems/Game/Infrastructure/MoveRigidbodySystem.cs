using Entitas;

namespace Ecs.Systems.Game.Infrastructure
{
	public class MoveRigidbodySystem: IExecuteSystem
	{
		private IGroup<GameEntity> _group;

		public MoveRigidbodySystem(GameContext context)
		{
			_group = context.GetGroup(GameMatcher.AllOf(GameMatcher.Move, GameMatcher.RigidbodyView));
		}

		public void Execute()
		{
			foreach (var gameEntity in _group.AsEnumerable())
			{
				var direction = gameEntity.move.Direction;
				if (gameEntity.hasMoveSpeed)
				{
					direction *= gameEntity.moveSpeed.Value;
				}
				
				gameEntity.rigidbodyView.Value.velocity = direction;
				gameEntity.ReplaceWorldPosition(gameEntity.bulletView.View.transform.position);
			}
		}
		
	}
}