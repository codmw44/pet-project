using System.Collections.Generic;
using Ecs.Components.Game.Infrastructure;
using Entitas;

namespace Ecs.Systems.Game.Infrastructure
{
	public class CleanupSystem : MultiReactiveSystem<CleanupEntity, EcsContexts>
	{
		public CleanupSystem(EcsContexts contexts) : base(contexts)
		{
		}

		protected override ICollector[] GetTrigger(EcsContexts contexts)
		{
			return new ICollector[]
			{
				contexts.GameContext.CreateCollector(GameMatcher.Cleanup.Added()), contexts.InputContext.CreateCollector(InputMatcher.Cleanup.Added())
			};
		}

		protected override bool Filter(CleanupEntity entity)
		{
			return entity.isCleanup;
		}

		protected override void Execute(List<CleanupEntity> entities)
		{
			foreach (var entity in entities)
			{
				entity.Destroy();
			}
		}
	}
}