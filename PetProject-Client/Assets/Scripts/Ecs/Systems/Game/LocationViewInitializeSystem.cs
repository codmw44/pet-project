using Controllers.Location;
using Entitas;

namespace Ecs.Systems.Game
{
	public class LocationViewInitializeSystem : IExecuteSystem, IInitializeSystem
	{
		private readonly GameContext _gameContext;
		private readonly LocationController _locationController;

		public LocationViewInitializeSystem(GameContext gameContext, LocationController locationController)
		{
			_gameContext = gameContext;
			_locationController = locationController;
		}

		public void Initialize()
		{
			if (!_gameContext.hasLevelModel || _gameContext.hasLocationView)
			{
				return;
			}

			_locationController.SetLocation(_gameContext.levelModel.Model);

			_gameContext.ReplaceLocationView(_locationController);
		}

		public void Execute()
		{
			Initialize();
		}
	}
}