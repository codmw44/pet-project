using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace Ecs.Systems.Game.Input
{
	public class PlayerCharacterSetMoveSystem : ReactiveSystem<InputEntity>
	{
		private readonly InputContext _context;
		private IGroup<GameEntity> _playersGroup;

		public PlayerCharacterSetMoveSystem(InputContext context, GameContext gameContext) : base(context)
		{
			_context = context;
			_playersGroup = gameContext.GetGroup(GameMatcher.AllOf(GameMatcher.Character, GameMatcher.Player));
		}

		protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
		{
			return context.CreateCollector(InputMatcher.JoystickInput);
		}

		protected override bool Filter(InputEntity entity)
		{
			return entity.hasJoystickInput;
		}

		protected override void Execute(List<InputEntity> entities)
		{
			foreach (var gameEntity in _playersGroup.AsEnumerable())
			{
				var direction = new Vector3(_context.joystickInput.Value.x, 0f, _context.joystickInput.Value.y);

				gameEntity.ReplaceMove(direction);
			}
		}
	}
}