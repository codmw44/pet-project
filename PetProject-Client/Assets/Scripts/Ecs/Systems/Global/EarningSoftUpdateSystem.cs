using Ecs.Components.Global.Player;
using Entitas;

namespace Ecs.Systems.Game.Player
{
	public class EarningSoftUpdateSystem : IInitializeSystem, ITearDownSystem
	{
		private readonly GlobalContext _gameContext;
		private readonly IGroup<GlobalEntity> _softGroup;

		public EarningSoftUpdateSystem(GlobalContext gameContext)
		{
			_gameContext = gameContext;
			_softGroup = gameContext.GetGroup(GlobalMatcher.SoftCurrency);
		}

		public void Initialize()
		{
			_softGroup.OnEntityUpdated += OnSoftUpdated;
		}

		public void TearDown()
		{
			_softGroup.OnEntityUpdated -= OnSoftUpdated;
		}

		private void OnSoftUpdated(IGroup<GlobalEntity> group, GlobalEntity entity, int index, IComponent previouscomponent, IComponent newcomponent)
		{
			var previous = (SoftCurrencyComponent)previouscomponent;
			var next = (SoftCurrencyComponent)newcomponent;

			if (previous.Value >= next.Value)
			{
				return;
			}

			var delta = next.Value - previous.Value;
			var earned = _gameContext.hasEarnedSoft ? _gameContext.earnedSoft.Value : 0;
			_gameContext.ReplaceEarnedSoft(earned + delta);
		}
	}
}