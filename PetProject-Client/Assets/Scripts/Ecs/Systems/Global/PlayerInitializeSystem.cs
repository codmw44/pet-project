using AssetProvider.Infrastructure;
using Configs;
using Entitas;
using UnityFx.Async.Promises;

namespace Ecs.Systems.Game.Player
{
	public class PlayerInitializeSystem : IInitializeSystem
	{
		public const string PlayerConfigName = "Player";
		private readonly IAssetProvider _assetProvider;

		private readonly GlobalContext _context;

		public PlayerInitializeSystem(GlobalContext context, IAssetProvider assetProvider)
		{
			_context = context;
			_assetProvider = assetProvider;
		}

		public void Initialize()
		{
			var isInitialzed = _context.hasSoftCurrency && _context.hasHardCurrency;
			if (isInitialzed)
			{
				return;
			}

			_assetProvider.Load<PlayerHolder>(PlayerConfigName).Done(InitializePlayerComponents);
		}

		private void InitializePlayerComponents(PlayerHolder obj)
		{
			if (!_context.hasSoftCurrency)
			{
				_context.SetSoftCurrency(obj.Item.Soft);
			}

			if (!_context.hasHardCurrency)
			{
				_context.SetHardCurrency(obj.Item.Hard);
			}
		}
	}
}