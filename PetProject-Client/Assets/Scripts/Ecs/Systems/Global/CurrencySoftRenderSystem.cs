using System.Collections.Generic;
using Entitas;
using GameStates.Infrastructure;
using GameStates.States;
using Models.Shop;
using UI.Infrastructure;
using Views.Currency;

namespace Ecs.Systems.Game.Player
{
	public class CurrencySoftRenderSystem : ReactiveSystem<GlobalEntity>, IInitializeSystem
	{
		private readonly GlobalContext _context;
		private readonly GameStateMachine _gameStateMachine;
		private readonly UIWindowContainer _uiWindowContainer;

		private IEnumerable<SoftCurrencyView> _windows;

		public CurrencySoftRenderSystem(GlobalContext context, UIWindowContainer uiWindowContainer, GameStateMachine gameStateMachine) : base(context)
		{
			_context = context;
			_uiWindowContainer = uiWindowContainer;
			_gameStateMachine = gameStateMachine;
		}

		public void Initialize()
		{
			_windows = _uiWindowContainer.GetWindows<SoftCurrencyView>();
			foreach (var hardCurrencyView in _windows)
			{
				hardCurrencyView.OnClickPlus += () => { _gameStateMachine.Enter<ShopGameState, ShopType>(ShopType.Hard); };
			}
		}

		protected override ICollector<GlobalEntity> GetTrigger(IContext<GlobalEntity> context)
		{
			return context.CreateCollector(GlobalMatcher.SoftCurrency.Added());
		}

		protected override bool Filter(GlobalEntity entity)
		{
			return entity.hasSoftCurrency;
		}

		protected override void Execute(List<GlobalEntity> entities)
		{
			var value = _context.softCurrency.Value;

			foreach (var window in _windows)
			{
				window.SetValue(value);
			}
		}
	}
}