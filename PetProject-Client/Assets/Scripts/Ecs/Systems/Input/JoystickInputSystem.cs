using Entitas;
using UnityEngine;

namespace Ecs.Systems.Input
{
	public class JoystickInputSystem : IExecuteSystem
	{
		private readonly InputContext _context;

		public JoystickInputSystem(InputContext context)
		{
			_context = context;
		}

		public void Execute()
		{
			var vertical = UnityEngine.Input.GetAxis("Vertical");
			var horizontal = UnityEngine.Input.GetAxis("Horizontal");
			var input = new Vector2(horizontal, vertical);
			if (input != Vector2.zero)
			{
				_context.ReplaceJoystickInput(input);
			}
			else if (!_context.hasJoystickInput || _context.joystickInput.Value != Vector2.zero)
			{
				_context.ReplaceJoystickInput(Vector2.zero);
			}
		}
	}
}