using System.Collections.Generic;
using Entitas;
using GameStates.Infrastructure;
using GameStates.States;

namespace Ecs.Systems.Lobby
{
    public class LoadGameSystem : ReactiveSystem<GameEntity>
    {
        private readonly GameContext _context;
        private readonly GameStateMachine _gameStateMachine;

        public LoadGameSystem(GameContext context, GameStateMachine gameStateMachine) : base(context)
        {
            _context = context;
            _gameStateMachine = gameStateMachine;
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.LevelModel);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasLevelModel;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            var sceneName = _context.levelModel.Model.Scene.ToString();
            _gameStateMachine.Enter<LoadLevelGameState, string>(sceneName);
        }
    }
}