using NPG.DependentRandom.Infrastructure;
using NPG.DependentRandom.Models;

namespace Controllers.Serializer
{
	public class RollHistorySerializer : Serializer<RollHistoryContainer>, IRollHistorySerializer
	{
		public override string FileName => "RollHistory";

		void IRollHistorySerializer.Serialize(RollHistoryContainer rollHistoryContainer)
		{
			base.Serialize(rollHistoryContainer);
		}

		RollHistoryContainer IRollHistorySerializer.Deserialize()
		{
			return base.Deserialize() ?? new RollHistoryContainer();
		}
	}
}