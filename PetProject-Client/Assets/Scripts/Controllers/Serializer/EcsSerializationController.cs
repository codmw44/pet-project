using System;
using AssetProvider.Infrastructure;
using Ecs;
using Ecs.Serialization;
using Ecs.Serialization.Infrastructure;
using Models;
using NPG.DependentRandom.Infrastructure;
using UnityEngine;
using UnityFx.Async;
using UnityFx.Async.Promises;

namespace Controllers.Serializer
{
	public class EcsSerializationController : IDisposable
	{
		private const string SerializationConfigName = "EcsSerializationConfig";
		private readonly IAssetProvider _assetProvider;
		private readonly IComponentSerializer _componentSerializer;

		private readonly EcsContexts _contexts;
		private readonly IDependentRandom _dependentRandom;
		private readonly Container<GameInitModel> _gameInitModel;
		private readonly GameEcsSerializer _gameSerializer;
		private readonly GlobalEcsSerializer _globalSerializer;
		private readonly LobbyEcsSerializer _lobbySerializer;
		private readonly TimeCheatPrevention _timeCheatPrevention;

		private ContextSerializer<GameContext, GameEntity> _gameContextSerializer;
		private ContextSerializer<GlobalContext, GlobalEntity> _globalContextSerializer;
		private ContextSerializer<LobbyContext, LobbyEntity> _lobbyContextSerializer;

		public EcsSerializationController(EcsContexts contexts, IAssetProvider assetProvider, GameEcsSerializer gameSerializer,
			LobbyEcsSerializer lobbySerializer, GlobalEcsSerializer globalSerializer, IDependentRandom dependentRandom,
			TimeCheatPrevention timeCheatPrevention,
			IComponentSerializer componentSerializer, Container<GameInitModel> gameInitModel)
		{
			_contexts = contexts;
			_assetProvider = assetProvider;
			_gameSerializer = gameSerializer;
			_lobbySerializer = lobbySerializer;
			_timeCheatPrevention = timeCheatPrevention;
			_componentSerializer = componentSerializer;
			_gameInitModel = gameInitModel;
			_globalSerializer = globalSerializer;
			_dependentRandom = dependentRandom;
		}

		public void Dispose()
		{
			var inLevel = _contexts.GameContext.hasLevelModel;
			if (inLevel)
			{
				SerializeGameContext();
			}
			else
			{
				SerializeLobbyContext();
			}

			SerializeGlobalContext();
		}

		public IAsyncOperation Initialize()
		{
			return _assetProvider.Load<EcsSerializationConfig>(SerializationConfigName).Then(config =>
			{
				_gameContextSerializer = new ContextSerializer<GameContext, GameEntity>(_componentSerializer, config);
				_lobbyContextSerializer = new ContextSerializer<LobbyContext, LobbyEntity>(_componentSerializer, config);
				_globalContextSerializer = new ContextSerializer<GlobalContext, GlobalEntity>(_componentSerializer, config);
			});
		}

		public void SerializeAll()
		{
			SerializeGlobalContext();
			SerializeLobbyContext();
			SerializeGameContext();

			_dependentRandom.Dispose();
		}

		public void SerializeGameContext()
		{
			if (_gameInitModel.Value == null || !_gameInitModel.Value.IsInitialized)
			{
				return;
			}

			if (_contexts.GameContext == null)
			{
				return;
			}

			if (_gameContextSerializer == null)
			{
				Debug.Log("Trying to serialize, but game serialization config is not loaded yet!");
				return;
			}

			_contexts.GameContext.ReplaceSerializationTime(_timeCheatPrevention.GetTimeStamp());
			_gameSerializer.Serialize(_gameContextSerializer.Serialize(_contexts.GameContext));
		}

		public void SerializeGlobalContext()
		{
			if (_gameInitModel.Value == null || !_gameInitModel.Value.IsInitialized)
			{
				return;
			}

			if (_contexts.GlobalContext == null)
			{
				return;
			}

			if (_globalContextSerializer == null)
			{
				Debug.Log("Trying to serialize, but global serialization config is not loaded yet!");
				return;
			}

			_contexts.GameContext.ReplaceSerializationTime(_timeCheatPrevention.GetTimeStamp());
			_globalSerializer.Serialize(_globalContextSerializer.Serialize(_contexts.GlobalContext));
		}

		public void SerializeLobbyContext()
		{
			if (_gameInitModel.Value == null || !_gameInitModel.Value.IsInitialized)
			{
				return;
			}

			if (_contexts.LobbyContext == null)
			{
				return;
			}

			if (_lobbyContextSerializer == null)
			{
				Debug.Log("Trying to serialize, but lobby serialization config is not loaded yet!");
				return;
			}

			_contexts.GameContext.ReplaceSerializationTime(_timeCheatPrevention.GetTimeStamp());
			_lobbySerializer.Serialize(_lobbyContextSerializer.Serialize(_contexts.LobbyContext));
		}

		public void DeserializeAll()
		{
			DeserializeLobbyContext();
			DeserializeGlobalContext();
			DeserializeGameContext();
		}

		public void DeserializeGlobalContext()
		{
			if (_globalContextSerializer == null)
			{
				Debug.Log("Trying to deserialize, but serialization config is not loaded yet!");
				return;
			}

			_globalContextSerializer.Deserialize(_globalSerializer.Deserialize(), _contexts.GlobalContext);
		}

		public void DeserializeLobbyContext()
		{
			if (_lobbyContextSerializer == null)
			{
				Debug.Log("Trying to deserialize, but serialization config is not loaded yet!");
				return;
			}

			_lobbyContextSerializer.Deserialize(_lobbySerializer.Deserialize(), _contexts.LobbyContext);
		}

		public void DeserializeGameContext()
		{
			if (_gameContextSerializer == null)
			{
				Debug.Log("Trying to deserialize, but serialization config is not loaded yet!");
				return;
			}

			var contextData = _gameSerializer.Deserialize();
			_gameContextSerializer.Deserialize(contextData, _contexts.GameContext);
		}
	}
}