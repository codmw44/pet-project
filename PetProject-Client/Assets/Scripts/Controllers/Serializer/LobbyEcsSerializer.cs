using Ecs.Serialization.Infrastructure;

namespace Controllers.Serializer
{
	public class LobbyEcsSerializer : Serializer<ContextData>
	{
		public override string FileName => "LobbyEcsState";
	}
}