using Ecs.Serialization.Infrastructure;

namespace Controllers.Serializer
{
	public class GameEcsSerializer : Serializer<ContextData>
	{
		public override string FileName => "GameEcsState";
	}
}