using Ecs.Serialization.Infrastructure;

namespace Controllers.Serializer
{
	public class GlobalEcsSerializer : Serializer<ContextData>
	{
		public override string FileName => "GlobalEcsState";
	}
}