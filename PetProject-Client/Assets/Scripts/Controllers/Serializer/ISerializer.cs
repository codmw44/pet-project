namespace Controllers.Serializer
{
	public interface ISerializer<T> where T : class
	{
		void Serialize(T obj);
		T Deserialize();
	}
}