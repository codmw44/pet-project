using System;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

namespace Controllers.Serializer
{
	public abstract class Serializer<T> : ISerializer<T> where T : class
	{
#if UNITY_EDITOR
		private const string FileExtension = ".json";
#else
		private const string FileExtension = ".save";
#endif

		private static readonly JsonSerializerSettings SerializerSettings = new JsonSerializerSettings {TypeNameHandling = TypeNameHandling.None};

		public abstract string FileName { get; }

		public void Serialize(T obj)
		{
			var json = JsonConvert.SerializeObject(obj, Formatting.None, SerializerSettings);
			Save(json, FileName);
		}

		public T Deserialize()
		{
			var data = Load();

			if (string.IsNullOrEmpty(data))
			{
				return null;
			}

			try
			{
				return JsonConvert.DeserializeObject<T>(data, SerializerSettings);
			}
			catch (Exception e)
			{
				Debug.LogError(e);
			}

			return null;
		}

		public string Load()
		{
			var pathFile = GetPath(FileName);

			Debug.Log($"{FileName} Try load data");
			var data = string.Empty;

			var directoryPath = Path.GetDirectoryName(pathFile);
			if (!Directory.Exists(directoryPath))
			{
				return data;
			}

			try
			{
				if (pathFile.Contains("://") || pathFile.Contains(":///"))
				{
					var www = new WWW(pathFile);
					while (!www.isDone)
					{
					}

					data = www.text;
				}
				else
				{
					data = File.ReadAllText(pathFile);
				}

#if ENCRYPT
				data = AES.Decrypt(data);
#endif
			}
			catch (Exception ex)
			{
				Debug.Log($"{FileName} load has exception {ex}");
			}

			return data;
		}

		public void Clear()
		{
			var pathFile = GetPath(FileName);
			if (!File.Exists(pathFile))
			{
				return;
			}

			File.Delete(pathFile);
		}

		private void Save(string data, string nameFile)
		{
			var pathFile = GetPath(nameFile);
			try
			{
				var directoryPath = Path.GetDirectoryName(pathFile);

				if (!Directory.Exists(directoryPath))
				{
					Directory.CreateDirectory(directoryPath);
				}
#if ENCRYPT
				data = AES.Encrypt(data);
#endif

				File.WriteAllText(pathFile, data);

				Debug.Log($"Success save {nameFile}");
			}
			catch (Exception exception)
			{
				Debug.LogError($"File {nameFile} at path {pathFile} generated exception {exception}.");
			}
		}

		private static string GetPath(string name)
		{
#if UNITY_EDITOR
			return Path.Combine("Assets/Save", string.Concat(name, FileExtension));
#else
			return Path.Combine(Application.persistentDataPath, "Data", string.Concat(name, FileExtension));
#endif
		}
	}
}