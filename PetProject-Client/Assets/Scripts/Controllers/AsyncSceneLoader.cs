using UnityEngine.SceneManagement;
using UnityFx.Async;
using UnityFx.Async.Extensions;

namespace Controllers
{
	public class AsyncSceneController
	{
		public IAsyncOperation LoadScene(string sceneName, LoadSceneMode loadSceneMode = LoadSceneMode.Single)
		{
			var request = SceneManager.LoadSceneAsync(sceneName, loadSceneMode);

			return request.ToAsync();
		}
	}
}