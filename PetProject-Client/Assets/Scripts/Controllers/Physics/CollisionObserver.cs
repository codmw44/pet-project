using System;
using UnityEngine;

namespace Controllers.Physics
{
	[RequireComponent(typeof(Collider))]
	public class CollisionObserver : MonoBehaviour
	{
		private GameEntity _entity;

		public void SetEntity(GameEntity entity)
		{
			_entity = entity;
		}

		private void OnCollisionEnter(Collision other)
		{
			_entity.ReplaceCollision(other);
		}

		private void OnCollisionStay(Collision other)
		{
			_entity.ReplaceCollision(other);
		}

		private void OnCollisionExit(Collision other)
		{
			if (_entity.hasCollision)
			{
				_entity.RemoveCollision();
			}
		}

		private void OnDestroy()
		{
			_entity = null;
		}
	}
}