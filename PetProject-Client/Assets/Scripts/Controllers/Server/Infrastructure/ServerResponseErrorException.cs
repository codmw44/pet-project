using System;
using PetServer.Defenitions.Models.Response;

namespace Controllers.Server.Infrastructure
{
	public class ServerResponseErrorException : Exception
	{
		public Response Response { get; private set; }

		public ServerResponseErrorException(Response response)
		{
			Response = response;
		}

		public override string Message => $"Server error: {Response.ErrorEnum.ToString()}";
	}
}