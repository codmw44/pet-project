using UnityFx.Async;

namespace Controllers.Server.Infrastructure
{
	public interface IAuthController
	{
		IAsyncOperation Auth();
	}
}