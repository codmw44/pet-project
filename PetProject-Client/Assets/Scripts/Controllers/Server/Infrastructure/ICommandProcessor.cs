using PetServer.Defenitions.Commands.Infrastructure;
using PetServer.Defenitions.Models.Response;
using UnityFx.Async;


namespace Controllers.Server.Infrastructure
{
	public interface ICommandProcessor
	{
		IAsyncOperation ProcessCommand(ICommand<Response> command, bool blockUntilComplete = false);
		IAsyncOperation<T> ProcessCommand<T>(ICommand<T> command, bool blockUntilComplete = false) where T : Response;
	}
}