using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Grpc.Core;
using PetServer.Balancer.Models.Balance;
using UnityEngine;
using UnityFx.Async;
using UnityFx.Async.Extensions;

namespace Controllers.Server
{
	public class ChannelController
	{
		private readonly ChannelCredentials _channelCredentials;
		private const float TimeoutSec = 5f;

		readonly Dictionary<string, Channel> _channels = new Dictionary<string, Channel>();

		public ChannelController(ChannelCredentials channelCredentials)
		{
			_channelCredentials = channelCredentials;
		}

		public IAsyncOperation<Channel> GetChannel(ChannelType channelType)
		{
			return GetChannel(_channelCredentials.GetCredentials(channelType));
		}

		public IAsyncOperation<Channel> GetChannel(string target)
		{
			if (_channels.TryGetValue(target, out var channel))
			{
				if (channel.State == ChannelState.Shutdown)
				{
					channel = CreateNewChannel();
					_channels[target] = channel;
				}

				return ConnectChannel(channel).ToAsync();
			}

			var newChannel = CreateNewChannel();

			_channels.Add(target, newChannel);

			return ConnectChannel(newChannel).ToAsync();

			Channel CreateNewChannel()
			{
#if !DEVELOPMENT_BUILD
				var tlsCrt = File.ReadAllText(Path.Combine(Application.streamingAssetsPath, "tls.crt"));
				var serverCred = new SslCredentials(tlsCrt);
				return new Channel(target, serverCred);
#endif
				return new Channel(target, Grpc.Core.ChannelCredentials.Insecure);
			}
		}

		private async Task<Channel> ConnectChannel(Channel channel)
		{
			if (channel.State != ChannelState.Ready)
			{
				return channel;
			}

			try
			{
				await channel.ConnectAsync(DateTime.Now.AddSeconds(TimeoutSec));
			}
			catch (Exception e)
			{
				Debug.LogError(e);
				throw;
			}

			return channel;
		}
	}
}