using Commands.Auth;
using Controllers.Server.Infrastructure;
using PetServer.Defenitions.Models.Auth;
using PetServer.Models.Auth;
using UnityEngine;
using UnityFx.Async;
using UnityFx.Async.Promises;
using Utility.Extensions;

namespace Controllers.Server
{
	public class AuthController : IAuthController
	{
		private readonly ICommandProcessor _serverCommandController;
		private readonly GuestAuthCommand.Factory _guestAuthCommandFactory;
		private readonly Container<SessionToken> _sessionToken;
		private string _authToken;

		public AuthController(ICommandProcessor serverCommandController, GuestAuthCommand.Factory guestAuthCommandFactory,
			Container<SessionToken> sessionToken)
		{
			_serverCommandController = serverCommandController;
			_guestAuthCommandFactory = guestAuthCommandFactory;
			_sessionToken = sessionToken;
		}

		public IAsyncOperation Auth()
		{
			var deviceId = "Test ded";

			return _serverCommandController.ProcessCommand(_guestAuthCommandFactory.Create(deviceId), true).Then(ProcessAuthResult).DefaultCatch();
		}

		private IAsyncOperation ProcessAuthResult(AuthResponse authResult)
		{
			_sessionToken.Value = authResult.SessionToken;
			Debug.Log("Success auth with session token: " + _sessionToken.Value);
			return AsyncResult.CompletedOperation;
		}
	}
}