using Commands.Ping;
using Controllers.Server.Infrastructure;
using PetServer.Balancer.Models.Balance;
using UnityFx.Async;

namespace Controllers.Server
{
	public class ConnectionController
	{
		private readonly ICommandProcessor _serverCommandController;
		private readonly PingCommand.Factory _pingCommand;

		public ConnectionController(ICommandProcessor serverCommandController, PingCommand.Factory pingCommand)
		{
			_serverCommandController = serverCommandController;
			_pingCommand = pingCommand;
		}

		public IAsyncOperation PingServer(ChannelType channelType)
		{
			return _serverCommandController.ProcessCommand(_pingCommand.Create(channelType));
		}
	}
}