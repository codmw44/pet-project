using System;
using PetServer.Balancer.Models.Balance;

namespace Controllers.Server
{
	public class ChannelCredentials
	{
#if DEVELOPMENT_BUILD
		private const string GenericChannel = "backend-svc:12345";
#else
		private const string GenericChannel = "develop.codmw44dev.site";
#endif

		public virtual string GetCredentials(ChannelType channelType)
		{
			switch (channelType)
			{
				case ChannelType.General:
					return GenericChannel;

				default:
					throw new ArgumentOutOfRangeException(nameof(channelType), channelType, null);
			}
		}
	}
}