using PetServer.Balancer.Models.Balance;
using UnityEngine;

namespace Controllers.Server.Extensions
{
	public static class ClientVersionSpecExtension
	{
		public static ClientSpecModel GetSpec(this ClientSpecModel clientSpecModel)
		{
			clientSpecModel.ClientVersion = Application.version;
			return clientSpecModel;
		}
	}
}