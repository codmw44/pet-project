using PetServer.Defenitions.Models.Response;
using UnityEngine;

namespace Controllers.Server
{
	public class ResponseErrorProcessingController
	{
		public void ProcessResponse(Response response)
		{
			Debug.Log("Process server error: " + response.ErrorEnum.ToString());
			//todo show ui with error
		}
	}
}