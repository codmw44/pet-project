using System;
using PetServer.Balancer.Models.Balance;
using UI.Infrastructure;
using UnityEngine;
using UnityFx.Async;
using UnityFx.Async.Promises;
using Views.Notices;

namespace Controllers.Server
{
	public class ManualReconnectionController
	{
		private readonly ConnectionController _connectionController;
		private readonly UIWindowContainer _windowContainer;

		public ManualReconnectionController(ConnectionController connectionController, UIWindowContainer windowContainer)
		{
			_connectionController = connectionController;
			_windowContainer = windowContainer;
		}

		public IAsyncOperation Reconnect(int countAttempts = 1)
		{
			var resultPromise = new AsyncCompletionSource();

			void ExecuteReconnect()
			{
				_connectionController.PingServer(ChannelType.General).Then(() => { resultPromise.SetCompleted(); }, OnFailedConnect);
			}

			ExecuteReconnect();

			return resultPromise;

			void OnFailedConnect(Exception e)
			{
				countAttempts--;
				if (countAttempts > 0)
				{
					ExecuteReconnect();
				}
				else
				{
					resultPromise.SetException(e);
				}
			}
		}

		public IAsyncOperation RequestManualReconnect()
		{
			var resultPromise = new AsyncCompletionSource();
			var view = _windowContainer.GetWindow<ConnectionNoticeView>();
			view.Show();
			view.SetAvailableBtn(true);

			view.OnReconnectClick = () =>
			{
				view.SetAvailableBtn(false);

				Reconnect()
					.Then(() =>
					{
						resultPromise.SetCompleted();
						view.Hide();
					}, OnFailedConnect);
			};

			return resultPromise;

			void OnFailedConnect(Exception e)
			{
				Debug.Log("Failed ping balance server with error: " + e);
				view.ShowFailedAttemptConnection();
				view.SetAvailableBtn(true);
			}
		}
	}
}