using System;
using Controllers.Server.Infrastructure;
using PetServer.Defenitions.Commands.Infrastructure;
using PetServer.Defenitions.Models.Response;
using UnityEngine;
using UnityFx.Async;
using UnityFx.Async.Promises;

namespace Controllers.Server
{
	public class ServerCommandController : ICommandProcessor
	{
		private const int CountAutoReconnects = 3;

		private readonly Container<ManualReconnectionController> _manualReconnectionController;
		private readonly ResponseErrorProcessingController _responseErrorProcessingController;

		public ServerCommandController(Container<ManualReconnectionController> manualReconnectionController,
			ResponseErrorProcessingController responseErrorProcessingController)
		{
			_manualReconnectionController = manualReconnectionController;
			_responseErrorProcessingController = responseErrorProcessingController;
		}

		public IAsyncOperation ProcessCommand(ICommand<Response> command, bool blockUntilComplete = false)
		{
			return ProcessCommand<Response>(command, blockUntilComplete);
		}

		public IAsyncOperation<T> ProcessCommand<T>(ICommand<T> command, bool blockUntilComplete = false) where T : Response
		{
			var resultAsyncResult = new AsyncCompletionSource<T>();

			ExecuteCommand();

			return resultAsyncResult;

			void ExecuteCommand()
			{
				command.Execute().Then(ResolveCommand, RejectCommand);
			}

			void ResolveCommand(T response)
			{
				if (response.HasError)
				{
					resultAsyncResult.SetException(new ServerResponseErrorException(response));
					Debug.Log($"Succeed execute command {command.GetType().Name}. Response error: {response.ErrorEnum}");

					_responseErrorProcessingController.ProcessResponse(response);
				}
				else
				{
					resultAsyncResult.SetResult(response);
					Debug.Log($"Succeed execute command {command.GetType().Name}");
				}
			}

			void RejectCommand(Exception e)
			{
				if (blockUntilComplete)
				{
					_manualReconnectionController.Value.Reconnect(CountAutoReconnects).
					                              Then(ExecuteCommand,
						                              exception =>
						                              {
							                              _manualReconnectionController.Value.RequestManualReconnect().Done(ExecuteCommand);
						                              });
				}
				else
				{
					Debug.Log($"Failed process command {command.GetType().Name}\n {e}");

					resultAsyncResult.SetException(e);
				}
			}
		}
	}
}