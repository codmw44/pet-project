using Zenject;

namespace Controllers
{
	public class Container<T> where T : class
	{
		public T Value { get; set; }

		public void Resolve(DiContainer container)
		{
			Value = container.Resolve<T>();
		}
	}
}