using System.Collections.Generic;
using System.Linq;
using AssetProvider.Infrastructure;
using Configs;
using Controllers.Finance.Infrastructure;
using Models.Price;
using Models.Shop;
using UnityEngine;
using UnityFx.Async;
using UnityFx.Async.Promises;

namespace Controllers.Shop
{
	public class ShopController
	{
		public const string ShopHolderId = "ShopHolder";

		private readonly IAssetProvider _assetProvider;
		private readonly Container<IFinanceProcessor> _financeProcessor;

		private ShopModel _shopModel;

		public ShopController(IAssetProvider assetProvider, Container<IFinanceProcessor> financeProcessor)
		{
			_assetProvider = assetProvider;
			_financeProcessor = financeProcessor;
		}

		public IEnumerable<ShopItemModel> ShopItems
		{
			get
			{
				var list = new List<ShopItemModel>();
				foreach (var shopItem in _shopModel.ShopItems)
				{
					list.AddRange(shopItem.Value);
				}

				return list;
			}
		}

		public IAsyncOperation Initialize()
		{
			return _assetProvider.Load<ShopHolder>(ShopHolderId).Then(holder => { _shopModel = holder.Item; });
		}

		public void CollectInapp(string id)
		{
			var iapItem = ShopItems.FirstOrDefault(item =>
			{
				var inappPrice = item.Price as InappPrice;
				if (inappPrice == null)
				{
					return false;
				}

				return inappPrice.ProductId == id;
			});

			if (iapItem == null)
			{
				Debug.LogErrorFormat("Can't find item with inapp id '{0}'", id);
				return;
			}

			_financeProcessor.Value.Process(iapItem.Reward);
		}
	}
}