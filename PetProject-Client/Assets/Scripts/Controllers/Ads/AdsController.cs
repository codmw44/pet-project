using System;
using UnityFx.Async;

namespace Controllers.Ads
{
	public abstract class AdsController
	{
		protected string _idPlace;
		public Action OnAdsClosed = () => { };
		public Action<string> OnAdsFailed = id => { };
		public Action<string> OnAdsStart = id => { };
		public Action<string> OnAdsSuccess = id => { };

		public Action<bool> OnRewardedVideoStateChanged = b => { };

		public virtual IAsyncOperation Initialize()
		{
			return AsyncResult.CompletedOperation;
		}

		public void SetPlaceId(string id)
		{
			_idPlace = id;
		}

		public abstract bool IsAvailableAds();
		public abstract IAsyncOperation ShowAd();
	}
}