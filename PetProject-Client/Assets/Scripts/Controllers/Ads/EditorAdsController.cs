using UnityFx.Async;

namespace Controllers.Ads
{
	public class EditorAdsController : AdsController
	{
		public EditorAdsController()
		{
			OnRewardedVideoStateChanged(true);
		}

		public override bool IsAvailableAds()
		{
			return true;
		}

		public override IAsyncOperation ShowAd()
		{
			return AsyncResult.CompletedOperation;
		}
	}
}