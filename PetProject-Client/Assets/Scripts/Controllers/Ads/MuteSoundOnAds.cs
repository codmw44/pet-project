using UnityEngine;
using UnityEngine.SceneManagement;

namespace Controllers.Ads
{
	public class MuteSoundOnAds
	{
		public MuteSoundOnAds(AdsController adsController)
		{
			SceneManager.sceneLoaded += (arg0, mode) => { UnMuteSound(); };
			adsController.OnAdsStart += s => MuteSound();
			adsController.OnAdsClosed += UnMuteSound;
			adsController.OnAdsFailed += e => UnMuteSound();
		}

		private static void MuteSound()
		{
			AudioListener.volume = 0f;
			AudioListener.pause = true;
		}

		private static void UnMuteSound()
		{
			AudioListener.volume = 1f;
			AudioListener.pause = false;
		}
	}
}