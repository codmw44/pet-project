using System;

namespace Controllers.Notifications
{
	public interface INotificationController
	{
		string SetNotification(DateTime localTime, string title, string body);

		void CancelNotification(string id);
		void CancelAllNotifications();
	}
}