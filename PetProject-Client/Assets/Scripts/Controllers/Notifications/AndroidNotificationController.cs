
using UnityEngine;
using Utility.Extensions;
#if UNITY_ANDROID
using System;
using Unity.Notifications.Android;

namespace Controllers.Notifications
{
	public class AndroidNotificationController : INotificationController
	{
		private int _channelId;

		public string SetNotification(DateTime localTime, string title, string body)
		{
			if (localTime.IsNight())
			{
				return string.Empty;
			}

			_channelId++;
			var c = new AndroidNotificationChannel()
			{
				Id = _channelId.ToString(), Name = "Default Channel", Importance = Importance.Default, Description = "Generic notifications",
			};
			AndroidNotificationCenter.RegisterNotificationChannel(c);

			var notification = new AndroidNotification {Title = title, Text = body, FireTime = localTime};

			var id = AndroidNotificationCenter.SendNotification(notification, _channelId.ToString()).ToString();
			Debug.LogFormat("ScheduleNotification time: {0} title: {1} id: {2}", localTime, notification.Title, id);

			return id;
		}

		public void CancelNotification(string id)
		{
			if (string.IsNullOrEmpty(id))
			{
				return;
			}

			AndroidNotificationCenter.CancelNotification(int.Parse(id));
			Debug.LogFormat("CancelNotification id: {0}", id);
		}

		public void CancelAllNotifications()
		{
			AndroidNotificationCenter.CancelAllNotifications();
		}
	}
}
#endif