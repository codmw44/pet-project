using Utility.Extensions;
#if UNITY_IOS
using System;
using Unity.Notifications.iOS;
using UnityEngine;

namespace Controllers.Notifications
{
	public class IOSNotificationController : INotificationController
	{
		public string SetNotification(DateTime localTime, string title, string body)
		{
			if (localTime.IsNight())
			{
				return string.Empty;
			}

			var timeTrigger = new iOSNotificationTimeIntervalTrigger {TimeInterval = localTime - DateTime.Now, Repeats = false};

			var notification = new iOSNotification
			{
				Title = title,
				Body = body,
				ShowInForeground = true,
				ForegroundPresentationOption = PresentationOption.Alert | PresentationOption.Sound,
				Trigger = timeTrigger
			};

			iOSNotificationCenter.ScheduleNotification(notification);
			Debug.LogFormat("ScheduleNotification timespan: {0} title: {1} id: {2}", timeTrigger.TimeInterval, notification.Title, notification.Identifier);
			return notification.Identifier;
		}

		public void CancelNotification(string id)
		{
			if (string.IsNullOrEmpty(id))
			{
				return;
			}

			iOSNotificationCenter.RemoveScheduledNotification(id);
			Debug.LogFormat("CancelNotification id: {0}", id);
		}

		public void CancelAllNotifications()
		{
			iOSNotificationCenter.RemoveAllScheduledNotifications();
			iOSNotificationCenter.RemoveAllDeliveredNotifications();
		}
	}
}
#endif