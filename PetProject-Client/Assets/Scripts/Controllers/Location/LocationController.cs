using System;
using Models.Levels;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Controllers.Location
{
	public class LocationController : MonoBehaviour, IDisposable
	{
		[SerializeField, Required] private Vector2Int _settedSize;
		[SerializeField, Required] private Transform _floor;
		[SerializeField, Required] private Material _floorMaterial;

		[SerializeField, Required] private Transform _leftStartPoint;
		[SerializeField, Required] private Transform _rightStartPoint;

		private Vector3 _sizeCell;
		private Vector2 _defaultMaterialTiling;

		private void Awake()
		{
			_defaultMaterialTiling = _floorMaterial.GetTextureScale("_MainTex");
		}

		public void SetLocation(LevelModel model)
		{
			var settedSizeX = (float) LevelModel.SizeX / _settedSize.x;
			var settedSizeY = (float) model.SizeY / _settedSize.y;

			_floor.localScale = Vector3.Scale(_floor.localScale, new Vector3(settedSizeX, 1f, settedSizeY));
			_sizeCell = (_rightStartPoint.position - _leftStartPoint.position) / LevelModel.SizeX;


			_floorMaterial.SetTextureScale("_MainTex", Vector2.Scale(_defaultMaterialTiling, new Vector2(settedSizeX, settedSizeY)));
		}

		public Vector3 GetCenterCell(Vector2Int cell)
		{
			return _leftStartPoint.position + _sizeCell * cell.x + _sizeCell.magnitude * Vector3.forward * cell.y + _sizeCell.magnitude * Vector3.forward * 0.5f + _sizeCell * 0.5f;
		}

		public Bounds GetBounds()
		{
			return new Bounds();
		}

		public void Dispose()
		{
			_floorMaterial.SetTextureScale("_MainTex", _defaultMaterialTiling);
		}
	}
}