using System;
using System.Collections.Generic;
using System.Linq;
using Controllers.Server.Infrastructure;
using I2.Loc;
using Models.Price;
using Models.Rewards;
using PetServer.Defenitions.Commands.Finance;
using UnityFx.Async;

namespace Controllers.Finance.Infrastructure
{
	public class FinanceProcessor : IFinanceProcessor
	{
		private static readonly Type PackRewardType = typeof(PackReward);

		private readonly Dictionary<Type, IFinanceProcessor> _controllers;
		private readonly ICommandProcessor _commandProcessor;
		private readonly ProcessPriceCommand.Factory _priceFactory;
		private readonly ProcessRewardCommand.Factory _rewardFactory;

		public FinanceProcessor(Dictionary<Type, IFinanceProcessor> controllers, ICommandProcessor commandProcessor,
			ProcessPriceCommand.Factory priceFactory,
			ProcessRewardCommand.Factory rewardFactory)
		{
			_controllers = controllers;
			_commandProcessor = commandProcessor;
			_priceFactory = priceFactory;
			_rewardFactory = rewardFactory;
		}

		public IAsyncOperation Initialize()
		{
			return AsyncResult.WhenAll(_controllers.Values.Select(controller => controller.Initialize()));
		}

		public bool IsEnough(IPrice price)
		{
			if (!_controllers.TryGetValue(price.GetType(), out var controller))
			{
				return false;
			}

			return controller.IsEnough(price);
		}

		public void SubscribeEnoughChanged(IPrice price, Action<bool> action)
		{
			if (!_controllers.TryGetValue(price.GetType(), out var controller))
			{
				return;
			}

			controller.SubscribeEnoughChanged(price, action);
		}

		public void UnsubscribeEnoughChanged(IPrice price, Action<bool> action)
		{
			if (!_controllers.TryGetValue(price.GetType(), out var controller))
			{
				return;
			}

			controller.SubscribeEnoughChanged(price, action);
		}

		public IAsyncOperation Process(IPrice price)
		{
			if (price == null)
			{
				return AsyncResult.FromException(new Exception("Price is null"));
			}

			return _commandProcessor.ProcessCommand(_priceFactory.Create(price));
		}

		public IAsyncOperation Process(IReward reward)
		{
			if (reward == null)
			{
				return AsyncResult.FromException(new Exception("Reward is null"));
			}

			return _commandProcessor.ProcessCommand(_rewardFactory.Create(reward));
		}

		public IAsyncOperation<string> GetLabel(IPrice price)
		{
			if (!_controllers.TryGetValue(price.GetType(), out var controller))
			{
				return AsyncResult.FromException<string>(new Exception("Unknown price type"));
			}

			return controller.GetLabel(price);
		}

		public IAsyncOperation<string> GetLabel(IReward reward)
		{
			var type = reward.GetType();
			if (type == PackRewardType)
			{
				var pack = (PackReward) reward;
				return AsyncResult.FromResult(LocalizationManager.GetTranslation(pack.Term));
			}

			if (!_controllers.TryGetValue(reward.GetType(), out var controller))
			{
				return AsyncResult.FromException<string>(new Exception("Unknown reward type"));
			}

			return controller.GetLabel(reward);
		}
	}
}