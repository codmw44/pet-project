using System;
using System.Collections.Generic;
using Models.Price;
using Models.Rewards;
using UnityFx.Async;

namespace Controllers.Finance.Infrastructure
{
	public abstract class BaseFinanceProcessor<TPrice, TReward> : IFinanceProcessor where TPrice : IPrice where TReward : IReward
	{
		private readonly Dictionary<IPrice, List<Action<bool>>> _enoughChangedMap = new Dictionary<IPrice, List<Action<bool>>>();
		private readonly Dictionary<IPrice, bool> _enoughMap = new Dictionary<IPrice, bool>();

		public void SubscribeEnoughChanged(IPrice price, Action<bool> action)
		{
			if (!_enoughChangedMap.TryGetValue(price, out var enoughChangedCallbacks))
			{
				enoughChangedCallbacks = new List<Action<bool>>();
				_enoughChangedMap.Add(price, enoughChangedCallbacks);
				_enoughMap.Add(price, IsEnough(price));
			}

			enoughChangedCallbacks.Add(action);
		}

		public void UnsubscribeEnoughChanged(IPrice price, Action<bool> action)
		{
			if (!_enoughChangedMap.TryGetValue(price, out var enoughChangedCallbacks))
			{
				return;
			}

			enoughChangedCallbacks.Remove(action);
			if (enoughChangedCallbacks.Count == 0)
			{
				_enoughChangedMap.Remove(price);
				_enoughMap.Remove(price);
			}
		}

		public virtual IAsyncOperation Initialize()
		{
			return AsyncResult.CompletedOperation;
		}

		public bool IsEnough(IPrice price)
		{
			return IsEnough(Cast(price));
		}

		public IAsyncOperation Process(IPrice price)
		{
			return Process(Cast(price));
		}

		public IAsyncOperation Process(IReward reward)
		{
			return Process(Cast(reward));
		}

		public IAsyncOperation<string> GetLabel(IPrice price)
		{
			return GetLabel(Cast(price));
		}

		public IAsyncOperation<string> GetLabel(IReward reward)
		{
			return GetLabel(Cast(reward));
		}

		public virtual bool IsEnough(TPrice price)
		{
			return false;
		}

		public virtual IAsyncOperation Process(TPrice price)
		{
			return AsyncResult.CompletedOperation;
		}

		public virtual IAsyncOperation<string> GetLabel(TPrice price)
		{
			return AsyncResult.FromResult(string.Empty);
		}

		public virtual IAsyncOperation Process(TReward reward)
		{
			return AsyncResult.CompletedOperation;
		}

		public virtual IAsyncOperation<string> GetLabel(TReward reward)
		{
			return AsyncResult.FromResult(string.Empty);
		}

		protected void SourceChanged()
		{
			if (_enoughMap.Count == 0)
			{
				return;
			}

			foreach (var price in _enoughChangedMap.Keys)
			{
				var isEnough = IsEnough(price);
				if (isEnough == _enoughMap[price])
				{
					continue;
				}

				_enoughMap[price] = isEnough;
				var callbacks = _enoughChangedMap[price];
				callbacks.ForEach(callback => callback?.Invoke(isEnough));
			}
		}

		private TPrice Cast(IPrice price)
		{
			return (TPrice)price;
		}

		private TReward Cast(IReward reward)
		{
			return (TReward)reward;
		}
	}
}