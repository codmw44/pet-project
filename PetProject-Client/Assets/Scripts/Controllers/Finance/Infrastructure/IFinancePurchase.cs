using System;
using Models.Price;
using Models.Rewards;
using UnityFx.Async;

namespace Controllers.Finance.Infrastructure
{
	public interface IFinancePurchase
	{
		IAsyncOperation Process(IPrice price, IReward reward);
		event Action<IPrice, IReward> OnSucceedPurchase;
	}
}