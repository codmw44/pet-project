using System;
using Models.Price;
using Models.Rewards;
using UnityFx.Async;

namespace Controllers.Finance.Infrastructure
{
	public interface IFinanceProcessor
	{
		IAsyncOperation Initialize();

		bool IsEnough(IPrice price);

		void SubscribeEnoughChanged(IPrice price, Action<bool> action);
		void UnsubscribeEnoughChanged(IPrice price, Action<bool> action);

		IAsyncOperation Process(IPrice price);
		IAsyncOperation Process(IReward reward);

		IAsyncOperation<string> GetLabel(IPrice price);
		IAsyncOperation<string> GetLabel(IReward reward);
	}
}