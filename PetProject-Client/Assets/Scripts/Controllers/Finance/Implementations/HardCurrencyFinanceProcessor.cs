using Controllers.Finance.Infrastructure;
using Ecs;
using Entitas;
using Models.Price;
using Models.Rewards;
using UnityFx.Async;
using Utility.Formatting;

namespace Controllers.Finance.Implementations
{
	public class HardCurrencyFinanceProcessor : BaseFinanceProcessor<HardCurrencyPrice, HardCurrencyReward>
	{
		private readonly EcsContexts _ecsContexts;

		public HardCurrencyFinanceProcessor(EcsContexts ecsContexts)
		{
			_ecsContexts = ecsContexts;
		}

		public override IAsyncOperation Initialize()
		{
			_ecsContexts.GlobalContext.GetGroup(GlobalMatcher.HardCurrency).OnEntityUpdated += HardCurrencyUpdated;
			return base.Initialize();
		}

		private void HardCurrencyUpdated(IGroup<GlobalEntity> group, GlobalEntity entity, int index, IComponent previouscomponent, IComponent newcomponent)
		{
			SourceChanged();
		}

		public override bool IsEnough(HardCurrencyPrice price)
		{
			var gameContext = _ecsContexts.GlobalContext;
			return gameContext.hasHardCurrency && gameContext.hardCurrency.Value >= price.Amount;
		}

		public override IAsyncOperation Process(HardCurrencyPrice price)
		{
			var gameContext = _ecsContexts.GlobalContext;
			gameContext.ReplaceHardCurrency(gameContext.hardCurrency.Value - price.Amount);

			return AsyncResult.CompletedOperation;
		}

		public override IAsyncOperation Process(HardCurrencyReward reward)
		{
			var gameContext = _ecsContexts.GlobalContext;
			gameContext.ReplaceHardCurrency(gameContext.hardCurrency.Value + reward.Amount);
			return AsyncResult.CompletedOperation;
		}

		public override IAsyncOperation<string> GetLabel(HardCurrencyPrice price)
		{
			return AsyncResult.FromResult($"<sprite=1>{price.Amount.ToShortString()}");
		}

		public override IAsyncOperation<string> GetLabel(HardCurrencyReward reward)
		{
			return AsyncResult.FromResult($"<sprite=1>{reward.Amount.ToShortString()}");
		}
	}
}