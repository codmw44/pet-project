using Controllers.Finance.Infrastructure;
using Ecs;
using Entitas;
using Models.Price;
using Models.Rewards;
using UnityFx.Async;
using Utility.Formatting;

namespace Controllers.Finance.Implementations
{
	public class SoftCurrencyFinanceProcessor : BaseFinanceProcessor<SoftCurrencyPrice, SoftCurrencyReward>
	{
		private readonly EcsContexts _ecsContexts;

		public SoftCurrencyFinanceProcessor(EcsContexts ecsContexts)
		{
			_ecsContexts = ecsContexts;
		}

		public override IAsyncOperation Initialize()
		{
			_ecsContexts.GlobalContext.GetGroup(GlobalMatcher.SoftCurrency).OnEntityUpdated += SoftCurrencyUpdated;
			return base.Initialize();
		}

		private void SoftCurrencyUpdated(IGroup<GlobalEntity> group, GlobalEntity entity, int index, IComponent previouscomponent,
			IComponent newcomponent)
		{
			SourceChanged();
		}

		public override bool IsEnough(SoftCurrencyPrice price)
		{
			var gameContext = _ecsContexts.GlobalContext;
			return gameContext.hasSoftCurrency && gameContext.softCurrency.Value >= price.Amount;
		}

		public override IAsyncOperation Process(SoftCurrencyPrice price)
		{
			var gameContext = _ecsContexts.GlobalContext;
			gameContext.ReplaceSoftCurrency(gameContext.softCurrency.Value - price.Amount);

			return AsyncResult.CompletedOperation;
		}

		public override IAsyncOperation Process(SoftCurrencyReward currencyReward)
		{
			var gameContext = _ecsContexts.GlobalContext;
			gameContext.ReplaceSoftCurrency(gameContext.softCurrency.Value + currencyReward.Amount);
			return AsyncResult.CompletedOperation;
		}

		public override IAsyncOperation<string> GetLabel(SoftCurrencyPrice price)
		{
			return AsyncResult.FromResult($"<sprite=0>{price.Amount.ToShortString()}");
		}

		public override IAsyncOperation<string> GetLabel(SoftCurrencyReward currencyReward)
		{
			return AsyncResult.FromResult($"<sprite=0>{currencyReward.Amount.ToShortString()}");
		}
	}
}