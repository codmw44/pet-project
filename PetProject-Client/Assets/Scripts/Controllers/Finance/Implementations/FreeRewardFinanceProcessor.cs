using Controllers.Finance.Infrastructure;
using I2.Loc;
using Models.Price;
using Models.Rewards;
using UnityFx.Async;

namespace Controllers.Finance.Implementations
{
	public class FreeRewardFinanceProcessor : BaseFinanceProcessor<RewardPrice, FreeReward>
	{
		private const string FreeTerm = "Reward";

		public override bool IsEnough(RewardPrice price)
		{
			return true;
		}

		public override IAsyncOperation Process(RewardPrice price)
		{
			return AsyncResult.CompletedOperation;
		}

		public override IAsyncOperation<string> GetLabel(RewardPrice price)
		{
			return AsyncResult.FromResult(LocalizationManager.GetTranslation(FreeTerm));
		}
	}
}