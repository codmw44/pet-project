using System;
using Controllers.Finance.Infrastructure;
using Models.Price;
using Models.Rewards;
using UnityFx.Async;
using UnityFx.Async.Promises;

namespace Controllers.Finance.Implementations
{
	public class FinancePurchaseProcessor : IFinancePurchase
	{
		private readonly IFinanceProcessor _financeProcessor;

		public FinancePurchaseProcessor(IFinanceProcessor financeProcessor)
		{
			_financeProcessor = financeProcessor;
		}

		public event Action<IPrice, IReward> OnSucceedPurchase;

		public IAsyncOperation Process(IPrice price, IReward reward)
		{
			var AsyncResultRes = new AsyncCompletionSource();

			var AsyncResultPrice = _financeProcessor.Process(price);
			AsyncResultPrice.Done(
				() =>
				{
					_financeProcessor.Process(reward).
					                  Done(() => AsyncResultRes.SetCompleted(), AsyncResultRes.SetException);
				},
				AsyncResultRes.SetException);

			AsyncResultRes.Done(() => OnSucceedPurchase?.Invoke(price, reward));
			return AsyncResultRes;
		}
	}
}