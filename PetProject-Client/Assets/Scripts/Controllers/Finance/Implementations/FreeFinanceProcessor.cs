using Controllers.Finance.Infrastructure;
using I2.Loc;
using Models.Price;
using Models.Rewards;
using UnityFx.Async;

namespace Controllers.Finance.Implementations
{
	public class FreeFinanceProcessor : BaseFinanceProcessor<FreePrice, FreeReward>
	{
		private const string FreeTerm = "Free";

		public override bool IsEnough(FreePrice price)
		{
			return true;
		}

		public override IAsyncOperation Process(FreePrice price)
		{
			return AsyncResult.CompletedOperation;
		}

		public override IAsyncOperation<string> GetLabel(FreePrice price)
		{
			return AsyncResult.FromResult(LocalizationManager.GetTranslation(FreeTerm));
		}
	}
}