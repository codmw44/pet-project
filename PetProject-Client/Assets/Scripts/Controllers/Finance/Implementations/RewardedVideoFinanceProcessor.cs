using Controllers.Ads;
using Controllers.Finance.Infrastructure;
using Models.Price;
using Models.Rewards;
using UnityFx.Async;

namespace Controllers.Finance.Implementations
{
	public class RewardedVideoFinanceProcessor : BaseFinanceProcessor<RewardedVideoPrice, RewardedVideoReward>
	{
		private readonly AdsController _adsController;

		public RewardedVideoFinanceProcessor(AdsController adsController)
		{
			_adsController = adsController;
		}

		public override IAsyncOperation Initialize()
		{
			_adsController.OnRewardedVideoStateChanged += state => SourceChanged();
			return _adsController.Initialize();
		}

		public override bool IsEnough(RewardedVideoPrice price)
		{
			return _adsController.IsAvailableAds();
		}

		public override IAsyncOperation Process(RewardedVideoPrice price)
		{
			_adsController.SetPlaceId(price.IdPlace);
			return _adsController.ShowAd();
		}

		public override IAsyncOperation<string> GetLabel(RewardedVideoPrice price)
		{
			return AsyncResult.FromResult("<sprite=2>");
		}
	}
}