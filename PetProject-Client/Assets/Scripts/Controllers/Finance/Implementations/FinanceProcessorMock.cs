using System;
using Controllers.Finance.Infrastructure;
using Models.Price;
using Models.Rewards;
using UnityFx.Async;

namespace Controllers.Finance.Implementations
{
	public class FinanceProcessorMock : IFinanceProcessor
	{
		public IAsyncOperation Initialize()
		{
			return AsyncResult.CompletedOperation;
		}

		public bool IsEnough(IPrice price)
		{
			return true;
		}

		public void SubscribeEnoughChanged(IPrice price, Action<bool> action)
		{
		}

		public void UnsubscribeEnoughChanged(IPrice price, Action<bool> action)
		{
		}

		public IAsyncOperation Process(IPrice price)
		{
			return AsyncResult.CompletedOperation;
		}

		public IAsyncOperation Process(IReward reward)
		{
			return AsyncResult.CompletedOperation;
		}

		public IAsyncOperation<string> GetLabel(IPrice price)
		{
			return AsyncResult.FromResult(price.GetType().Name);
		}

		public IAsyncOperation<string> GetLabel(IReward reward)
		{
			return AsyncResult.FromResult(reward.GetType().Name);
		}
	}
}