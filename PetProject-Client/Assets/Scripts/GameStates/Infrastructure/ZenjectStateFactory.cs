using NPG.States;
using Zenject;

namespace GameStates.Infrastructure
{
	public class ZenjectStateFactory : IStateFactory
	{
		private readonly DiContainer _container;

		public ZenjectStateFactory(DiContainer container)
		{
			_container = container;
		}

		public T GetState<T>() where T : class, IExitState
		{
			return _container.Resolve<T>();
		}
	}
}