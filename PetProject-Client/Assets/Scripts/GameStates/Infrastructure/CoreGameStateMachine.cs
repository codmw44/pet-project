using System;
using NPG.States;
using UnityEngine;
using Zenject;

namespace GameStates.Infrastructure
{
    public class CoreGameStateMachine : StateMachine, ITickable
    {
        private readonly ZenjectStateFactory _factory;

        public CoreGameStateMachine(ZenjectStateFactory factory)
        {
            _factory = factory;
        }

        protected override IStateFactory Factory => _factory;

        public Type CurrentState { get; private set; }

        public void Tick()
        {
            Update();
        }

        public event Action<IExitState> OnStateChanged;

        protected override void StateChanged(IExitState oldState, IExitState newState)
        {
#if DEVELOPMENT_BUILD
            Debug.LogFormat("{0} -> {1}", oldState?.GetType().Name, newState?.GetType().Name);
#endif
            CurrentState = newState?.GetType();
            OnStateChanged?.Invoke(newState);
        }
    }
}