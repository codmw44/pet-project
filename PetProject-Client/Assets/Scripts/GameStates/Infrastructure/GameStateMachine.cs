using System;
using NPG.States;
using UnityEngine;
using Zenject;

namespace GameStates.Infrastructure
{
	public class GameStateMachine : StateMachine, ITickable
	{
		private readonly ZenjectStateFactory _factory;

		public GameStateMachine(ZenjectStateFactory factory)
		{
			_factory = factory;
		}

		protected override IStateFactory Factory => _factory;

		public Type CurrentState { get; private set; }

		public void Tick()
		{
			Update();
		}

		public event Action<IExitState> OnStateChanged;

		protected override void StateChanged(IExitState oldState, IExitState newState)
		{
			Debug.LogFormat("{0} -> {1}", oldState?.GetType().Name, newState?.GetType().Name);
			
			CurrentState = newState?.GetType();
			OnStateChanged?.Invoke(newState);
		}
	}
}