using GameStates.Infrastructure;
using GameStates.States.CoreGameStates;
using NPG.States;
using UnityEngine;

namespace GameStates.States
{
    public class CoreGameState : IUpdatable, IState
    {
        private readonly GameStateMachine _gameStateMachine;
        private readonly CoreGameStateMachine _coreGameStateMachine;

        private bool _isInit;

        public CoreGameState(GameStateMachine gameStateMachine, CoreGameStateMachine coreGameStateMachine)
        {
            _gameStateMachine = gameStateMachine;
            _coreGameStateMachine = coreGameStateMachine;
        }

        public void OnEnter()
        {
            if (!_isInit)
            {
                _coreGameStateMachine.Enter<InitializeCoreGameState>();
            }

            _isInit = true;
        }

        public void OnExit()
        {
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                _gameStateMachine.Enter<LoadLobbyGameState>();
            }
        }
    }
}