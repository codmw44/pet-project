using System;
using AssetProvider.Infrastructure;
using Controllers;
using Controllers.Finance.Infrastructure;
using Controllers.Serializer;
using DG.Tweening;
using Ecs;
using Ecs.Systems.Game.Player;
using GameStates.Infrastructure;
using Models;
using NPG.States;
using UI.Infrastructure;
using UnityEngine;
using UnityFx.Async;
using UnityFx.Async.Promises;
using Utility;
using Views.Loader;

namespace GameStates.States
{
	public class FirstLoadGameState : IState
	{
		private const string LobbySceneName = "LobbyScene";
		private readonly IAssetProvider _assetProvider;
		private readonly AsyncSceneController _asyncSceneController;
		private readonly Container<EcsRunner> _ecsRunner;
		private readonly EcsSerializationController _ecsSerializationController;
		private readonly IFinanceProcessor _financeProcessor;
		private readonly Container<GameInitModel> _gameInitModel;

		private readonly GameStateMachine _stateMachine;
		private readonly TimeCheatPrevention _timeCheatPrevention;
		private readonly UIWindowContainer _windowContainer;
		private LoaderView _loaderView;

		public FirstLoadGameState(GameStateMachine stateMachine, AsyncSceneController asyncSceneController, UIWindowContainer windowContainer,
			TimeCheatPrevention timeCheatPrevention, IAssetProvider assetProvider, EcsSerializationController ecsSerializationController,
			Container<EcsRunner> ecsRunner, Container<GameInitModel> gameInitModel, IFinanceProcessor financeProcessor)
		{
			_stateMachine = stateMachine;
			_asyncSceneController = asyncSceneController;
			_windowContainer = windowContainer;
			_timeCheatPrevention = timeCheatPrevention;
			_assetProvider = assetProvider;
			_ecsSerializationController = ecsSerializationController;
			_ecsRunner = ecsRunner;
			_gameInitModel = gameInitModel;
			_financeProcessor = financeProcessor;
		}

		private LoaderView LoaderView
		{
			get
			{
				if (_loaderView == null)
				{
					_loaderView = _windowContainer.GetWindow<LoaderView>();
				}

				return _loaderView;
			}
		}

		public void OnEnter()
		{
			TraceLog.Start("FirstLoadGameState.Initialization");

			var operation = _timeCheatPrevention.Initialize().
			                                     Then(() => AsyncResult.WhenAll(_asyncSceneController.LoadScene(LobbySceneName),
				                                     _assetProvider.Initialize())).
			                                     Then(() =>
				                                     AsyncResult.WhenAll(
					                                     _ecsSerializationController.Initialize().Then(_ecsSerializationController.DeserializeAll))).
			                                     Then(() => AsyncResult.WhenAll(_financeProcessor.Initialize()));
			operation.Catch(Debug.LogError);
			operation.Done(() => { DOVirtual.DelayedCall(LoaderView.AnimationDuration / 2, GameSceneLoaded); });
			operation.ProgressChanged += LoaderView.SetProgressValue;
		}

		public void OnExit()
		{
		}

		private IAsyncOperation PreWarmAssets()
		{
			var AsyncResultPreload = _assetProvider.Preload(new[] {PlayerInitializeSystem.PlayerConfigName});

			return AsyncResultPreload;
		}

		private void GameSceneLoaded()
		{
			TraceLog.Stop("FirstLoadGameState.Initialization");

			_gameInitModel.Value = new GameInitModel {IsInitialized = true};

			LoaderView.Hide();

			_ecsRunner.Value.Initialize();
			_ecsRunner.Value?.SetAvailable(true);

			_stateMachine.Enter<ConnectServerState>();
		}
	}
}