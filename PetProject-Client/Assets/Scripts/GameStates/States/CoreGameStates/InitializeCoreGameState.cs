using Ecs;
using GameStates.Infrastructure;
using NPG.States;

namespace GameStates.States.CoreGameStates
{
	public class InitializeCoreGameState : IState
	{
		private readonly EcsContexts _ecsContexts;
		private readonly CoreGameStateMachine _coreGameStateMachine;

		public InitializeCoreGameState(EcsContexts ecsContexts, CoreGameStateMachine coreGameStateMachine)
		{
			_ecsContexts = ecsContexts;
			_coreGameStateMachine = coreGameStateMachine;
		}

		public void OnEnter()
		{
			if (_ecsContexts.GameContext.GetGroup(GameMatcher.Character).count == 0)
			{
				_ecsContexts.GameContext.isRequiredSetCharacters = true;
			}

			_coreGameStateMachine.Enter<PlayCoreGameState>();
		}

		public void OnExit()
		{
		}
	}
}