using Models.Shop;
using NPG.States;

namespace GameStates.States
{
	public class ShopGameState : IState, IPayloadedState<ShopType>, IUpdatable
	{
		public void OnEnter(ShopType payload)
		{
		}

		public void OnEnter()
		{
			OnEnter(ShopType.IAP);
		}

		public void OnExit()
		{
		}

		public void Update()
		{
		}

		private void CloseShop()
		{
		}
	}
}