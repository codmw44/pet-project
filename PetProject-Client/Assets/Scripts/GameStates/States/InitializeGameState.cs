using Controllers;
using Controllers.Server.Infrastructure;
using DG.Tweening;
using GameStates.Infrastructure;
using NPG.States;
using UI.Infrastructure;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityFx.Async;
using UnityFx.Async.Promises;
using Views.Loader;
using Views.Notices;

namespace GameStates.States
{
	public class InitializeGameState : IState
	{
		private const string InitializeSceneName = "InitializeScene";
		private readonly AsyncSceneController _asyncSceneController;

		private readonly GameStateMachine _stateMachine;
		private readonly UIWindowContainer _windowContainer;

		public InitializeGameState(GameStateMachine stateMachine, AsyncSceneController asyncSceneController, UIWindowContainer windowContainer)
		{
			_stateMachine = stateMachine;
			_asyncSceneController = asyncSceneController;
			_windowContainer = windowContainer;
		}

		public void OnEnter()
		{
			DOTween.onWillLog += OnWillDoTweenLog;

			if (SceneManager.GetActiveScene().buildIndex == 0)
			{
				InitializeSceneLoaded();
			}
			else
			{
				_asyncSceneController.LoadScene(InitializeSceneName).Done(InitializeSceneLoaded);
			}
		}

		public void OnExit()
		{
		}

		private void InitializeSceneLoaded()
		{
			RegisterUI();
			_stateMachine.Enter<FirstLoadGameState>();
		}

		private void RegisterUI()
		{
			var loaderView = Object.FindObjectOfType<LoaderView>();
			_windowContainer.RegisterWindow(loaderView);
			var connectionNoticeView = Object.FindObjectOfType<ConnectionNoticeView>();
			_windowContainer.RegisterWindow(connectionNoticeView);
		}

		/// <summary>
		///     Intercept DOTween's logs
		/// </summary>
		/// <param name="logType"></param>
		/// <param name="message"></param>
		/// <returns>Return TRUE if you want DOTween to proceed with the log, FALSE otherwise.</returns>
		private bool OnWillDoTweenLog(LogType logType, object message)
		{
			if (logType == LogType.Warning)
			{
				Debug.LogError(message);
				return false;
			}

			return true;
		}
	}
}