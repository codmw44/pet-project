using Controllers.Server;
using GameStates.Infrastructure;
using NPG.States;
using PetServer.Balancer.Models.Balance;
using UnityFx.Async.Promises;
using Utility.Extensions;

namespace GameStates.States
{
	public class ConnectServerState : IState
	{
		private readonly GameStateMachine _gameStateMachine;
		private readonly ConnectionController _connectionController;

		public ConnectServerState(GameStateMachine gameStateMachine, ConnectionController connectionController)
		{
			_gameStateMachine = gameStateMachine;
			_connectionController = connectionController;
		}

		public void OnEnter()
		{
			_connectionController.PingServer(ChannelType.General).Then(GoToAuth).DefaultCatch();
		}

		public void OnExit()
		{
		}

		private void GoToAuth()
		{
			_gameStateMachine.Enter<AuthGameState>();
		}
	}
}