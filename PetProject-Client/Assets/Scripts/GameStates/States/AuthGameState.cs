using Controllers.Server.Infrastructure;
using GameStates.Infrastructure;
using NPG.States;
using UnityFx.Async.Promises;
using Utility.Extensions;

namespace GameStates.States
{
	public class AuthGameState : IState
	{
		private readonly IAuthController _authController;
		private readonly GameStateMachine _gameStateMachine;

		public AuthGameState(IAuthController authController, GameStateMachine gameStateMachine)
		{
			_authController = authController;
			_gameStateMachine = gameStateMachine;
		}

		public void OnEnter()
		{
			_authController.Auth().Then(GoToLobby).DefaultCatch();
		}

		public void OnExit()
		{
		}


		private void GoToLobby()
		{
			_gameStateMachine.Enter<LobbyGameState>();
		}
	}
}