using System.ComponentModel;
using Controllers;
using Controllers.Serializer;
using DG.Tweening;
using Ecs;
using GameStates.Infrastructure;
using NPG.States;
using UI.Infrastructure;
using UnityFx.Async;
using UnityFx.Async.Promises;
using Utility;
using Utility.Extensions;
using Views.Loader;

namespace GameStates.States
{
	public class LoadLevelGameState : IPayloadedState<string>
	{
		private readonly AsyncSceneController _asyncSceneController;
		private readonly EcsContexts _ecsContexts;
		private readonly Container<EcsRunner> _ecsRunner;
		private readonly EcsSerializationController _ecsSerializationController;
		private readonly GameStateMachine _stateMachine;
		private readonly UIWindowContainer _windowContainer;
		private LoaderView _loaderView;

		public LoadLevelGameState(GameStateMachine stateMachine, AsyncSceneController asyncSceneController, UIWindowContainer windowContainer,
			Container<EcsRunner> ecsRunner, EcsSerializationController ecsSerializationController, EcsContexts ecsContexts)
		{
			_stateMachine = stateMachine;
			_asyncSceneController = asyncSceneController;
			_windowContainer = windowContainer;
			_ecsRunner = ecsRunner;
			_ecsSerializationController = ecsSerializationController;
			_ecsContexts = ecsContexts;
		}

		private LoaderView LoaderView
		{
			get
			{
				if (_loaderView == null)
				{
					_loaderView = _windowContainer.GetWindow<LoaderView>();
				}

				return _loaderView;
			}
		}

		public void OnEnter(string payload)
		{
			TraceLog.Start("LevelLoadGameState.Initialization");

			LoaderView.Show();
			_ecsSerializationController.SerializeGlobalContext();
			_ecsSerializationController.SerializeLobbyContext();
			_ecsSerializationController.SerializeGameContext();

			_ecsRunner.Value.Dispose();
			_ecsContexts.Initialize();

			_ecsSerializationController.DeserializeGameContext();

			var asyncOperation = _asyncSceneController.LoadScene(payload);
			asyncOperation.ProgressChanged += LoaderView.SetProgressValue;
			asyncOperation.DefaultCatch();
			asyncOperation.Done(() =>
			{
				LoaderView.SetProgressValue(null, new ProgressChangedEventArgs(100, null));
				DOVirtual.DelayedCall(LoaderView.AnimationDuration / 2, GameSceneLoaded);
			});
		}

		public void OnExit()
		{
		}

		private void GameSceneLoaded()
		{
			TraceLog.Stop("LevelLoadGameState.Initialization");

			LoaderView.Hide();

			_ecsRunner.Value?.Initialize();
			_ecsRunner.Value?.SetAvailable(true);

			_stateMachine.Enter<CoreGameState>();
		}
	}
}