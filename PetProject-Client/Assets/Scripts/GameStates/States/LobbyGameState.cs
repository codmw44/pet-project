using Controllers.Finance.Infrastructure;
using Ecs;
using Models.Price;
using Models.Rewards;
using NPG.States;
using UnityEngine;
using UnityFx.Async.Promises;
using Utility.Extensions;

namespace GameStates.States
{
	public class LobbyGameState : IState, IUpdatable
	{
		private readonly EcsContexts _ecsContexts;
		private readonly IFinanceProcessor _financeProcessor;

		public LobbyGameState(EcsContexts ecsContexts, IFinanceProcessor financeProcessor)
		{
			_ecsContexts = ecsContexts;
			_financeProcessor = financeProcessor;
		}

		public void OnEnter()
		{
			if (_ecsContexts.GameContext.hasLevelId) //has previous game progress
			{
				Debug.Log("Has previous game progress. Load it");
				return;
			}

			// _financeProcessor.Process(new HardCurrencyPrice(5)).Done(() => { Debug.Log("Succed prcoess price!!"); });
			_financeProcessor.Process(new HardCurrencyReward(10)).DefaultCatch().Done(() => { Debug.Log("Succed prcoess reward!!"); });

			//IMPORTANT: It forward move to game for increase tests 
			//            OpenGame("Level_0");
		}

		private void OpenGame(string idLevel)
		{
			_ecsContexts.GameContext.ReplaceLevelId(idLevel);
		}

		public void OnExit()
		{
		}

		public void Update()
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				_financeProcessor.Process(new HardCurrencyReward(10)).DefaultCatch().Done(() => { Debug.Log("Succed prcoess reward!!"); });
				_financeProcessor.Process(new HardCurrencyPrice(1)).DefaultCatch().Done(() => { Debug.Log("Succed prcoess price!!"); });
			}
			else if (Input.GetKeyDown(KeyCode.P))
			{
				_financeProcessor.Process(new HardCurrencyPrice(10000000)).DefaultCatch().Done(() => { Debug.Log("Succed prcoess price!!"); });
			}
		}
	}
}