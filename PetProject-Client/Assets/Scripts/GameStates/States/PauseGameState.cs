using Controllers;
using Controllers.Serializer;
using Ecs;
using NPG.States;

namespace GameStates.States
{
	public class PauseGameState : IState
	{
		private readonly Container<EcsRunner> _ecsRunner;
		private readonly EcsSerializationController _ecsSerializationController;

		public PauseGameState(EcsSerializationController ecsSerializationController, Container<EcsRunner> ecsRunner)
		{
			_ecsSerializationController = ecsSerializationController;
			_ecsRunner = ecsRunner;
		}

		public void OnEnter()
		{
			_ecsRunner.Value?.SetAvailable(false);
			_ecsSerializationController.SerializeAll();
		}

		public void OnExit()
		{
		}
	}
}