using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEditor.AddressableAssets.Settings;
using UnityEngine;
using Utility;

namespace Configs
{
	[CreateAssetMenu]
	public class AtlasInfoConfig : SerializedScriptableObject
	{
		[field: SerializeField, ReadOnly] public Dictionary<string, string> AtlasInfo { get; } = new Dictionary<string, string>();

		public bool TryGetAtlas(string spriteName, out string atlasName)
		{
			return AtlasInfo.TryGetValue(spriteName, out atlasName);
		}

#if UNITY_EDITOR
		[Button(ButtonSizes.Medium)]
		public void UpdateAtlasInfo()
		{
			AtlasInfo.Clear();
			var groups = EditorUtils.LoadAssets<AddressableAssetGroup>();
			foreach (var assetGroup in groups)
			foreach (var entry in assetGroup.entries)
			{
				var assets = AssetDatabase.LoadAllAssetRepresentationsAtPath(entry.AssetPath);
				var isAtlas = assets.Length > 1;
				if (!isAtlas)
				{
					continue;
				}

				var atlasName = entry.address;
				foreach (var asset in assets)
				{
					AtlasInfo.Add(asset.name, atlasName);
				}
			}
		}
#endif
	}
}