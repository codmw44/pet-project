using Models;
using UnityEngine;

namespace Configs
{
	[CreateAssetMenu]
	public class PlayerHolder : BaseHolder<PlayerModel>
	{
	}
}