using Models.Shop;
using UnityEngine;

namespace Configs
{
	[CreateAssetMenu]
	public class ShopHolder : BaseHolder<ShopModel>
	{
	}
}