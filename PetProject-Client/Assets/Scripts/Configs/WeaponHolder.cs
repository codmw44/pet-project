using Models.Weapons.Infrastructure;
using UnityEngine;

namespace Configs
{
	[CreateAssetMenu]
	public class WeaponHolder : BaseHolder<BaseWeaponModel>
	{
	}
}