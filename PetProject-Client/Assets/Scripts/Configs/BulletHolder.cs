using Models.Weapons.Bullets;
using UnityEngine;

namespace Configs
{
	[CreateAssetMenu]
	public class BulletHolder : BaseHolder<BulletModel>
	{
	}
}