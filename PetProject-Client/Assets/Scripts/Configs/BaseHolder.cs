using Sirenix.OdinInspector;
using UnityEngine;

namespace Configs
{
	public class BaseHolder<T> : SerializedScriptableObject
	{
		[SerializeField, Required] public T Item;
	}
}