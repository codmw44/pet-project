using Models.Levels;
using UnityEngine;

namespace Configs
{
	[CreateAssetMenu]
	public class LevelHolder : BaseHolder<LevelModel>
	{
	}
}