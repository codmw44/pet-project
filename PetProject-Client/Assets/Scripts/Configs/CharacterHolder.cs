using Models.Characters.Infrastructure;
using UnityEngine;

namespace Configs
{
	[CreateAssetMenu]
	public class CharacterHolder : BaseHolder<BaseCharacterModel>
	{
	}
}