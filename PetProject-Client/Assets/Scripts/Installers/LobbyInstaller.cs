using System;
using System.Collections.Generic;
using System.Linq;
using Controllers;
using Ecs;
using Ecs.Systems.Game.Infrastructure;
using Ecs.Systems.Game.Level;
using Ecs.Systems.Game.Player;
using Ecs.Systems.Lobby;
using Entitas;
using Sirenix.OdinInspector;
using UI.Infrastructure;
using UnityEngine;
using Views.Currency;
using Zenject;

namespace Installers
{
    public class LobbyInstaller : MonoInstaller<LobbyInstaller>, IInitializable, IDisposable
    {
        public const string InputSystems = "Input";
        public const string LogicSystems = "Logic";
        public const string RenderSystems = "Render";

        private static readonly HashSet<string> IgronedNamespaces = new HashSet<string> {"Entitas", "Entitas.VisualDebugging.Unity"};

        [SerializeField, Required] private List<HardCurrencyView> _hardCurrencyViews;

        [Header("Views"), SerializeField, Required]
        private List<SoftCurrencyView> _softCurrencyViews;

        public void Dispose()
        {
            Container.Resolve<Container<EcsRunner>>().Value.Dispose();
        }

        public void Initialize()
        {
            Container.Resolve<Container<EcsRunner>>().Resolve(Container);
        }

        public override void InstallBindings()
        {
            Container.BindInterfacesTo<LobbyInstaller>().FromInstance(this);

            BindEcs();

            var uiWindowContainer = Container.Resolve<UIWindowContainer>();
            uiWindowContainer.ClearUnusedWindows();

            BindCurrenciesViews(uiWindowContainer);
            BindLobbyViews(uiWindowContainer);
        }

        private void BindContexts()
        {
            var contexts = Container.Resolve<EcsContexts>();

            contexts.InitializeObserver();
            Container.Bind<LobbyContext>().FromInstance(contexts.LobbyContext).AsSingle();
            Container.Bind<GlobalContext>().FromInstance(contexts.GlobalContext).AsSingle();
            Container.Bind<GameContext>().FromInstance(contexts.GameContext).AsSingle();
        }

        private bool CheckSystemNamespace(string @namespace)
        {
            if (string.IsNullOrEmpty(@namespace))
            {
                return false;
            }

            return !IgronedNamespaces.Contains(@namespace);
        }

        [Button(ButtonSizes.Large)]
        private void FindReferences()
        {
            _softCurrencyViews.Clear();
            _softCurrencyViews.AddRange(FindObjectsOfType<SoftCurrencyView>());

            _hardCurrencyViews.Clear();
            _hardCurrencyViews.AddRange(FindObjectsOfType<HardCurrencyView>());
        }

        #region Views

        private void BindCurrenciesViews(UIWindowContainer windowContainer)
        {
            windowContainer.RegisterWindows(_softCurrencyViews);
            windowContainer.RegisterWindows(_hardCurrencyViews);
        }

        private void BindLobbyViews(UIWindowContainer windowContainer)
        {
        }

        #endregion

        #region ECS

        private void BindEcs()
        {
            Container.BindInterfacesAndSelfTo<EcsRunner>().AsSingle();

            BindContexts();
            BindSystems();

            Container.Bind<Dictionary<string, List<ISystem>>>().FromMethod(GetSystems);
        }

        private List<ISystem> GetLogicSystems()
        {
            var result = new List<ISystem>();

            ResolveInitializeSystems(ref result);
            ResolveProcessSystems(ref result);
            ResolveCleanUpSystems(ref result);

            return result;
        }

        private List<ISystem> GetInputSystems()
        {
            var result = new List<ISystem>();

            return result;
        }

        private List<ISystem> GetRenderSystems()
        {
            var result = new List<ISystem>();

            ResolveRenderSystems(ref result);

            return result;
        }

        private void ResolveInitializeSystems(ref List<ISystem> result)
        {
            result.Add(Container.Resolve<PlayerInitializeSystem>());
        }

        private void ResolveProcessSystems(ref List<ISystem> result)
        {
            result.Add(Container.Resolve<LevelModelInitializeSystem>());
            result.Add(Container.Resolve<LoadGameSystem>());
        }

        private void ResolveRenderSystems(ref List<ISystem> result)
        {
            //			result.Add(Container.Resolve<CurrencySoftRenderSystem>());
            //			result.Add(Container.Resolve<CurrencyHardRenderSystem>());
        }

        private void ResolveCleanUpSystems(ref List<ISystem> result)
        {
            result.Add(Container.Resolve<CleanupSystem>());
        }

        private Dictionary<string, List<ISystem>> GetSystems()
        {
            var result = new Dictionary<string, List<ISystem>>();

            result.Add(InputSystems, GetInputSystems());
            result.Add(LogicSystems, GetLogicSystems());
            result.Add(RenderSystems, GetRenderSystems());

            return result;
        }

        private void BindSystems()
        {
            var systemType = typeof(ISystem);
            var allSystems = AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes()).Where(p =>
                systemType.IsAssignableFrom(p) && !p.IsAbstract && CheckSystemNamespace(p.Namespace));

            foreach (var system in allSystems)
            {
                Container.BindInterfacesAndSelfTo(system).AsSingle();
            }
        }

        #endregion
    }
}