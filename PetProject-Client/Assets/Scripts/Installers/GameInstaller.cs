using System;
using System.Collections.Generic;
using System.Linq;
using Controllers;
using Controllers.Location;
using DefaultNamespace;
using Ecs;
using Ecs.Systems.Game;
using Ecs.Systems.Game.Bullet;
using Ecs.Systems.Game.Character;
using Ecs.Systems.Game.Health;
using Ecs.Systems.Game.Infrastructure;
using Ecs.Systems.Game.Input;
using Ecs.Systems.Game.Level;
using Ecs.Systems.Game.Physics;
using Ecs.Systems.Game.Weapons;
using Ecs.Systems.Input;
using Entitas;
using Sirenix.OdinInspector;
using UI.Infrastructure;
using UnityEngine;
using Zenject;

namespace Installers
{
    public class GameInstaller : MonoInstaller<GameInstaller>, IInitializable, IDisposable
    {
        public const string InputSystems = "Input";
        public const string LogicSystems = "Logic";
        public const string RenderSystems = "Render";
        public const string PhysicsSystems = "Physics";

        private static readonly HashSet<string> IgnoredNamespaces = new HashSet<string> {"Entitas", "Entitas.VisualDebugging.Unity"};

        [SerializeField, Required] private LocationController _locationController;

        public void Dispose()
        {
        }

        public void Initialize()
        {
            Container.Resolve<Container<EcsRunner>>().Resolve(Container);
        }

        public override void InstallBindings()
        {
            Container.BindInterfacesTo<GameInstaller>().FromInstance(this);

            BindEcs();

            var uiWindowContainer = Container.Resolve<UIWindowContainer>();
            uiWindowContainer.ClearUnusedWindows();

            BindActionViews(uiWindowContainer);

            Container.BindInterfacesAndSelfTo<LocationController>().FromInstance(_locationController).AsSingle();
        }

        private void BindContexts()
        {
            var contexts = Container.Resolve<EcsContexts>();

            contexts.InitializeObserver();

            Container.Bind<InputContext>().FromInstance(contexts.InputContext).AsSingle();
            Container.Bind<GameContext>().FromInstance(contexts.GameContext).AsSingle();
            Container.Bind<GlobalContext>().FromInstance(contexts.GlobalContext).AsSingle();
        }

        private bool CheckSystemNamespace(string @namespace)
        {
            if (string.IsNullOrEmpty(@namespace))
            {
                return false;
            }

            return !IgnoredNamespaces.Contains(@namespace);
        }

        [Button(ButtonSizes.Large)]
        private void FindReferences()
        {
        }

        #region Views

        private void BindActionViews(UIWindowContainer windowContainer)
        {
        }

        #endregion

        #region ECS

        private void BindEcs()
        {
            Container.BindInterfacesAndSelfTo<EcsRunner>().AsSingle();

            BindContexts();
            BindSystems();

            Container.Bind<Dictionary<string, List<ISystem>>>().FromMethod(GetSystems);
        }

        private List<ISystem> GetLogicSystems()
        {
            var result = new List<ISystem>();

            ResolveInitializeSystems(ref result);
            ResolveProcessSystems(ref result);
            ResolveCleanUpSystems(ref result);

            return result;
        }

        private List<ISystem> GetInputSystems()
        {
            var result = new List<ISystem>();

            result.Add(Container.Resolve<TickSystem>());
            result.Add(Container.Resolve<JoystickInputSystem>());

            result.Add(Container.Resolve<PlayerCharacterSetMoveSystem>());

            return result;
        }

        private List<ISystem> GetPhysicSystems()
        {
            var result = new List<ISystem>();

            ResolvePhysicsSystems(ref result);

            return result;
        }

        private List<ISystem> GetRenderSystems()
        {
            var result = new List<ISystem>();

            ResolveRenderSystems(ref result);
            ResolvePositionsSystems(ref result);

            return result;
        }

        private void ResolveInitializeSystems(ref List<ISystem> result)
        {
            result.Add(Container.Resolve<LevelModelInitializeSystem>());
            result.Add(Container.Resolve<LocationViewInitializeSystem>());
        }

        private void ResolveProcessSystems(ref List<ISystem> result)
        {
            result.Add(Container.Resolve<TimerSystem>());
            result.Add(Container.Resolve<InitializeCharactersSystem>());
            result.Add(Container.Resolve<CharacterSetSystem>());
            result.Add(Container.Resolve<PlayerRequiredShootSystem>());
            result.Add(Container.Resolve<ShootByTimerSystem>());
            result.Add(Container.Resolve<ShootProcessingSystem>());
            result.Add(Container.Resolve<DamageProcessSystem>());
            result.Add(Container.Resolve<BulletDestroyByCollisionSystem>());
        }

        private void ResolvePhysicsSystems(ref List<ISystem> result)
        {
            result.Add(Container.Resolve<ColliderRenderSystem>());
            result.Add(Container.Resolve<SetColliderObserverSystem>());
            result.Add(Container.Resolve<CollisionToEntityProcessSystem>());
            result.Add(Container.Resolve<MoveSystem>());
        }

        private void ResolveRenderSystems(ref List<ISystem> result)
        {
            result.Add(Container.Resolve<CharacterSetRenderSystem>());
            result.Add(Container.Resolve<CharacterMoveRotationSystem>());
            result.Add(Container.Resolve<BulletRenderSystem>());
        }

        private void ResolvePositionsSystems(ref List<ISystem> result)
        {
            result.Add(Container.Resolve<CharacterRotationSystem>());
            result.Add(Container.Resolve<CharacterMoveSystem>());
            result.Add(Container.Resolve<MoveRigidbodySystem>());
        }

        private void ResolveCleanUpSystems(ref List<ISystem> result)
        {
            result.Add(Container.Resolve<DeathCleanUpSystem>());
            result.Add(Container.Resolve<CleanupSystem>());
            result.Add(Container.Resolve<CharacterCleanUpViewSystem>());
            result.Add(Container.Resolve<BulletCleanUpSystem>());
            result.Add(Container.Resolve<CollisionObserverCleanUpSystem>());
        }

        private Dictionary<string, List<ISystem>> GetSystems()
        {
            var result = new Dictionary<string, List<ISystem>>();

            result.Add(InputSystems, GetInputSystems());
            result.Add(PhysicsSystems, GetPhysicSystems());
            result.Add(LogicSystems, GetLogicSystems());
            result.Add(RenderSystems, GetRenderSystems());

            return result;
        }

        private void BindSystems()
        {
            var systemType = typeof(ISystem);
            var allSystems = AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes()).Where(p =>
                systemType.IsAssignableFrom(p) && !p.IsAbstract && CheckSystemNamespace(p.Namespace));

            foreach (var system in allSystems)
            {
                Container.BindInterfacesAndSelfTo(system).AsSingle();
            }
        }

        #endregion
    }
}