using System;
using System.Collections.Generic;
using System.Diagnostics;
using AssetProvider.Implementations;
using CodeStage.AdvancedFPSCounter;
using Commands.Auth;
using Commands.Ping;
using Controllers;
using Controllers.Ads;
using Controllers.Finance.Implementations;
using Controllers.Finance.Infrastructure;
using Controllers.Serializer;
using Controllers.Server;
using Controllers.Server.Infrastructure;
using Controllers.Shop;
using Ecs;
using Ecs.Serialization.Implementations;
using Ecs.Serialization.Infrastructure;
using GameStates.Infrastructure;
using GameStates.States;
using GameStates.States.CoreGameStates;
using LunarConsolePlugin;
using Models;
using Models.Price;
using Models.Rewards;
using Newtonsoft.Json;
using NPG.DependentRandom.Implementations;
using NPG.DependentRandom.Infrastructure;
using ObjectPool;
using PetServer.Balancer.Models.Balance;
using PetServer.Defenitions.Commands.Finance;
using PetServer.Models.Auth;
using Sirenix.OdinInspector;
using UI.Infrastructure;
using UnityEngine;
using Zenject;
using Debug = UnityEngine.Debug;

#if !UNITY_EDITOR
using Controllers.Ads;
#endif

namespace Installers
{
	[CVarContainer]
	public class RootInstaller : MonoInstaller<RootInstaller>, IInitializable, IDisposable
	{
		private static readonly CVar _fpsEnabled = new CVar("FPS Enabled", true, CFlags.NoArchive);
		private static readonly CVar _money = new CVar("Money", "100", CFlags.NoArchive);
		[SerializeField, Required] private AFPSCounter _fpsConter;

		private Container<GameInitModel> _gameInitModel;

		private GameStateMachine _gameStateMachine;

		[SerializeField, Required] private LunarConsole _lunarConsole;
		[SerializeField, Required] private TimeCheatPrevention _timeCheatPrevention;

		private GameStateMachine GameStateMachine
		{
			get
			{
				if (_gameStateMachine == null)
				{
					_gameStateMachine = Container.Resolve<GameStateMachine>();
				}

				return _gameStateMachine;
			}
		}

		private bool IsInitialized => GameInitModel.Value != null && GameInitModel.Value.IsInitialized;

		private Container<GameInitModel> GameInitModel
		{
			get
			{
				if (_gameInitModel == null)
				{
					_gameInitModel = Container.Resolve<Container<GameInitModel>>();
				}

				return _gameInitModel;
			}
		}

		public void Dispose()
		{
		}

		public void Initialize()
		{
			InitializeCheats();
			RegisterCheats();

			Container.Resolve<Container<ManualReconnectionController>>().Resolve(Container);
			GameStateMachine.Enter<InitializeGameState>();
		}

		public override void InstallBindings()
		{
			Container.BindInterfacesTo<RootInstaller>().FromInstance(this);

			Container.BindInterfacesAndSelfTo<AsyncSceneController>().AsSingle();
			Container.BindInterfacesAndSelfTo<EcsContexts>().AsSingle();
			Container.BindInterfacesAndSelfTo<UIWindowContainer>().AsSingle();
			Container.BindInterfacesAndSelfTo(typeof(Container<>)).AsSingle();

			Container.BindInterfacesTo<AddressablesAssetProvider>().AsSingle();

			Container.BindInterfacesAndSelfTo<TimeCheatPrevention>().FromInstance(_timeCheatPrevention);

			Container.Bind<IComponentSerializer>().To<JsonComponentSerializer>().AsSingle();

			BindServer();

			BindCommandFactories();
			BindCurrencyControllers();
			BindAdsController();
			BindDependentRandom();

			BindGameStates();
			BindSerialization();
			BindEcs();
		}

		private void BindServer()
		{
			Container.BindInterfacesAndSelfTo<ResponseErrorProcessingController>().AsSingle();
			Container.BindInterfacesAndSelfTo<ShopController>().AsSingle();
			Container.BindInterfacesAndSelfTo<ChannelController>().AsSingle();
			Container.BindInterfacesAndSelfTo<ChannelCredentials>().AsSingle();
			Container.BindInterfacesAndSelfTo<Container<ManualReconnectionController>>().AsSingle();
			Container.BindInterfacesAndSelfTo<ManualReconnectionController>().AsSingle();
			Container.BindInterfacesAndSelfTo<ConnectionController>().AsSingle();
			Container.Bind<ICommandProcessor>().To<ServerCommandController>().AsSingle();
			Container.Bind<IAuthController>().To<AuthController>().AsSingle();
			Container.Bind<Container<SessionToken>>().AsSingle();
		}

		private void BindDependentRandom()
		{
			Container.Bind<IDependentRandom>().To<DependentRandom>().AsSingle();
			Container.BindInterfacesAndSelfTo<CachedDependentChanceProvider>().AsSingle();
			Container.BindInterfacesAndSelfTo<SystemRandom>().AsSingle();
		}

		private void BindCommandFactories()
		{
			Container.BindFactory<ChannelType, PingCommand, PingCommand.Factory>().FromFactory<PingCommandFactory>();
			Container.BindFactory<string, GuestAuthCommand, GuestAuthCommand.Factory>().FromFactory<GuestAuthCommandFactory>();

			Container.BindFactory<IPrice, ProcessPriceCommand, ProcessPriceCommand.Factory>().FromFactory<ProcessPriceFactory>();
			Container.BindFactory<IReward, ProcessRewardCommand, ProcessRewardCommand.Factory>().FromFactory<ProcessRewardFactory>();
		}

		private void BindEcs()
		{
			BindContexts();
		}

		private void BindContexts()
		{
			var contexts = Container.Resolve<EcsContexts>();
			contexts.Initialize();
			contexts.InitializeObserver();
		}

		private void BindAdsController()
		{
			Container.Bind<AdsController>().To<EditorAdsController>().AsSingle();
		}

		private void BindCurrencyControllers()
		{
			Container.Bind<SoftCurrencyFinanceProcessor>().ToSelf().AsSingle();
			Container.Bind<HardCurrencyFinanceProcessor>().ToSelf().AsSingle();
			Container.Bind<RewardedVideoFinanceProcessor>().ToSelf().AsSingle();
			Container.Bind<FinanceProcessorMock>().ToSelf().AsSingle();
			Container.Bind<FreeFinanceProcessor>().ToSelf().AsSingle();
			Container.Bind<FreeRewardFinanceProcessor>().ToSelf().AsSingle();

			Container.Bind<IFinanceProcessor>().To<FinanceProcessor>().AsSingle();
			Container.Bind<IFinancePurchase>().To<FinancePurchaseProcessor>().AsSingle();
			Container.Bind<Dictionary<Type, IFinanceProcessor>>()
				.FromMethod(() => new Dictionary<Type, IFinanceProcessor>
				{
					{typeof(SoftCurrencyPrice), Container.Resolve<SoftCurrencyFinanceProcessor>()},
					{typeof(HardCurrencyPrice), Container.Resolve<HardCurrencyFinanceProcessor>()},
					{typeof(RewardedVideoPrice), Container.Resolve<RewardedVideoFinanceProcessor>()},
#if UNITY_EDITOR || DEVELOPMENT_BUILD
					{typeof(InappPrice), Container.Resolve<FinanceProcessorMock>()},
#else
				{typeof(InappPrice), Container.Resolve<InappFinanceProcessor>()},
#endif
					{typeof(SoftCurrencyReward), Container.Resolve<SoftCurrencyFinanceProcessor>()},
					{typeof(HardCurrencyReward), Container.Resolve<HardCurrencyFinanceProcessor>()},
					{typeof(FreePrice), Container.Resolve<FreeFinanceProcessor>()},
					{typeof(RewardPrice), Container.Resolve<FreeRewardFinanceProcessor>()}
				})
				.AsSingle();
		}

		private void BindGameStates()
		{
			Container.BindInterfacesAndSelfTo<ZenjectStateFactory>().AsSingle();
			Container.BindInterfacesAndSelfTo<GameStateMachine>().AsSingle();
			Container.BindInterfacesAndSelfTo<CoreGameStateMachine>().AsSingle();

			Container.BindInterfacesAndSelfTo<InitializeGameState>().AsSingle();
			Container.BindInterfacesAndSelfTo<FirstLoadGameState>().AsSingle();
			Container.BindInterfacesAndSelfTo<LoadLevelGameState>().AsSingle();
			Container.BindInterfacesAndSelfTo<LoadLobbyGameState>().AsSingle();
			Container.BindInterfacesAndSelfTo<LobbyGameState>().AsSingle();
			Container.BindInterfacesAndSelfTo<CoreGameState>().AsSingle();

			Container.BindInterfacesAndSelfTo<InitializeCoreGameState>().AsSingle();
			Container.BindInterfacesAndSelfTo<PlayCoreGameState>().AsSingle();
			Container.BindInterfacesAndSelfTo<PauseGameState>().AsSingle();

			Container.BindInterfacesAndSelfTo<ConnectServerState>().AsSingle();
			Container.BindInterfacesAndSelfTo<AuthGameState>().AsSingle();
		}

		private void BindSerialization()
		{
			Container.BindInterfacesAndSelfTo<EcsSerializationController>().AsSingle();
			Container.BindInterfacesAndSelfTo<GameEcsSerializer>().AsSingle();
			Container.BindInterfacesAndSelfTo<LobbyEcsSerializer>().AsSingle();
			Container.BindInterfacesAndSelfTo<GlobalEcsSerializer>().AsSingle();
			Container.BindInterfacesAndSelfTo<RollHistorySerializer>().AsSingle();
		}

		private void InitializeCheats()
		{
#if UNITY_EDITOR || DEVELOPMENT_BUILD
			_lunarConsole.Spawn();

			var fps = _fpsConter.Spawn();
			_fpsEnabled.AddDelegate(cvar => fps.OperationMode = cvar.BoolValue ? OperationMode.Normal : OperationMode.Disabled);
			fps.OperationMode = _fpsEnabled.BoolValue ? OperationMode.Normal : OperationMode.Disabled;

			var manifest = Resources.Load<TextAsset>("UnityCloudBuildManifest.json");
			if (manifest == null)
			{
				return;
			}

			var manifestDict = JsonConvert.DeserializeObject<Dictionary<string, object>>(manifest.text);
			if (manifestDict == null)
			{
				return;
			}

			if (!manifestDict.TryGetValue("buildNumber", out var buildNumber))
			{
				buildNumber = "undefined";
			}

			fps.AppendText = string.Format("Build #{0}", buildNumber);
#endif
		}

		[Conditional("DEVELOPMENT_BUILD")]
		private void RegisterCheats()
		{
			LunarConsole.RegisterAction("Print save", () =>
			{
				Debug.Log(Container.Resolve<GameEcsSerializer>().Load());
				Debug.Log(Container.Resolve<LobbyEcsSerializer>().Load());
			});
			LunarConsole.RegisterAction("Add soft", () =>
			{
				var processor = Container.Resolve<IFinanceProcessor>();
				processor.Process(new SoftCurrencyReward(int.Parse(_money)));
			});
			LunarConsole.RegisterAction("Add hard", () =>
			{
				var processor = Container.Resolve<IFinanceProcessor>();
				processor.Process(new HardCurrencyReward(int.Parse(_money)));
			});
			LunarConsole.RegisterAction("Reset soft", () =>
			{
				var contexts = Container.Resolve<EcsContexts>();
				contexts.GlobalContext.ReplaceSoftCurrency(0);
			});
			LunarConsole.RegisterAction("Reset hard", () =>
			{
				var contexts = Container.Resolve<EcsContexts>();
				contexts.GlobalContext.ReplaceHardCurrency(0);
			});
			LunarConsole.RegisterAction("Delete save", () =>
			{
				Container.Resolve<Container<GameInitModel>>().Value.IsInitialized = false;
				Container.Resolve<GameEcsSerializer>().Clear();
				Container.Resolve<LobbyEcsSerializer>().Clear();
				Application.Quit();
			});
		}
	}
}