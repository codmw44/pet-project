using System.Collections.Generic;
using System.Linq;
using I2.Loc;
using UnityEditor;
using UnityEngine;

namespace Utility
{
	public static class EditorUtils
	{
#if UNITY_EDITOR
		public static List<T> LoadAssets<T>(string name = "") where T : Object
		{
			return AssetDatabase.FindAssets(string.Format("t:{0} {1}", typeof(T).Name, name)).Select(AssetDatabase.GUIDToAssetPath)
				.Select(AssetDatabase.LoadAssetAtPath<T>).ToList();
		}

		public static T LoadAsset<T>(string name = "") where T : Object
		{
			return LoadAssets<T>(name)[0];
		}

		public static bool IsTermValid(string term)
		{
			var source = LoadAsset<LanguageSourceAsset>().mSource;
			return source.ContainsTerm(term);
		}
#endif
	}
}