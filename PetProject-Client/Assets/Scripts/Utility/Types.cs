﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class Types
{
	/// <summary>
	/// return type with advanced Assembly lookup
	/// expected namespace format for auto type lookup: [AssemblyName].[Namespace1]. ... .[NamespaceN].[ClassName]
	/// </summary>
	public static Type GetType(string typeName)
	{
		if (string.IsNullOrEmpty(typeName))
		{
			return null;
		}

		var result = Type.GetType(typeName, false, true);
		if (result != null)
		{
			return result;
		}

		foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
		{
			if (assembly.IsDynamic)
			{
				continue;
			}

			result = assembly.GetType(typeName);
			if (result != null)
			{
				return result;
			}
		}

		return null;
	}

	/// <summary>
	/// return type with advanced Assembly lookup or throw exception
	/// </summary>
	public static Type GetExistingType(string typeName)
	{
		var result = GetType(typeName);
		if (result == null) throw new TypeLoadException($"Cannot find type '{typeName}'");

		return result;
	}
	
	public static IEnumerable<Type> EnumerateAll(Func<Type, bool> filter)
	{
		foreach (var t in AppDomain.CurrentDomain.GetAssemblies().Where(x => !x.IsDynamic).SelectMany(x => x.GetTypes().Where(filter)))
		{
			yield return t;
		}
	}
	
}