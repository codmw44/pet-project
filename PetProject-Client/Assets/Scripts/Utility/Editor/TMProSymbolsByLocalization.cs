using System;
using System.IO;
using I2.Loc;
using TMPro;
using TMPro.EditorUtilities;
using UnityEditor;
using UnityEngine;

namespace City.Editor
{
	public static class TMProSymbolsByLocalization
	{
		private const string ForceRequiredSymbols = "1234567890,./%()+-abcdefghijklmnopqrstuvwxyz";
		private const string IgnoreSymbols = "";
		private const string NameFont = "SFProDisplay-Medium SDF";
		private static string FilePath => Path.Combine(Application.dataPath, "generatedFontSymbols.txt");

		[MenuItem("Tools/Font/GenerateSymbolsByLocalization")]
		public static void GenerateSymbolsByLocalization()
		{
			var localizationFile = Resources.Load<LanguageSourceAsset>("I2Languages");
			var source = localizationFile.SourceData;

			var CSVstring = source.Export_CSV(null, ';');

			ProcessSymbols(ref CSVstring);
			SaveFile(CSVstring);
			Debug.Log("Success GenerateSymbolsByLocalization");
		}

		[MenuItem("Tools/Font/OpenFontForSymbols")]
		public static void OpenFontForSymbols()
		{
			if (!File.Exists(FilePath))
			{
				Debug.LogException(new Exception($"Not found file {FilePath}"));
				return;
			}

			var font = Resources.Load<TMP_FontAsset>(NameFont);
			TMPro_FontAssetCreatorWindow.ShowFontAtlasCreatorWindow(font);
		}

		private static void ProcessSymbols(ref string source)
		{
			var result = string.Empty;

			foreach (var ch in source)
			{
				if (IgnoreSymbols.Contains(ch.ToString()))
				{
					continue;
				}

				if (result.Contains(ch.ToString().ToLower()))
				{
					continue;
				}

				result = string.Concat(result, ch.ToString().ToLower());
				if (ch.ToString().ToLower() != ch.ToString().ToUpper())
				{
					result = string.Concat(result, ch.ToString().ToUpper());
				}
			}

			result = string.Concat(result, ForceRequiredSymbols);
			source = result;
		}

		private static void SaveFile(string symbols)
		{
			File.WriteAllText(FilePath, symbols);
		}
	}
}