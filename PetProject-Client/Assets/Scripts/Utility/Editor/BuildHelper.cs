﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Configs;
using Ecs.Serialization;
using UnityEditor;
using UnityEditor.AddressableAssets.Settings;
using UnityEngine;
using Utility;

public static class BuildHelper
{
	[MenuItem("Tools/City/Prepare build")]
	public static void PrepareBuild()
	{
		EcsSerializationConfig.Generate();
		SyncAddressablesKeys();
		AddMissingAddressablesConfigs();
		CheckUniqueAddressablesKeys();
		RebuildAddressablesContent();
	}

	[MenuItem("Tools/City/Delete save %&r")]
	public static void DeleteSave()
	{
		var directory = "Assets/Save";
		if (Directory.Exists(directory))
		{
			Directory.Delete(directory, true);
			AssetDatabase.Refresh();
		}

		Debug.Log("Save deleted!");
	}

	[MenuItem("Tools/City/Sync addressables keys")]
	public static void SyncAddressablesKeys()
	{
		var blacklist = new HashSet<string> {"EditorSceneList"};
		var groups = EditorUtils.LoadAssets<AddressableAssetGroup>();
		foreach (var assetGroup in groups)
		{
			foreach (var entry in assetGroup.entries)
			{
				var key = Path.GetFileNameWithoutExtension(entry.AssetPath);
				if (string.IsNullOrEmpty(key) || blacklist.Contains(key) || blacklist.Contains(entry.address))
				{
					continue;
				}

				if (key == entry.address)
				{
					continue;
				}

				Debug.LogFormat("Asset address changed from '{0}' to '{1}'", entry.address, key);
				entry.SetAddress(key);
			}

			EditorUtility.SetDirty(assetGroup);
		}

		Debug.Log("Syncing Addressables Keys finished!");
	}

	[MenuItem("Tools/City/Add Missing Addressables Configs")]
	public static void AddMissingAddressablesConfigs()
	{
		var addMethod = typeof(AddressableAssetGroup).GetMethod("AddAssetEntry", BindingFlags.NonPublic | BindingFlags.Instance);

		var processedFolders = new HashSet<string> {"*/Resources"};
		var groups = EditorUtils.LoadAssets<AddressableAssetGroup>();
		foreach (var assetGroup in groups)
		{
			var missingEntries = new List<AddressableAssetEntry>();
			foreach (var entry in assetGroup.entries)
			{
				var path = entry.AssetPath;
				if (string.IsNullOrEmpty(path))
				{
					continue;
				}

				var folder = Path.GetDirectoryName(path);
				if (string.IsNullOrEmpty(folder) || processedFolders.Contains(folder))
				{
					continue;
				}

				var extension = Path.GetExtension(path);

				processedFolders.Add(folder);

				var files = Directory.GetFiles(folder).Where(file =>
					!string.IsNullOrEmpty(file) && !Path.GetFileName(file).StartsWith(".") && Path.GetExtension(file) == extension &&
					assetGroup.entries.All(e => e.AssetPath != file));
				foreach (var file in files)
				{
					var missingEntry = CreateEntry(AssetDatabase.AssetPathToGUID(file), file, assetGroup);
					missingEntry.address = Path.GetFileNameWithoutExtension(file);
					foreach (var label in entry.labels)
					{
						missingEntry.SetLabel(label, true);
					}

					missingEntries.Add(missingEntry);
				}
			}

			foreach (var missingEntry in missingEntries)
			{
				addMethod.Invoke(assetGroup, new object[] {missingEntry, true});
				Debug.LogFormat("Missing asset '{0}' added to group '{1}' with labels {2}", missingEntry.AssetPath, assetGroup.Name,
					missingEntry.labels.Print());
			}

			EditorUtility.SetDirty(assetGroup);
		}

		Debug.Log("Adding Missing Addressables Configs finished!");
	}

	private static AddressableAssetEntry CreateEntry(string guid, string address, AddressableAssetGroup parent)
	{
		return (AddressableAssetEntry)Createinstance(guid, address, parent, false);
	}

	private static object Createinstance(params object[] args)
	{
		var flags = BindingFlags.NonPublic | BindingFlags.Instance;
		return Activator.CreateInstance(typeof(AddressableAssetEntry), flags, null, args, null);
	}

	[MenuItem("Tools/City/Check Unique Addressables Keys")]
	private static void CheckUniqueAddressablesKeys()
	{
		var usedKeys = new Dictionary<string, List<string>>();
		var groups = EditorUtils.LoadAssets<AddressableAssetGroup>();
		var atlasConfig = EditorUtils.LoadAsset<AtlasInfoConfig>();
		foreach (var assetGroup in groups)
		foreach (var entry in assetGroup.entries)
		{
			if (!usedKeys.TryGetValue(entry.address, out var list))
			{
				list = new List<string>();
				usedKeys.Add(entry.address, list);
			}

			list.Add(entry.AssetPath);
		}

		foreach (var atlasEntry in atlasConfig.AtlasInfo)
		{
			if (!usedKeys.TryGetValue(atlasEntry.Key, out var list))
			{
				list = new List<string>();
				usedKeys.Add(atlasEntry.Key, list);
			}

			list.Add(atlasEntry.Value);
		}

		foreach (var list in usedKeys.Values)
		{
			if (list.Count > 1)
			{
				Debug.LogErrorFormat("Duplicate keys found in files : \n{0}", list.Print());
			}
		}

		Debug.Log("Checking Unique Addressables Keys finished!");
	}

	private static void RebuildAddressablesContent()
	{
		AddressableAssetSettings.CleanPlayerContent();
		AddressableAssetSettings.BuildPlayerContent();
		Debug.Log("Addressables content rebuilded!");
	}
}