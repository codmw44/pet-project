﻿using System;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace Utility
{
    /// <summary>
    /// create concrete item through reflection from JSON
    /// usage:
    /// 
    /// 	[JsonConverter(typeof(JsonDynamicConverter&lt;BaseClassType, DefaultTypeResolver&gt;))]
    /// 	public abstract class BaseClassType
    /// 	{
    /// 	   ....
    /// 	}
    /// </summary>
    public class JsonDynamicConverter<TBaseClass, TTypeResolver> : JsonConverter
        where TBaseClass : class where TTypeResolver : IJsonDynamicConverterResolver, new()
    {
        private class BaseSpecifiedConcreteClassConverter : DefaultContractResolver
        {
            protected override JsonConverter ResolveContractConverter(Type objectType)
            {
                if (typeof(TBaseClass).IsAssignableFrom(objectType) && !objectType.IsAbstract)
                {
                    return null;
                }

                return base.ResolveContractConverter(objectType);
            }
        }

        private static readonly JsonSerializer JsonSerializer = new JsonSerializer() {ContractResolver = new BaseSpecifiedConcreteClassConverter()};
        private static readonly TTypeResolver TypeResolver = new TTypeResolver();

        public override bool CanConvert(Type objectType)
        {
            return typeof(TBaseClass).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jsonObject = JObject.Load(reader);

            var t = TypeResolver.GetType(jsonObject);
            if (t == null)
            {
                throw new Exception($"Cannot detect class from Json object '{jsonObject.ToString().Take(128)}'");
            }

            var result = jsonObject.ToObject(t, JsonSerializer);

            return result;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var jsonObject = JObject.FromObject(value, JsonSerializer);
            TypeResolver.WriteType(jsonObject, value.GetType());
            jsonObject.WriteTo(writer);
        }
    }

    public interface IJsonDynamicConverterResolver
    {
        /// <summary>
        /// load class name from object and resolve it
        /// </summary>
        Type GetType(JObject jsonObject);

        /// <summary>
        /// save class name from object
        /// </summary>
        void WriteType(JObject jsonObject, Type t);
    }

    /// <summary>
    /// resolve type from field '_className'
    /// </summary>
    public class DefaultTypeResolver : IJsonDynamicConverterResolver
    {
        public Type GetType(JObject jsonObject)
        {
            var typeName = jsonObject["_className"]?.ToString().Trim() ?? string.Empty;
            var t = Types.GetType(typeName);
            if (t == null)
            {
                throw new Exception($"Cannot resolve _className '{typeName}' while reading JSON");
            }

            return t;
        }

        public void WriteType(JObject jsonObject, Type t)
        {
            jsonObject["_className"] = t.FullName;
        }
    }
}