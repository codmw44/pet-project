using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Utility
{
	public static class TraceLog
	{
		private static readonly Dictionary<string, Stopwatch> _timers = new Dictionary<string, Stopwatch>();

		private static readonly StringBuilder _sb = new StringBuilder();

		[Conditional("DEVELOPMENT_BUILD")]
		public static void Print(params object[] args)
		{
			_sb.Length = 0;

			var frame = new StackTrace().GetFrame(1);

			var method = frame.GetMethod();

			if (method.ReflectedType != null)
			{
				_sb.Append(method.ReflectedType.Name).Append(".");
			}

			_sb.Append(method.Name).Append("(");

			if (args.Length > 0)
			{
				_sb.Append(" ");
			}

			var parameters = method.GetParameters();
			for (var i = 0; i < args.Length; ++i)
			{
				_sb.Append(parameters[i].Name);
				_sb.Append(" = ");
				_sb.Append(ArgumentToString(args[i], parameters[i].ParameterType));
				if (i < args.Length - 1)
				{
					_sb.Append(", ");
				}
			}

			if (args.Length > 0)
			{
				_sb.Append(" ");
			}

			_sb.Append(")");

			Debug.Log(_sb.ToString());
		}

		[Conditional("DEVELOPMENT_BUILD")]
		public static void Start(string msg)
		{
			Stopwatch sw;
			if (_timers.TryGetValue(msg, out sw))
			{
				sw.Start();
			}
			else
			{
				sw = new Stopwatch();
				_timers.Add(msg, sw);
			}

			sw.Start();
		}

		[Conditional("DEVELOPMENT_BUILD")]
		public static void Pause(string msg)
		{
			Stopwatch sw;
			if (_timers.TryGetValue(msg, out sw))
			{
				sw.Stop();
			}
			else
			{
				_sb.Append("Timer with message '");
				_sb.Append(msg);
				_sb.Append("' not found");
				Debug.LogError(_sb.ToString());
			}
		}

		[Conditional("DEVELOPMENT_BUILD")]
		public static void Stop(string msg)
		{
			_sb.Length = 0;

			Stopwatch sw;
			if (_timers.TryGetValue(msg, out sw))
			{
				sw.Stop();
				_sb.Append(msg);
				_sb.Append(" : ");
				_sb.Append(sw.ElapsedMilliseconds);
				_sb.Append("ms");

				Debug.Log(_sb.ToString());

				_timers.Remove(msg);
			}
			else
			{
				_sb.Append("Timer with message '");
				_sb.Append(msg);
				_sb.Append("' not found");
				Debug.LogError(_sb.ToString());
			}
		}

		private static string ArgumentToString(object argument, Type argumentType)
		{
			var result = string.Empty;
			if (typeof(string).IsAssignableFrom(argumentType))
			{
				result = "'" + argument + "'";
			}
			else if (typeof(GameObject).IsAssignableFrom(argumentType))
			{
				result = ((GameObject)argument).name;
			}
			else
			{
				foreach (var t in argumentType.GetInterfaces())
				{
					if (t.IsGenericType)
					{
						var typedef = t.GetGenericTypeDefinition();
						if (typeof(IEnumerable<>).IsAssignableFrom(typedef))
						{
							var enumerable = (IEnumerable)argument;
							result = enumerable.Print();
							break;
						}
					}
				}
			}

			if (string.IsNullOrEmpty(result))
			{
				result = argument.ToString();
			}

			return result;
		}

		public static string Print<T>(this IEnumerable<T> source, Func<T, string> print = null)
		{
			var stringBuilder = new StringBuilder();
			stringBuilder.Append("[");

			foreach (var v in source)
			{
				if (print != null)
				{
					stringBuilder.Append(print(v));
				}
				else
				{
					stringBuilder.Append(v);
				}

				stringBuilder.Append(", ");
			}

			stringBuilder.Append("]");
			return stringBuilder.ToString().Replace(", ]", "]");
		}

		public static string Print(this IEnumerable source, Func<object, string> print = null)
		{
			var stringBuilder = new StringBuilder();
			stringBuilder.Append("[");

			foreach (var v in source)
			{
				if (print != null)
				{
					stringBuilder.Append(print(v));
				}
				else
				{
					stringBuilder.Append(v);
				}

				stringBuilder.Append(", ");
			}

			stringBuilder.Append("]");
			return stringBuilder.ToString().Replace(", ]", "]");
		}
	}
}