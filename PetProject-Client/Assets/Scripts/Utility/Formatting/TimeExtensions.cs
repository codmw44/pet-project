using System;
using I2.Loc;

namespace Utility.Formatting
{
	public static class TimeExtensions
	{
		private const float Minute = 60;
		private const float Hour = Minute * 60;
		private const float Day = Hour * 24;

		private const string CommonDay = "timer/d";
		private const string CommonHour = "timer/h";
		private const string CommonMinute = "timer/m";
		private const string CommonSecond = "timer/s";

		public static string ToFormattedString(this TimeSpan timeSpan, bool canBeShort = true)
		{
			string result;

			var day = LocalizationManager.GetTermTranslation(CommonDay);
			var hour = LocalizationManager.GetTermTranslation(CommonHour);
			var minute = LocalizationManager.GetTermTranslation(CommonMinute);
			var second = LocalizationManager.GetTermTranslation(CommonSecond);

			var totalSeconds = timeSpan.TotalSeconds;
			if (totalSeconds >= Day)
			{
				result = string.Format("{0}{1} {2}{3}", timeSpan.Days, day, timeSpan.Hours, hour);
			}
			else if (totalSeconds >= Hour)
			{
				if (!canBeShort || totalSeconds % Hour > 0)
				{
					result = string.Format("{0}{1} {2}{3}", timeSpan.Hours, hour, timeSpan.Minutes, minute);
				}
				else
				{
					result = string.Format("{0}{1}", timeSpan.Hours, hour);
				}
			}
			else if (totalSeconds >= Minute)
			{
				if (!canBeShort || totalSeconds % Minute > 0)
				{
					result = string.Format("{0}{1} {2}{3}", timeSpan.Minutes, minute, timeSpan.Seconds, second);
				}
				else
				{
					result = string.Format("{0}{1}", timeSpan.Minutes, minute);
				}
			}
			else
			{
				result = string.Format("{0}{1}", timeSpan.Seconds, second);
			}

			return result;
		}

		public static string SecondsToTime(this float seconds, bool canBeShort = false)
		{
			return TimeSpan.FromSeconds(seconds).ToFormattedString(canBeShort);
		}

		public static string SecondsToTime(this double seconds, bool canBeShort = false)
		{
			return TimeSpan.FromSeconds(seconds).ToFormattedString(canBeShort);
		}
	}
}