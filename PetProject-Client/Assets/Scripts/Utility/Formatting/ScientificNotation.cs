﻿using I2.Loc;
using UnityEngine.Scripting;

namespace Utility.Formatting
{
	[Preserve]
	public static class ScientificNotation
	{
		public static string ToShortString(this int value)
		{
			return value.ToString();
		}

		public static string ToPerSeconds(this int value)
		{
			return $"{ToShortString(value)}/{LocalizationManager.GetTranslation("timer/s")}";
		}
	}
}