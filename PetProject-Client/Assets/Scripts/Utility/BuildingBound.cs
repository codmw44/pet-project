using System;
using UnityEngine;

namespace Utility
{
	[Serializable]
	public class BuildingBound
	{
		public BuildingBound(Vector3 downLeft, Vector3 downRight, Vector3 upRight, Vector3 upLeft)
		{
			DownLeft = downLeft;
			DownRight = downRight;
			UpRight = upRight;
			UpLeft = upLeft;
		}

		public BuildingBound()
		{
		}

		[field: SerializeField] public Vector3 DownLeft { get; }

		[field: SerializeField] public Vector3 DownRight { get; }

		[field: SerializeField] public Vector3 UpRight { get; }

		[field: SerializeField] public Vector3 UpLeft { get; }

		public Vector3[] GetArray()
		{
			var array = new Vector3[4];
			array[0] = DownLeft;
			array[1] = DownRight;
			array[2] = UpRight;
			array[3] = UpLeft;
			return array;
		}
	}
}