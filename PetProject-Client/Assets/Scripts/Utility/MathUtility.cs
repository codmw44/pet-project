﻿using System;
using UnityEngine;

namespace Utility
{
	public static class MathUtility
	{
		/// N-ый член арифметической прогессии
		public static double ArithmeticProgression(double first, double increaseNumber, int index)
		{
			return first + (index - 1) * increaseNumber;
		}

		/// Сумма n последовательных членов АП
		public static double ArithmeticProgressionSum(double first, double increaseNumber, int startIndex, int count)
		{
			var startNumber = ArithmeticProgression(first, increaseNumber, startIndex);
			return (startNumber * 2 + increaseNumber * (count - 1)) / 2 * count;
		}

		/// N-ый член геометрической прогессии
		public static double GeometricProgression(double first, double multiplierNumber, int index)
		{
			return first * Math.Pow(multiplierNumber, index - 1);
		}

		/// Сумма n последовательных членов ГП
		public static double GeometricProgressionSum(double first, double multiplierNumber, int startIndex, int count)
		{
			var startNumber = GeometricProgression(first, multiplierNumber, startIndex);
			double summ;
			if (multiplierNumber == 1.0)
			{
				summ = startNumber * count;
			}
			else
			{
				summ = startNumber * ((Math.Pow(multiplierNumber, count) - 1) / (multiplierNumber - 1));
			}

			return summ;
		}

		public static int Square(this Vector2Int vector)
		{
			return vector.x * vector.y;
		}
	}
}