﻿using System.Collections.Generic;
using UnityEngine;

namespace Utility
{
	public class WaypointsGizmos : MonoBehaviour
	{
		private readonly List<Vector3> _points = new List<Vector3>();

		private void OnDrawGizmos()
		{
			_points.Clear();
			foreach (Transform child in transform)
			{
				_points.Add(child.position);
			}

			if (_points.Count < 3)
			{
				return;
			}

			Gizmos.color = Color.blue;
			Gizmos.DrawLine(_points[0], _points[1]);

			for (var i = 1; i < _points.Count - 2; i++)
			{
				Gizmos.color = Color.red;
				Gizmos.DrawWireSphere(_points[i], 2);
				Gizmos.color = Color.white;
				Gizmos.DrawLine(_points[i], _points[i + 1]);
			}

			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(_points[_points.Count - 2], 2);
			Gizmos.color = Color.white;

			Gizmos.DrawLine(_points[_points.Count - 2], _points[1]);

			Gizmos.color = Color.green;
			Gizmos.DrawLine(_points[1], _points[_points.Count - 1]);
		}
	}
}