using System.Collections.Generic;
using UnityEngine;

namespace Utility
{
	public static class BuildingSortingOrderUtility
	{
		private const int QuarterDeltaSort = 2;
		private const int DefaultQuarterPositionDelta = 9500;
		private const int IrregularPositionDelta = 4;
		private const int ZOrderMultiplier = 5;

		private static readonly int[,] Orders = {{0, 10, 29, 60}, {10, 30, 40, 60}, {29, 38, 50, 60}};

		private static readonly HashSet<int> IrregularXPositions = new HashSet<int> {2};

		public static int CalculateSortingOrder(Vector2Int buildingPosition, Vector2Int buildingSize, Vector2Int quarterPosition)
		{
			var sum = 0;
			var count = 0;
			for (var x = 0; x < buildingSize.x; x++)
			for (var y = 0; y < buildingSize.y; y++)
			{
				var sortOrder = GetSortOrder(buildingPosition + new Vector2Int(x, y));
				sum += sortOrder;
				count++;
			}

			if (IrregularXPositions.Contains(buildingPosition.x))
			{
				sum += IrregularPositionDelta;
			}

			return sum * ZOrderMultiplier / count + GetQuarterOffset(quarterPosition);
		}

		private static int GetSortOrder(Vector2Int position)
		{
			if (position.x < 0 || position.y < 0 || position.x >= Orders.GetLength(0) || position.y >= Orders.GetLength(1))
			{
				return 0;
			}

			return Orders[position.x, position.y];
		}

		private static int GetQuarterOffset(Vector2Int quarterPosAtGrid)
		{
			return QuarterDeltaSort * quarterPosAtGrid.sqrMagnitude - DefaultQuarterPositionDelta;
		}
	}
}