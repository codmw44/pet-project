using System;

namespace Utility.Extensions
{
	public class TaskException : AggregateException
	{
		public override string StackTrace { get; }

		public TaskException(Exception exception, string stackTrace) : base(exception)
		{
			StackTrace = stackTrace;
		}

		public TaskException(string message, string stackTrace) : base(message)
		{
			StackTrace = stackTrace;
		}
	}
}