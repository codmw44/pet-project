using UnityEngine;

namespace Utility.Extensions
{
	public static class VectorIntExtension
	{
		public static Vector3Int ToVector3Int(this Vector2Int vector)
		{
			return new Vector3Int(vector.x, vector.y, 0);
		}

		public static Vector2Int ToVector2Int(this Vector3Int vector)
		{
			return new Vector2Int(vector.x, vector.y);
		}
	}
}