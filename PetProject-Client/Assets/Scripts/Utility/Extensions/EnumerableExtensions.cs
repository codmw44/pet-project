using System.Collections.Generic;
using UnityEngine;

namespace Utility.Extensions
{
	public static class EnumerableExtensions
	{
		public static T GetRandom<T>(this T[] array)
		{
			return array[Random.Range(0, array.Length)];
		}

		public static T GetRandom<T>(this List<T> list)
		{
			return list[Random.Range(0, list.Count)];
		}

		public static float GetRandom(this Vector2 vector)
		{
			return Random.Range(vector.x, vector.y);
		}
	}
}