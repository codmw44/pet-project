using System;
using System.Numerics;

namespace Utility.Extensions
{
	public static class BigIntegerExtensions
	{
		public static double Divide(this BigInteger source, BigInteger target)
		{
			return Math.Exp(BigInteger.Log(source) - BigInteger.Log(target));
		}
	}
}