using System;

namespace Utility.Extensions
{
	public static class DateTimeExtension
	{
		public static bool IsNight(this DateTime time)
		{
			return time.Hour >= 23 || time.Hour < 8;
		}
	}
}