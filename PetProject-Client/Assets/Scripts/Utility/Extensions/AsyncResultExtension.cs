using UnityEngine;
using UnityFx.Async;
using UnityFx.Async.Promises;

namespace Utility.Extensions
{
	public static class AsyncResultExtension
	{
		public static IAsyncOperation DefaultCatch(this IAsyncOperation operation)
		{
			operation.Catch(Debug.LogError);
			return operation;
		}
	}
}