﻿using UnityEngine;

namespace Utility
{
	public static class BuildingSizeIconUtility
	{
		private const string Icon1X1 = "Icon_1x1";
		private const string Icon1X2 = "Icon_1x2";
		private const string Icon2X1 = "Icon_2x1";
		private const string Icon2X2 = "Icon_2x2";
		private const string Icon2X3 = "Icon_2x3";
		private const string Icon3X2 = "Icon_3x2";
		private const string Icon3X3 = "Icon_3x3";

		public static string GetSizeIconName(Vector2Int size)
		{
			if ((size.x == 1) & (size.y == 1))
			{
				return Icon1X1;
			}

			if ((size.x == 1) & (size.y == 2))
			{
				return Icon1X2;
			}

			if ((size.x == 2) & (size.y == 1))
			{
				return Icon2X1;
			}

			if ((size.x == 2) & (size.y == 2))
			{
				return Icon2X2;
			}

			if ((size.x == 2) & (size.y == 3))
			{
				return Icon2X3;
			}

			if ((size.x == 3) & (size.y == 2))
			{
				return Icon3X2;
			}

			return Icon3X3;
		}
	}
}