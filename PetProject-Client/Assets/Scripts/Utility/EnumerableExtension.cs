using System;
using System.Collections.Generic;

namespace Utility
{
	public static class EnumerableExtension
	{
		public static float Multiply<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector)
		{
			if (source == null)
			{
				throw new NullReferenceException(nameof(source));
			}

			if (selector == null)
			{
				throw new NullReferenceException(nameof(selector));
			}

			var num = 1.0;
			foreach (var source1 in source)
			{
				num *= selector(source1);
			}

			return (float)num;
		}
	}
}