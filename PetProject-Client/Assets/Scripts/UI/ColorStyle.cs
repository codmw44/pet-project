using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
	[Serializable]
	public class ColorStyle
	{
		[SerializeField] private ColorForObjs[] _colors;

		[Button]
		public void ApplyStyle()
		{
			foreach (var color in _colors)
			{
				foreach (var graphic in color.Graphics)
				{
					graphic.color = color.Color;
				}

				foreach (var graphic in color.Outlines)
				{
					graphic.effectColor = color.Color;
				}
			}
		}

		[Serializable]
		private class ColorForObjs
		{
			[field: SerializeField] public Color Color { get; }

			[field: SerializeField] public List<Graphic> Graphics { get; }

			[field: SerializeField] public List<Shadow> Outlines { get; }
		}
	}
}