using UI.Infrastructure;
using UnityFx.Async;

namespace UI
{
	public abstract class PayloadedUIWindow<TPayload> : UIWindow
	{
		protected abstract void OnShow(TPayload payload);

		public IAsyncOperation Show(TPayload payload, bool instantAction = false)
		{
			OnShow(payload);
			return base.Show(instantAction);
		}

		protected sealed override void OnShow()
		{
		}
	}
}