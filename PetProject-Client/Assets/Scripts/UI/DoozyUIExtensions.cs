using Doozy.Engine.UI;
using UnityEngine;
using UnityEngine.Events;
using UnityFx.Async;

namespace UI
{
	public static class DoozyUIExtensions
	{
		public static IAsyncOperation ShowAsync(this UIView view, bool isInstant = false)
		{
			if (view.IsVisible)
			{
				return UnityFx.Async.AsyncResult.CompletedOperation;
			}

			var AsyncResult = new AsyncCompletionSource();

			view.Show(isInstant);

			UnityAction<float> action = null;
			action = value =>
			{
				if (Mathf.Approximately(value, 1f))
				{
					view.OnVisibilityChanged.RemoveListener(action);

					AsyncResult.SetCompleted();
				}
			};

			view.OnVisibilityChanged.AddListener(action);
			return AsyncResult;
		}

		public static IAsyncOperation HideAsync(this UIView view, bool isInstant = false)
		{
			if (view.IsHidden)
			{
				return UnityFx.Async.AsyncResult.CompletedOperation;
			}

			var AsyncResult = new AsyncCompletionSource();

			view.Hide(isInstant);

			UnityAction<float> action = null;
			action = value =>
			{
				if (Mathf.Approximately(value, 0f))
				{
					view.OnVisibilityChanged.RemoveListener(action);

					AsyncResult.SetCompleted();
				}
			};

			view.OnVisibilityChanged.AddListener(action);
			return AsyncResult;
		}
	}
}