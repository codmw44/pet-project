using System.Linq;
using Doozy.Engine.UI;
using UnityEngine;
using UnityFx.Async;

namespace UI.Infrastructure
{
	[RequireComponent(typeof(UIView))]
	public class UIWindow : MonoBehaviour
	{
		private UIView _view;

		public UIView View
		{
			get
			{
				if (_view == null)
				{
					_view = GetComponent<UIView>();
				}

				return _view;
			}
		}

		public bool IsVisible => View.IsVisible;

		protected virtual void OnShow()
		{
		}

		protected virtual void OnHide()
		{
		}

		public IAsyncOperation Show(bool instantAction = false)
		{
			if (this == null)
			{
				return AsyncResult.CompletedOperation;
			}

			OnShow();

			if (IsDefaultView())
			{
				return View.ShowAsync(instantAction);
			}

			return AsyncResult.WhenAll(UIView.GetViews(View.ViewCategory, View.ViewName).Select(view => view.ShowAsync(instantAction)));
		}

		public IAsyncOperation Hide(bool instantAction = false)
		{
			if (this == null)
			{
				return AsyncResult.CompletedOperation;
			}

			OnHide();

			if (IsDefaultView())
			{
				return View.HideAsync(instantAction);
			}

			return AsyncResult.WhenAll(UIView.GetViews(View.ViewCategory, View.ViewName).Select(view => view.HideAsync(instantAction)));
		}

		private bool IsDefaultView()
		{
			return View.ViewCategory == UIView.DefaultViewCategory || View.ViewName == UIView.DefaultViewName;
		}
	}
}