using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UI.Infrastructure
{
	public class UIWindowContainer
	{
		private readonly Dictionary<Type, TypeContainer> _windows = new Dictionary<Type, TypeContainer>();

		public void RegisterWindow<T>(T window, string key = "") where T : UIWindow
		{
			var type = typeof(T);
			if (!_windows.TryGetValue(type, out var storage))
			{
				storage = new TypeContainer();
				storage.Windows.Add(key, new List<UIWindow>());
				_windows.Add(type, storage);
			}

			if (storage.Windows.TryGetValue(key, out var value))
			{
				value.Add(window);
			}
			else
			{
				storage.Windows.Add(key, new List<UIWindow> {window});
			}
		}

		public void RegisterWindows<T>(IEnumerable<T> windows, string key = "") where T : UIWindow
		{
			foreach (var window in windows)
			{
				RegisterWindow(window, key);
			}
		}

		public T GetWindow<T>(string key = "") where T : UIWindow
		{
			return GetWindows<T>(key).FirstOrDefault();
		}

		public IEnumerable<T> GetWindows<T>(string key = "") where T : UIWindow
		{
			var type = typeof(T);
			if (!_windows.TryGetValue(type, out var storage))
			{
				Debug.Log(new KeyNotFoundException("type"));
				return new List<T>();
			}

			if (!storage.Windows.TryGetValue(key, out var windows))
			{
				Debug.Log(new KeyNotFoundException("key"));
				return new List<T>();
			}

			return windows.Select(o => o as T);
		}

		public void ClearUnusedWindows()
		{
			foreach (var storage in _windows.Values)
			{
				var unused = new List<string>();
				foreach (var windowKey in storage.Windows.Keys)
				foreach (var window in storage.Windows[windowKey])
				{
					if (window == null || window.gameObject == null)
					{
						unused.Add(windowKey);
					}
				}

				var unusedCount = unused.Count;
				for (var i = 0; i < unusedCount; i++)
				{
					var key = unused[i];
					storage.Windows.Remove(key);
				}
			}
		}

		private class TypeContainer
		{
			public Dictionary<string, List<UIWindow>> Windows { get; } = new Dictionary<string, List<UIWindow>>();
		}
	}
}