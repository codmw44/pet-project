﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
	public class UIGradient : BaseMeshEffect
	{
		[SerializeField] private Color _botLeftColor = Color.black;
		[SerializeField] private Color _botRightColor = Color.white;
		[SerializeField] private Color _topLeftColor = Color.black;
		[SerializeField] private Color _topRightColor = Color.white;

		public override void ModifyMesh(VertexHelper vertexHelper)
		{
			if (!IsActive())
			{
				return;
			}

			var vertices = new List<UIVertex>();
			vertexHelper.GetUIVertexStream(vertices);

			ModifyVertices(vertices);

			vertexHelper.Clear();
			vertexHelper.AddUIVertexTriangleStream(vertices);
		}

		private void ModifyVertices(List<UIVertex> vertices)
		{
			if (!IsActive() || vertices.Count == 0)
			{
				return;
			}

			var count = vertices.Count;
			var bottomX = vertices[0].position.x;
			var bottomY = vertices[0].position.y;
			var topX = vertices[0].position.x;
			var topY = vertices[0].position.y;

			GetTopAndBottomPosition(vertices, count, ref topX, ref bottomX, ref topY, ref bottomY);

			var width = topX - bottomX;
			var height = topY - bottomY;

			for (var i = 0; i < count; i++)
			{
				var uiVertex = vertices[i];

				var xDelta = (uiVertex.position.x - bottomX) / width;
				var yDelta = (uiVertex.position.y - bottomY) / height;

				var topColor = Color.Lerp(_topLeftColor, _topRightColor, xDelta);
				var botColor = Color.Lerp(_botLeftColor, _botRightColor, xDelta);
				var resultColor = Color.Lerp(topColor, botColor, yDelta);

				uiVertex.color = uiVertex.color * resultColor;

				vertices[i] = uiVertex;
			}
		}

		private static void GetTopAndBottomPosition(List<UIVertex> vertices, int count, ref float topX, ref float bottomX, ref float topY, ref float bottomY)
		{
			for (var i = 1; i < count; i++)
			{
				var x = vertices[i].position.x;
				var y = vertices[i].position.y;

				if (x > topX)
				{
					topX = x;
				}
				else if (x < bottomX)
				{
					bottomX = x;
				}

				if (y > topY)
				{
					topY = y;
				}
				else if (y < bottomY)
				{
					bottomY = y;
				}
			}
		}
	}
}