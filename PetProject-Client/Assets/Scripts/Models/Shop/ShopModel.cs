using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;

namespace Models.Shop
{
	public class ShopModel
	{
		[TableList(AlwaysExpanded = true)] public Dictionary<ShopType, List<ShopItemModel>> ShopItems;

		[Button]
		private void SortItemsById()
		{
			foreach (var shopItemModel in ShopItems)
			{
				ShopItems[shopItemModel.Key] = shopItemModel.Value.OrderBy(item => item.Id).ToList();
			}
		}
	}
}