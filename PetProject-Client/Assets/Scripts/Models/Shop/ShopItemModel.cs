using Models.Price;
using Models.Rewards;
using Models.ViewFilters;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

namespace Models.Shop
{
	public class ShopItemModel
	{
		public ShopItemModel()
		{
		}

		public ShopItemModel(int id, IPrice price, IReward reward)
		{
			Id = id;
			Price = price;
			Reward = reward;
		}

		public ShopItemModel(int id, IPrice price, IReward reward, ITimerViewFilter viewFilter)
		{
			Id = id;
			Price = price;
			Reward = reward;
			ViewFilter = viewFilter;
		}

		[field: TableColumnWidth(48, false), OdinSerialize]
		public int Id { get; }

		[field: OdinSerialize] public IPrice Price { get; }

		[field: OdinSerialize] public IReward Reward { get; }

		[field: OdinSerialize] public ITimerViewFilter ViewFilter { get; }
	}
}