using Ecs;

namespace Models.ViewFilters
{
	public interface ITimerViewFilter : IViewFilter
	{
		float GetTime(EcsContexts contexts);
	}
}