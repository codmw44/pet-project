using Ecs;

namespace Models.ViewFilters
{
	public class HardVideoFilter : ITimerViewFilter
	{
		public bool IsVisible(EcsContexts contexts)
		{
			return true;
		}

		public float GetTime(EcsContexts contexts)
		{
			return IsAvailable(contexts) || !contexts.GameContext.isHardByAds ? 0f : contexts.GameContext.hardByAdsEntity.timer.Current;
		}

		private static bool IsAvailable(EcsContexts contexts)
		{
			var isHardByAds = contexts.GameContext.isHardByAds;
			return !isHardByAds || isHardByAds && !contexts.GameContext.hardByAdsEntity.hasTimer;
		}
	}
}