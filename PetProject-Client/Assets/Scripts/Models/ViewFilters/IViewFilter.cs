using Ecs;

namespace Models.ViewFilters
{
	public interface IViewFilter
	{
		bool IsVisible(EcsContexts contexts);
	}
}