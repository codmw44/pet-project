using Ecs;

namespace Models.ViewFilters
{
	public class SoftVideoFilter : ITimerViewFilter
	{
		public bool IsVisible(EcsContexts contexts)
		{
			return true;
		}

		public float GetTime(EcsContexts contexts)
		{
			return IsAvailable(contexts) || !contexts.GameContext.isSoftByAds ? 0f : contexts.GameContext.softByAdsEntity.timer.Current;
		}

		private static bool IsAvailable(EcsContexts contexts)
		{
			var isSoftByAds = contexts.GameContext.isSoftByAds;
			return !isSoftByAds || isSoftByAds && !contexts.GameContext.softByAdsEntity.hasTimer;
		}
	}
}