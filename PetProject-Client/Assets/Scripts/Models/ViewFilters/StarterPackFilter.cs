using Ecs;

namespace Models.ViewFilters
{
	public class StarterPackFilter : ITimerViewFilter
	{
		public bool IsVisible(EcsContexts contexts)
		{
			//todo add trigger
			return contexts.GlobalContext.hardCurrency.Value > 30d;
		}

		public float GetTime(EcsContexts contexts)
		{
			return 10f;
		}
	}
}