namespace Models.Characters
{
    public enum CharacterBehaviourType
    {
        Grounded,
        Fly,
        Ghost
    }
}