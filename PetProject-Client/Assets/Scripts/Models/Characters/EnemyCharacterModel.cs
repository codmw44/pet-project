using Models.Characters.Infrastructure;
using UnityEngine;

namespace Models.Characters
{
    public class EnemyCharacterModel : BaseCharacterModel
    {
        [SerializeField] private CharacterBehaviourType _characterBehaviourType;

        public CharacterBehaviourType CharacterBehaviourType => _characterBehaviourType;
    }
}