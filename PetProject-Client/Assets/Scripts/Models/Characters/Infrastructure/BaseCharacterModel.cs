using Configs;
using Models.Weapons.Infrastructure;
using UnityEngine;

namespace Models.Characters.Infrastructure
{
    public abstract class BaseCharacterModel
    {
        [SerializeField] private string _viewId;
        [SerializeField] private int _startHealth;
        [SerializeField] private float _speed = 0.1f;
        [SerializeField] private WeaponHolder _weaponHolder;

        public string ViewId => _viewId;
        public int StartHealth => _startHealth;
        public float Speed => _speed;
        public BaseWeaponModel WeaponModel => _weaponHolder.Item;

        protected BaseCharacterModel()
        {
        }
    }
}