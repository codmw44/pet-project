using System;

namespace Models
{
	[Serializable]
	public class PlayerModel
	{
		public int Hard;
		public int Soft;
	}
}