using System;
using Ecs;
using Ecs.Components.Global.Player;
using Entitas;
using Models.EcsTriggers.Infrastructure;
using UnityFx.Async;

namespace Models.EcsTriggers
{
	public class HasSoftTrigger : IEcsTrigger<int>
	{
		private IGroup<GlobalEntity> _group;

		public event Action<int> OnTrigged;

		public IAsyncOperation Initialize(EcsContexts ecsContexts)
		{
			var globalContext = ecsContexts.GlobalContext;

			OnTrigged?.Invoke(globalContext.hasSoftCurrency ? globalContext.softCurrency.Value : 0);

			_group = globalContext.GetGroup(GlobalMatcher.SoftCurrency);
			_group.OnEntityAdded += OnGroupAdded;

			return AsyncResult.CompletedOperation;
		}

		public void Dispose()
		{
			if (_group == null)
			{
				return;
			}

			_group.OnEntityAdded -= OnGroupAdded;
		}

		private void OnGroupAdded(IGroup<GlobalEntity> group, GlobalEntity entity, int index, IComponent component)
		{
			var population = (SoftCurrencyComponent)component;
			OnTrigged?.Invoke(population.Value);
		}
	}
}