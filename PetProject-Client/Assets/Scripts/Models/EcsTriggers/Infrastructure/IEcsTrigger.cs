using System;
using Ecs;
using UnityFx.Async;

namespace Models.EcsTriggers.Infrastructure
{
	public interface IEcsTrigger<T> : IDisposable
	{
		IAsyncOperation Initialize(EcsContexts ecsContexts);
		event Action<T> OnTrigged;
	}
}