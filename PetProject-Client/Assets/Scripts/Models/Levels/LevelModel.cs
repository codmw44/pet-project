using System.Collections.Generic;
using System.Linq;
using Configs;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEditor;
using UnityEngine;

namespace Models.Levels
{
	public class LevelModel
	{
		public const int SizeX = 11;
		
		[SerializeField] private SceneName _scene;
		[SerializeField] private string _id;
		[SerializeField] private Dictionary<SpawnPointModel, CharacterHolder> _enemies;

		[TableMatrix(DrawElementMethod = "DrawColoredEnumElement", SquareCells = true)] [SerializeField] [ValidateInput("ValidateInput")]
		private TypeCell[,] _level = new TypeCell[11, 10];

		public string Id => _id;
		public int SizeY => _level.GetLength(1);
		public Dictionary<SpawnPointModel, CharacterHolder> Enemies => _enemies;
		public TypeCell[,] Level => _level;
		public SceneName Scene => _scene;

#if UNITY_EDITOR

		private static Color NoneColor => new Color(0.3f, 0.3f, 0.3f);

		private static TypeCell DrawColoredEnumElement(Rect rect, TypeCell value)
		{
			if (Event.current.type == EventType.MouseDown && rect.Contains(Event.current.mousePosition))
			{
				var i = ((int) value);
				value = (TypeCell) ((i + 1) % ((int) TypeCell.Spawn + 1));

				if (Event.current.shift)
				{
					value = TypeCell.None;
				}

				GUI.changed = true;
				Event.current.Use();
			}

			EditorGUI.DrawTextureTransparent(rect.Padding(1), Resources.Load<Texture>(value.ToString().ToLower()));

			if (value == TypeCell.None)
			{
				EditorGUI.DrawRect(rect.Padding(1), NoneColor);
			}


			return value;
		}

		private bool ValidateInput(TypeCell[,] value)
		{
			if (value.GetLength(0) != SizeX)
			{
				Debug.Log("SizeX");
				return false;
			}

			return true;
		}

		[Button(ButtonSizes.Large)]
		private void SetSpawnPoints()
		{
			var countEnemies = new List<Vector2Int>();

			for (var xIndex = 0; xIndex < _level.GetLength(0); xIndex++)
			{
				for (var yIndex = 0; yIndex < _level.GetLength(1); yIndex++)
				{
					if (_level[xIndex, yIndex] == TypeCell.Spawn)
					{
						countEnemies.Add(new Vector2Int(xIndex, yIndex));
					}
				}
			}

			var enemiesModels = _enemies.Select(enemy => enemy.Value).ToList();
			_enemies = new Dictionary<SpawnPointModel, CharacterHolder>();
			var i = 0;
			foreach (var countEnemy in countEnemies)
			{
				_enemies.Add(new SpawnPointModel(countEnemy), enemiesModels[Mathf.Clamp(i, 0, enemiesModels.Count - 1)]);
				i++;
			}
		}
#endif
	}
}