using UnityEngine;

namespace Models.Levels
{
    public class SpawnPointModel
    {
        [SerializeField] private Vector2Int _pos;

        public Vector2Int Pos => _pos;

        public SpawnPointModel()
        {
        }

        public SpawnPointModel(Vector2Int pos)
        {
            _pos = pos;
        }
    }
}