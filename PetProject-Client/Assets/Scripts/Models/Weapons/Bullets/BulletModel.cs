using UnityEngine;

namespace Models.Weapons.Bullets
{
    public class BulletModel
    {
        [SerializeField] private string _viewId;
        [SerializeField] private int _damage;
        [SerializeField] private float _speed = 0.2f;

        public string ViewId => _viewId;
        public int Damage => _damage;
        public float Speed => _speed;

        public BulletModel()
        {
        }

        public BulletModel(string viewId, int damage, float speed)
        {
            _viewId = viewId;
            _damage = damage;
            _speed = speed;
        }
    }
}