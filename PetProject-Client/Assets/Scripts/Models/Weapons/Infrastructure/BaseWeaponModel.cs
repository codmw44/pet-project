using Configs;
using Models.Weapons.Bullets;
using UnityEngine;

namespace Models.Weapons.Infrastructure
{
    public abstract class BaseWeaponModel
    {
        [SerializeField] private string _viewId;
        [SerializeField] private float _shootsPerSecond;
        [SerializeField] private BulletHolder _bulletHolder;

        public string ViewId => _viewId;
        public BulletModel BulletModel => _bulletHolder.Item;
        public float ShootsPerSecond => _shootsPerSecond;

        protected BaseWeaponModel()
        {
        }

        
    }
}