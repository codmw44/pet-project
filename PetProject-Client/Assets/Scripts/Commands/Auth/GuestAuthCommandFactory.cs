using Zenject;

namespace Commands.Auth
{
    public class GuestAuthCommandFactory : IFactory<string, GuestAuthCommand>
    {
        private readonly DiContainer _diContainer;

        public GuestAuthCommandFactory(DiContainer diContainer)
        {
            _diContainer = diContainer;
        }

        public GuestAuthCommand Create(string param)
        {
            return _diContainer.Instantiate<GuestAuthCommand>(new[] {param});
        }
    }
}