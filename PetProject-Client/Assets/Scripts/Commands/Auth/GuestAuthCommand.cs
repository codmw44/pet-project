using Controllers.Server;
using Controllers.Server.Extensions;
using MagicOnion.Client;
using PetServer.Balancer.Models.Balance;
using PetServer.Defenitions.Commands.Infrastructure;
using PetServer.Defenitions.Models.Auth;
using PetServer.Models.Auth;
using PetServer.Services;
using UnityEngine;
using UnityFx.Async;
using UnityFx.Async.Extensions;
using UnityFx.Async.Promises;
using Utility.Extensions;
using Zenject;

namespace Commands.Auth
{
	public class GuestAuthCommand : MagicOnionCommand<AuthResponse, IAuthService>
	{
		private readonly string _deviceId;

		public GuestAuthCommand(ChannelController channelController, string deviceId) : base(channelController)
		{
			_deviceId = deviceId;
		}

		protected override ChannelType ChannelType => ChannelType.General;


		protected override IAsyncOperation<AuthResponse> Request(IAuthService service)
		{
			return service.ProcessGuestAuth(_deviceId, new ClientSpecModel().GetSpec()).ResponseAsync.ToAsync();
		}

		public class Factory : PlaceholderFactory<string, GuestAuthCommand>
		{
		}
	}
}