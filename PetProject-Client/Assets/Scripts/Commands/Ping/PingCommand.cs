using Controllers.Server;
using MagicOnion.Client;
using PetServer.Balancer.Models.Balance;
using PetServer.Defenitions.Commands.Infrastructure;
using PetServer.Defenitions.Models.Response;
using PetServer.Services;
using UnityFx.Async;
using UnityFx.Async.Extensions;
using UnityFx.Async.Promises;
using Zenject;

namespace Commands.Ping
{
	public class PingCommand : ICommand<Response>
	{
		private readonly ChannelController _channelController;
		private readonly ChannelType _channelType;

		public PingCommand(ChannelController channelController, ChannelType channelType)
		{
			_channelController = channelController;
			_channelType = channelType;
		}


		public IAsyncOperation<Response> Execute()
		{
			return _channelController.GetChannel(_channelType).
			                          Then(channel => AsyncResult.FromResult(MagicOnionClient.Create<IPingService>(channel))).
			                          Then(service => service.PingConnection().ResponseAsync.ToAsync()).
			                          Then(b => b
				                          ? AsyncResult.FromResult(new Response())
				                          : AsyncResult.FromResult(new Response {ErrorEnum = ResponseErrorEnum.InternalError}));
		}


		public class Factory : PlaceholderFactory<ChannelType, PingCommand>
		{
		}
	}
}