using PetServer.Balancer.Models.Balance;
using Zenject;

namespace Commands.Ping
{
	public class PingCommandFactory : IFactory<ChannelType, PingCommand>
	{
		private readonly DiContainer _diContainer;

		public PingCommandFactory(DiContainer diContainer)
		{
			_diContainer = diContainer;
		}

		public PingCommand Create(ChannelType param)
		{
			return _diContainer.Instantiate<PingCommand>(new object[] {param});
		}
	}
}