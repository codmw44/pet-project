using Models.Price;
using Zenject;

namespace PetServer.Defenitions.Commands.Finance
{
	public class ProcessPriceFactory : IFactory<IPrice, ProcessPriceCommand>
	{
		private readonly DiContainer _diContainer;

		public ProcessPriceFactory(DiContainer diContainer)
		{
			_diContainer = diContainer;
		}

		public ProcessPriceCommand Create(IPrice param)
		{
			return _diContainer.Instantiate<ProcessPriceCommand>(new []{param});
		}
	}
}