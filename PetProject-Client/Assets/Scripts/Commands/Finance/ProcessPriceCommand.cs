using Controllers;
using Controllers.Server;
using MagicOnion.Client;
using Models.Price;
using PetServer.Balancer.Models.Balance;
using PetServer.Defenitions.Commands.Infrastructure;
using PetServer.Defenitions.Models.Response;
using PetServer.Models.Auth;
using PetServer.Services;
using UnityEngine;
using UnityFx.Async;
using UnityFx.Async.Extensions;
using UnityFx.Async.Promises;
using Utility.Extensions;
using Zenject;

namespace PetServer.Defenitions.Commands.Finance
{
	public class ProcessPriceCommand : MagicOnionCommand<Response, IFinanceService>
	{
		private readonly Container<SessionToken> _sessionToken;
		private readonly IPrice _price;
		protected override ChannelType ChannelType => ChannelType.General;

		public ProcessPriceCommand(ChannelController channelController, Container<SessionToken> sessionToken, IPrice price) : base(channelController)
		{
			_sessionToken = sessionToken;
			_price = price;
		}

		protected override IAsyncOperation<Response> Request(IFinanceService service)
		{
			return service.ProcessPrice(_sessionToken.Value, _price).ResponseAsync.ToAsync();
		}

		public class Factory : PlaceholderFactory<IPrice, ProcessPriceCommand>
		{
		}
	}
}