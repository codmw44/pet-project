using Models.Rewards;
using Zenject;

namespace PetServer.Defenitions.Commands.Finance
{
	public class ProcessRewardFactory: IFactory<IReward, ProcessRewardCommand>
	{
		private readonly DiContainer _diContainer;

		public ProcessRewardFactory(DiContainer diContainer)
		{
			_diContainer = diContainer;
		}

		public ProcessRewardCommand Create(IReward param)
		{
			return _diContainer.Instantiate<ProcessRewardCommand>(new []{param});
		}
	}
}