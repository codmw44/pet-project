using Controllers;
using Controllers.Server;
using Models.Rewards;
using PetServer.Balancer.Models.Balance;
using PetServer.Defenitions.Commands.Infrastructure;
using PetServer.Defenitions.Models.Response;
using PetServer.Models.Auth;
using PetServer.Services;
using UnityFx.Async;
using UnityFx.Async.Extensions;
using Zenject;

namespace PetServer.Defenitions.Commands.Finance
{
	public class ProcessRewardCommand : MagicOnionCommand<Response, IFinanceService>
	{
		private readonly Container<SessionToken> _sessionToken;
		private readonly IReward _reward;

		protected override ChannelType ChannelType => ChannelType.General;

		public ProcessRewardCommand(ChannelController channelController, Container<SessionToken> sessionToken, IReward reward) : base(
			channelController)
		{
			_sessionToken = sessionToken;
			_reward = reward;
		}

		protected override IAsyncOperation<Response> Request(IFinanceService service)
		{
			return service.ProcessReward(_sessionToken.Value, _reward).ResponseAsync.ToAsync();
		}

		public class Factory : PlaceholderFactory<IReward, ProcessRewardCommand>
		{
		}
	}
}