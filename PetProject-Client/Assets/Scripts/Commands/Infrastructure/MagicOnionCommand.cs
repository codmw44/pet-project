using Controllers.Server;
using Grpc.Core;
using MagicOnion;
using MagicOnion.Client;
using PetServer.Balancer.Models.Balance;
using UnityFx.Async;
using UnityFx.Async.Promises;

namespace PetServer.Defenitions.Commands.Infrastructure
{
	public abstract class MagicOnionCommand<TResponse, TService> : ICommand<TResponse> where TService : IService<TService>
	{
		private readonly ChannelController _channelController;

		protected abstract ChannelType ChannelType { get; }

		protected MagicOnionCommand(ChannelController channelController)
		{
			_channelController = channelController;
		}

		public IAsyncOperation<TResponse> Execute()
		{
			return GetChannel().Then(GetService).Then(Request).Then(ProcessResult);
		}

		protected virtual IAsyncOperation<Channel> GetChannel()
		{
			return _channelController.GetChannel(ChannelType);
		}

		protected virtual IAsyncOperation<TService> GetService(Channel channel)
		{
			var authService = MagicOnionClient.Create<TService>(channel);
			return AsyncResult.FromResult(authService);
		}

		protected abstract IAsyncOperation<TResponse> Request(TService service);

		protected virtual IAsyncOperation<TResponse> ProcessResult(TResponse result)
		{
			return AsyncResult.FromResult(result);
		}
	}
}