using UnityFx.Async;

namespace PetServer.Defenitions.Commands.Infrastructure
{
	public interface ICommand<T>
	{
		IAsyncOperation<T> Execute();
	}
}