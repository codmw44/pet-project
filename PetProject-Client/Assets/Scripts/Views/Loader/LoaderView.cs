using System.ComponentModel;
using Doozy.Engine.Progress;
using Sirenix.OdinInspector;
using UI.Infrastructure;
using UnityEngine;

namespace Views.Loader
{
	public class LoaderView : UIWindow
	{
		[SerializeField, Required] private Progressor _progressor;

		public float AnimationDuration => _progressor.AnimationDuration;

		public void SetProgressValue(object sender, ProgressChangedEventArgs progressChangedEventArgs)
		{
			_progressor.SetProgress(progressChangedEventArgs.ProgressPercentage / 100f);
		}
	}
}