using I2.Loc;
using Models.Price;
using UI.Infrastructure;

namespace Views.Notices
{
	public class NoticeMediator
	{
		private const string NotEnoughSoftToken = "Notice/NotEnough/Soft";
		private const string NotEnoughHardToken = "Notice/NotEnough/Hard";
		private const string NotEnoughVideoToken = "Notice/NotEnough/Video";

		private readonly UIWindowContainer _windowContainer;
		private BuildingNoticeView _buildingNoticeView;
		private float _lastTimeBuildingShow;

		private NoticeView _noticeView;

		public NoticeMediator(UIWindowContainer windowContainer)
		{
			_windowContainer = windowContainer;
		}

		private NoticeView NoticeView
		{
			get
			{
				if (_noticeView == null)
				{
					_noticeView = _windowContainer.GetWindow<NoticeView>();
				}

				return _noticeView;
			}
		}

		private BuildingNoticeView BuildingNoticeView
		{
			get
			{
				if (_buildingNoticeView == null)
				{
					_buildingNoticeView = _windowContainer.GetWindow<BuildingNoticeView>();
				}

				return _buildingNoticeView;
			}
		}

		public void Notify(string notificationTitle, bool force = true)
		{
			NoticeView.ShowNotice(notificationTitle, force);
		}

		public void Notify(IPrice price)
		{
			var token = "Not enough ";
			switch (price)
			{
				case SoftCurrencyPrice _:
					token = NotEnoughSoftToken;
					break;

				case HardCurrencyPrice _:
					token = NotEnoughHardToken;
					break;

				case RewardedVideoPrice _:
					token = NotEnoughVideoToken;
					break;
			}

			NoticeView.ShowNotice(LocalizationManager.GetTranslation(token));
		}
	}
}