using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UI.Infrastructure;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityFx.Async.Promises;

namespace Views.Notices
{
	public class BuildingNoticeView : UIWindow, IPointerDownHandler
	{
		private const float MinDelayBetweenShow = 1f;

		private readonly Queue<Sprite> _queueNotification = new Queue<Sprite>();
		private readonly Queue<bool> _queueTypeNotification = new Queue<bool>();
		[SerializeField, Required]  private Color _businessColor;
		[SerializeField, Required]  private Color _dormitoryColor;
		[SerializeField, Required]  private Image _headerImage;

		[SerializeField, Required]  private Image _image;

		private float _lastTimeShow;
		[SerializeField, Required]  private TextMeshProUGUI _textTitle;

		public void OnPointerDown(PointerEventData eventData)
		{
			_queueNotification.Clear();
			_queueTypeNotification.Clear();
		}

		public void ShowNotice(Sprite payload, bool isDormitory, bool force = true)
		{
			if (force)
			{
				SetAndShow(payload, isDormitory);
				return;
			}

			_queueNotification.Enqueue(payload);
			_queueTypeNotification.Enqueue(isDormitory);
			TryToShow();
		}

		private void TryToShow()
		{
			if (_queueNotification.Count == 0 || Time.time - _lastTimeShow < MinDelayBetweenShow)
			{
				return;
			}

			_lastTimeShow = Time.time;
			var sprite = _queueNotification.Dequeue();
			var isDormitory = _queueTypeNotification.Dequeue();

			SetAndShow(sprite, isDormitory);
		}

		private void SetAndShow(Sprite payload, bool isDormitory)
		{
			Hide(true);
			_image.sprite = payload;
			_headerImage.color = isDormitory ? _dormitoryColor : _businessColor;

			Show().Then(() => Hide().Then(TryToShow));
		}

		public void SetText(string title)
		{
			_textTitle.text = title;
		}
	}
}