using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UI.Infrastructure;
using UnityEngine;
using UnityEngine.UI;

namespace Views.Notices
{
	public class ConnectionNoticeView : UIWindow
	{
		[SerializeField, Required] private Button _reconnectBtn;

		public Action OnReconnectClick;

		private void Awake()
		{
			_reconnectBtn.onClick.AddListener(() => { OnReconnectClick?.Invoke(); });
		}

		public void ShowFailedAttemptConnection()
		{
			_reconnectBtn.transform.DOPunchScale(Vector3.one * 0.1f, 0.3f);
		}

		public void SetAvailableBtn(bool isAvailable)
		{
			_reconnectBtn.interactable = isAvailable;
		}
	}
}