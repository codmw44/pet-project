using System.Collections.Generic;
using TMPro;
using UI.Infrastructure;
using UnityEngine;
using UnityFx.Async.Promises;

namespace Views.Notices
{
	public class NoticeView : UIWindow
	{
		private const float MinDelayBetweenShow = 1f;

		private readonly Queue<string> _queueNotification = new Queue<string>();

		private float _lastTimeShow;

		[SerializeField] private TextMeshProUGUI _text;

		public void ShowNotice(string payload, bool force = true)
		{
			if (force)
			{
				SetAndShow(payload);
				return;
			}

			_queueNotification.Enqueue(payload);
			TryToShow();
		}

		private void TryToShow()
		{
			if (_queueNotification.Count == 0 || Time.time - _lastTimeShow < MinDelayBetweenShow)
			{
				return;
			}

			_lastTimeShow = Time.time;
			var obj = _queueNotification.Dequeue();

			SetAndShow(obj);
		}

		private void SetAndShow(string payload)
		{
			Hide(true);
			_text.text = payload;

			Show().Then(() => Hide().Done(TryToShow));
		}
	}
}