using UnityEngine;

namespace Views
{
	[ExecuteInEditMode]
	public class ForceSetSmallScale : MonoBehaviour
	{
		[SerializeField] private float _scale;

		private void Awake()
		{
			SetLocalScale();
		}

		private void Start()
		{
			SetLocalScale();
		}

		private void Update()
		{
			SetLocalScale();
		}

		public void SetScale(float scale)
		{
			_scale = scale;
			SetLocalScale();
		}

		private void SetLocalScale()
		{
			if (!Mathf.Approximately(transform.localScale.magnitude, _scale))
			{
				transform.localScale = Vector3.one * _scale;
			}
		}
	}
}