using System.Numerics;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UI.Infrastructure;
using UnityEngine;
using Utility.Formatting;
using Vector3 = UnityEngine.Vector3;

namespace Views.Currency
{
	public abstract class CurrencyView : UIWindow
	{
		private const float Duration = 0.35f;
		[SerializeField, Required]  private Transform _icon;

		private double _settedValue;
		private BigInteger _settedValueBigInteger;

		[SerializeField, Required]  private TextMeshProUGUI _text;
		private Tween _tween;

		public void SetValue(int value)
		{
			if (_settedValue < value)
			{
				PlayAnimation();
			}

			_settedValue = value;

			_text.text = value.ToShortString();
		}

		private void PlayAnimation()
		{
			_tween?.Kill();
			_icon.localScale = Vector3.one;
			_tween = _icon.DOPunchScale(Vector3.one * 0.1f, Duration);
		}
	}
}