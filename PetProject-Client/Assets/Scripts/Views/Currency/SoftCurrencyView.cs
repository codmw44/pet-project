using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace Views.Currency
{
	public class SoftCurrencyView : CurrencyView
	{
		[SerializeField, Required]  private Button _plusBtn;

		public event Action OnClickPlus;

		private void Awake()
		{
			_plusBtn.onClick.AddListener(() => OnClickPlus?.Invoke());
		}
	}
}