﻿using UnityEngine;
using System;
using Sirenix.OdinInspector;

namespace Crystal
{
	public class SafeAreaTest : MonoBehaviour
	{
		SafeArea.SimDevice[] Sims;
		int SimIdx;

		void Awake()
		{
			Sims = (SafeArea.SimDevice[])Enum.GetValues(typeof(SafeArea.SimDevice));
		}

		[Button]
		void SimulateSafeArea()
		{
			SimIdx++;

			if (SimIdx >= Sims.Length)
			{
				SimIdx = 0;
			}

			SafeArea.Sim = Sims[SimIdx];
			Debug.LogFormat("Switched to sim device {0}", Sims[SimIdx]);
		}
	}
}