version: 2
importPivots: 1
tpsheetFileNames:
- Assets/Textures/Boosters/Atlas_Prefects.tpsheet
- Assets/Textures/Boosters/Atlas_Prefects_2.tpsheet
- Assets/Textures/Cars/Atlas_Cars.tpsheet
- Assets/Textures/City/Base/QuaterTiles.tpsheet
- Assets/Textures/City/Base/RoadTiles.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Admin.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Beach1.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Beach2.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame1.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame10.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame11.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame12.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame13.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame14.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame15.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame16.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame2.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame3.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame4.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame5.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame6.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame7.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame8.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame9.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_InApp1.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_InApp2.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_InApp3.tpsheet
- Assets/Textures/City/Buildings/Atlas_Buildings_Soft_And_Fame.tpsheet
- Assets/Textures/City/Environments/Atlas_Buildings_Beach_Props1.tpsheet
- Assets/Textures/City/Environments/Atlas_Buildings_Beach_Props2.tpsheet
- Assets/Textures/City/Environments/Atlas_Environment_1.tpsheet
- Assets/Textures/City/Environments/Atlas_Environment_2.tpsheet
- Assets/Textures/City/Environments/Atlas_Environment_3.tpsheet
- Assets/Textures/City/Environments/Atlas_Environment_4.tpsheet
- Assets/Textures/UI/Atlas_Building_Size_Icons.tpsheet
- Assets/Textures/UI/Atlas_HeaderImgs.tpsheet
- Assets/Textures/UI/Atlas_Shop_1.tpsheet
- Assets/Textures/UI/Atlas_Shop_2.tpsheet
- Assets/Textures/UI/Atlas_UI.tpsheet
- Assets/Textures/UI/Atlas_UI_Normal.tpsheet
textureFileNames:
- Assets/Textures/Boosters/Atlas_Prefects.png
- Assets/Textures/Boosters/Atlas_Prefects_2.png
- Assets/Textures/Cars/Atlas_Cars.png
- Assets/Textures/City/Base/QuaterTiles.png
- Assets/Textures/City/Base/RoadTiles.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Admin.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Beach1.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Beach2.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame1.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame10.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame11.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame12.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame13.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame14.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame15.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame16.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame2.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame3.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame4.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame5.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame6.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame7.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame8.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Fame9.png
- Assets/Textures/City/Buildings/Atlas_Buildings_InApp1.png
- Assets/Textures/City/Buildings/Atlas_Buildings_InApp2.png
- Assets/Textures/City/Buildings/Atlas_Buildings_InApp3.png
- Assets/Textures/City/Buildings/Atlas_Buildings_Soft_And_Fame.png
- Assets/Textures/City/Environments/Atlas_Buildings_Beach_Props1.png
- Assets/Textures/City/Environments/Atlas_Buildings_Beach_Props2.png
- Assets/Textures/City/Environments/Atlas_Environment_1.png
- Assets/Textures/City/Environments/Atlas_Environment_2.png
- Assets/Textures/City/Environments/Atlas_Environment_3.png
- Assets/Textures/City/Environments/Atlas_Environment_4.png
- Assets/Textures/UI/Atlas_Building_Size_Icons.png
- Assets/Textures/UI/Atlas_HeaderImgs.png
- Assets/Textures/UI/Atlas_Shop_1.png
- Assets/Textures/UI/Atlas_Shop_2.png
- Assets/Textures/UI/Atlas_UI.png
- Assets/Textures/UI/Atlas_UI_Normal.png
normalmapFileNames:
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
- 
enableDebugOutput: 0
