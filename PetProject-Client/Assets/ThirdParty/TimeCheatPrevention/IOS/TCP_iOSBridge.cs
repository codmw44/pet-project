﻿using System.Collections;
using UnityEngine;
using System.Runtime.InteropServices;


public class TCP_iOSBridge
{
#if UNITY_IOS
	[DllImport("__Internal")]
	private static extern long GetElapsedTimeSinceLastBoot();
#endif


	//////////////////////////////////////////////////////////////////////////

	/// <summary>
	/// Check whether VoiceOver is actively running on this device
	/// </summary>
	/// <returns>true if VoiceOver was detected, false otherwise</returns>
	static public long GetTimeInSecSinceLastBoot()
	{
#if UNITY_IOS && !UNITY_EDITOR
		return GetElapsedTimeSinceLastBoot();
#endif
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////

}
