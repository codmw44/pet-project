﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityFx.Async;

public class TimeCheatPrevention : MonoBehaviour
{
	private const string URLTimespan = "http://159.253.20.186/";

	[Header("Debug")]

	// Debug Output
	public bool m_DebugOutput;

	//////////////////////////////////////////////////////////////////////////

	[Header("Callback")]
	[Tooltip("When it is impossible to get a time estimate or a server time, the plugin " +
		"will retry getting a Internet based time this many seconds before triggering the " + "callback with the device time instead.")]
	[Range(1.0f, 10.0f)]
	public float m_CallbackTimeout = 3.0f;

	/// <summary>
	/// Callback that triggers as soon as there is either a server-based time,
	/// or a cheat-free estimate. If neither is available, the callback will trigger
	/// after a time threshold with regardless. This ensures that the callback will be called
	/// regardless.
	/// Register with:
	/// TimeCheatPrevention.OnCheatFreeTimeAvailable.AddListener(<YourFunctionName>);
	/// and unregister when you've received the callback.
	/// </summary>
	static public Action OnCheatFreeTimeAvailable = () => { };

	[Tooltip("Callback that triggers as soon as there is either a server-based time, " +
		"or a cheat-free estimate. If neither is available, the callback will trigger after a time " +
		"threshold with regardless. This ensures that the callback will be called regardless")]
	public UnityEvent m_OnCheatFreeTimeAvailable = new UnityEvent();

	// Internal helpers for callback triggering
	//////////////////////////////////////////////////////////////////////////
	//! Triggers the callback(s) next frame - this delay allows other scripts to subscribe to the event at game start
	private bool m_CallbackNeedsTriggering;

	//! TimeOut for when the callback should be triggered regardless
	private float m_CallbackTriggerTimeOut = -1.0f;

	//////////////////////////////////////////////////////////////////////////

	// Singleton instance, private, because all access functions are static
	private static TimeCheatPrevention Instance => _instance;

	static private TimeCheatPrevention _instance;

	// Server Time
	private DateTime m_ServerTimeStampUTC;
	private float m_ServerTimeLastUpdated = -1.0f; // How many seconds ago did we last receive a valid server time stamp
	private float m_ServerTimeRetryTimer = -1.0f;
	private float m_ServerTimeRetryTimerNext = 1.0f;
	private bool m_ServerTimeRequestRunning; //! Server request sent, waiting for response

	// Device Uptime
	private DateTime m_LastConfirmedTimeStampUTC;

	private float
		m_DeviceUpTimeDelta =
			-1.0f; // This is the time passed since the last confirmed time stamp, calculated from the delta between the current and saved device uptime counters

	private bool m_UsingDeviceTime = true;

	private bool m_SkipFrame = true;

	private AsyncCompletionSource _initAsyncResult;

	public IAsyncOperation Initialize()
	{
		_initAsyncResult = new AsyncCompletionSource();
		InitializeTimeCheatPrevention();
		return _initAsyncResult;
	}

	//////////////////////////////////////////////////////////////////////////
	private void Awake()
	{
		// Singleton - one of these is enough
		if (Instance != null && Instance != this)
		{
			DestroyImmediate(gameObject);
			return;
		}

		_instance = this;
		DontDestroyOnLoad(gameObject);

		// InitializeTimeCheatPrevention();
	}

	//////////////////////////////////////////////////////////////////////////

	private void InitializeTimeCheatPrevention()
	{
		// Initialize quickly, in case the time is queried before estimation is complete
		m_ServerTimeStampUTC = DateTime.UtcNow;

		RefreshCheatFreeTime();
	}

	//////////////////////////////////////////////////////////////////////////

	private void OnApplicationFocus(bool focus)
	{
		if (focus)
		{
			// Refresh times (since we haven't been able to update it)
			Log("Refreshing cheat free time.");

			// Invalidate server time 
			m_ServerTimeLastUpdated = -1.0f;
			m_ServerTimeStampUTC = DateTime.UtcNow;

			// This will also retrigger the callback
			RefreshCheatFreeTime();
		}
	}

	//////////////////////////////////////////////////////////////////////////

	/// <summary>
	/// Updates both the locally estimated time and the server based time, and will trigger
	/// the callback once it has a cheat-free time (or the timeout is reached)
	/// </summary>
	private void RefreshCheatFreeTime()
	{
		// Start the timeout clock for the callback
		m_CallbackTriggerTimeOut = m_CallbackTimeout;

		// Load last valid time stamp and estimate from there
		UpdateEstimatedTime();

		// Start Query to server for Internet based time
		UpdateServerTime();

		// Skip one frame during interpolation
		m_SkipFrame = true;
	}

	//////////////////////////////////////////////////////////////////////////

	/// <summary>
	/// This function will try to calculate a cheat-free time without using the Internet.
	/// If successful, it will flag to trigger the callback next frame. If a reliable time
	/// cannot be estimated, the callback will not be triggered and instead wait for either
	/// an Internet-based time to become available, or for the timeout to be reached.
	/// </summary>
	private void UpdateEstimatedTime()
	{
		// Check if there is a chance for Internet. 
		// Note that just because a network appears reachable, it doesn't
		// mean that there actually is a valid connection. It could be that there
		// is a WiFi which requires a login, but auto-connects the device right away (leading 
		// any web requests to a landing page).
		// However, if there is definitely NO Internet reported, then we can fire the 
		// callback right after the estimation is complete, since there's no point in waiting.
		bool weAreOffline = (Application.internetReachability == NetworkReachability.NotReachable);

		// If there is no previously save time stamp - we have to initialize on first app start
		// The same is true if this platform doesn't support time estimation
		if (!IsPlatformTimeEstimationSupported())
		{
			Log("This platform doesn't support time estimation (only Android and iOS) - using device time instead");
			m_UsingDeviceTime = true;
			m_LastConfirmedTimeStampUTC = DateTime.UtcNow;
			m_DeviceUpTimeDelta = 0;

			// If there's no chance for a server based time, let's fire the callback right away
			if (weAreOffline)
				m_CallbackNeedsTriggering = true;
		}
		else if (!PlayerPrefs.HasKey("OS_UnbiasedTimeStamp"))
		{
			// Once, initialize with the current device time. 
			// Will be overridden as soon as we have an Internet based time.
			Log("No previously confirmed & saved time stamp. Initializing with system time.");
			UpdateEstimationTimeStamp(DateTime.UtcNow);
			m_UsingDeviceTime = true;

			// If there's no chance for a server based time, let's fire the callback right away
			if (weAreOffline)
				m_CallbackNeedsTriggering = true;
		}
		else
		{
			// Load estimated time
			string timeStamp = PlayerPrefs.GetString("OS_UnbiasedTimeStamp", "");
			m_LastConfirmedTimeStampUTC = DateTime.Parse(timeStamp);
			int prevUptime = PlayerPrefs.GetInt("OS_UnbiasedDeviceUpTime", 0);
			int currentUptime = (int)GetElapsedTimeSinceLastBoot();
			if (currentUptime < prevUptime)
			{
				// Device was rebooted - we need to re-init (which means trusting the system time)
				Log("Device was rebooted. Re-initializing with system time.");
				UpdateEstimationTimeStamp(DateTime.UtcNow);
				m_UsingDeviceTime = true;

				// If there's no chance for a server based time, let's fire the callback right away
				if (weAreOffline)
					m_CallbackNeedsTriggering = true;
			}
			else
			{
				m_DeviceUpTimeDelta = currentUptime - prevUptime;
				Log("Loading estimated offline timer. Seconds since boot: " + currentUptime.ToString("N0") + " before " + prevUptime.ToString("N0") +
					". Last valid time stamp: " + m_LastConfirmedTimeStampUTC + " making it now: " +
					m_LastConfirmedTimeStampUTC.AddSeconds(m_DeviceUpTimeDelta));
				m_UsingDeviceTime = false;

				// We have a cheat-free estimated time, so mark the callback for triggering
				// If there's the chance of an Internet connection, instead of triggering right 
				// away next frame, delay the trigger for 1.5 seconds,
				// to give the Internet time a chance to be received. 
				// Server based timing will always be more accurate.
				if (weAreOffline)
					m_CallbackNeedsTriggering = true;
				else if (m_CallbackTriggerTimeOut > 1.5f)
					m_CallbackTriggerTimeOut = 1.5f;
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////

	/// <summary>
	/// Saves a time stamp of the last confirmed time (usually Internet time), this is used as 
	/// a base time for calculation if there is no Internet time available in the future.
	/// </summary>
	/// <param name="dt"></param>
	private void UpdateEstimationTimeStamp(DateTime dt)
	{
		m_LastConfirmedTimeStampUTC = dt;
		m_DeviceUpTimeDelta = 0;

		// No need to save estimation data if this platform doesn't support it
		if (!IsPlatformTimeEstimationSupported())
			return;

		int elapsedSecondsSinceBoot = (int)GetElapsedTimeSinceLastBoot();
		PlayerPrefs.SetString("OS_UnbiasedTimeStamp", dt.ToString());
		PlayerPrefs.SetInt("OS_UnbiasedDeviceUpTime", elapsedSecondsSinceBoot);

		Log("Saving unbiased time stamp for time estimation. Now: " + m_LastConfirmedTimeStampUTC + " Seconds since last boot: " +
			elapsedSecondsSinceBoot.ToString("N0"));
	}

	//////////////////////////////////////////////////////////////////////////

	/// <summary>
	/// Helper function to get the number of seconds that have elapsed since the device was last booted.
	/// Only supported on Android and iOS. This value is used to estimate the time when there is no Internet connection.
	/// </summary>
	/// <returns></returns>
	private long GetElapsedTimeSinceLastBoot()
	{
		if (!IsPlatformTimeEstimationSupported())
		{
			// Since we don't have a device that we can use for an estimate, let's use local time
			if (Instance != null)
			{
				Log("Unsupported platform, we're using device time");
				long timeDifference = (long)DateTime.UtcNow.Subtract(Instance.m_LastConfirmedTimeStampUTC).TotalSeconds;
				return timeDifference;
			}

			return 0;
		}

		if (Application.platform == RuntimePlatform.Android)
		{
			AndroidJavaClass systemClock = new AndroidJavaClass("android.os.SystemClock");
			return (systemClock.CallStatic<long>("elapsedRealtime")) / 1000; // because this function returns milliseconds
		}

		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			return TCP_iOSBridge.GetTimeInSecSinceLastBoot();
		}

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////

	/// <summary>
	/// Starts a new attempt to get the correct time from the Internet
	/// </summary>
	private void UpdateServerTime()
	{
		// Don't start another web request if we're still waiting to hear back form the last one
		if (m_ServerTimeRequestRunning)
			return;
		m_ServerTimeRequestRunning = true;

		// Use an increasing amount of wait time in between requests (up to a max wait time value)
		m_ServerTimeRetryTimer = m_ServerTimeRetryTimerNext;
		m_ServerTimeRetryTimerNext += 1.0f;
		if (m_ServerTimeRetryTimerNext > 10.0f) // max wait time: 10 seconds
			m_ServerTimeRetryTimerNext = 10.0f;

		// Get time from server (this takes a moment)
		StartCoroutine("GetServerTime");
	}

	IEnumerator GetServerTime()
	{
		UnityWebRequest www = UnityWebRequest.Get(URLTimespan);
		// Set timeout to three seconds
		www.timeout = 3;

		// Wait until the request completed
#if UNITY_2017_2_OR_NEWER
		// Send() was deprecated with Unity 2017.2
		var operation = www.SendWebRequest();
		while (!operation.isDone)
		{
			_initAsyncResult?.SetProgress(www.downloadProgress);
			yield return null;
		}
#else
		yield return www.Send();
#endif

#if UNITY_2017_2_OR_NEWER
		if (www.isNetworkError || www.isHttpError)
#else
		if (www.isError)
#endif
		{
			// Happens for example if we're not online, no server connection etc...
			// The code will automatically try again
			Log(www.error);
		}
		else
		{
			var response = www.downloadHandler.text;
			try
			{
				double timestamp = Convert.ToInt64(response);
				if (timestamp < 1000)
				{
					// Invalid result
					m_ServerTimeRequestRunning = false;
					yield break;
				}

				DateTime serverDateTimeUTC = new DateTime(1970, 1, 1).AddSeconds(timestamp);

				// Update local server timestamp and reset time offset, so it can be interpolated locally
				m_ServerTimeStampUTC = serverDateTimeUTC;
				m_ServerTimeLastUpdated = 0.0f;

				Log("Received server time: " + m_ServerTimeStampUTC);

				// Since we have a confirmed time now, update unbiased calculation locally
				UpdateEstimationTimeStamp(m_ServerTimeStampUTC);
				m_UsingDeviceTime = false;

				// Reset waiting timers 
				m_ServerTimeRetryTimerNext = 1.0f;
				m_ServerTimeRetryTimer = 1.0f;

				// Mark callback for triggering next frame
				m_CallbackNeedsTriggering = true;
			}
			catch (Exception e)
			{
				//ASK PLAYQUANTUM ABOUT SERVER STATE!

				Log("Invalid/No data received from server");
			}
		}

		m_ServerTimeRequestRunning = false;
	}

	//////////////////////////////////////////////////////////////////////////

	void Update()
	{
		// Handle Callback
		if (m_CallbackNeedsTriggering)
			TriggerOnCheatFreeTimeAvailableCallback();

		// Handle frame skipping needed for estimation
		if (m_SkipFrame)
		{
			m_SkipFrame = false;
			return;
		}

		// Callback timeout
		if (m_CallbackTriggerTimeOut > 0.0f)
		{
			m_CallbackTriggerTimeOut -= Time.unscaledDeltaTime;
			if (m_CallbackTriggerTimeOut <= 0.0f)
				TriggerOnCheatFreeTimeAvailableCallback();
		}

		// Local estimation is always updated
		m_DeviceUpTimeDelta += Time.unscaledDeltaTime;

		// Server time is only updated if we actually have a server time
		if (m_ServerTimeLastUpdated >= 0)
		{
			// Server timestamp offset
			m_ServerTimeLastUpdated += Time.unscaledDeltaTime;
		}
		else
		{
			// Regularly try to contact time server until we get a server time
			m_ServerTimeRetryTimer -= Time.unscaledDeltaTime;

			if (m_ServerTimeRetryTimer <= 0.0f && !m_ServerTimeRequestRunning)
				UpdateServerTime();
		}
	}

	//////////////////////////////////////////////////////////////////////////

	/// <summary>
	/// Calls back all registered listeners to tell them it is now safe to use GetDateTimeUTC.
	/// Listeners can register either using the Prefab in the scene or by calling
	/// TimeCheatPrevention.OnCheatFreeTimeAvailable.AddListener(<YourFunctionName>);
	/// You must unregister after the callack, or you might receive the callback multiple times.
	/// </summary>
	private void TriggerOnCheatFreeTimeAvailableCallback()
	{
		m_CallbackNeedsTriggering = false;
		m_CallbackTriggerTimeOut = -1.0f;
		m_OnCheatFreeTimeAvailable.Invoke();
		OnCheatFreeTimeAvailable();

		if (_initAsyncResult == null)
		{
			return;
		}

		_initAsyncResult.SetCompleted();
		_initAsyncResult = null;
	}

	//////////////////////////////////////////////////////////////////////////

	static private void Log(string message)
	{
		if (Instance == null || !Instance.m_DebugOutput)
			return;

		Debug.Log("[Time Cheat Prevention] " + message);
	}

	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

	/// <summary>
	/// Returns the time int UTC, either from server or locally estimated
	/// </summary>
	/// <returns></returns>
	public DateTime GetDateTimeUTC()
	{
#if UNITY_EDITOR
		return DateTime.UtcNow;
#endif
		if (Instance.m_ServerTimeLastUpdated < 0)
		{
			DateTime currentEstimate = Instance.m_LastConfirmedTimeStampUTC;
			if (Instance.m_DeviceUpTimeDelta > 0)
				currentEstimate = currentEstimate.AddSeconds(Instance.m_DeviceUpTimeDelta);

			return currentEstimate;
		}

		DateTime currentTime = Instance.m_ServerTimeStampUTC;
		currentTime = currentTime.AddSeconds(Instance.m_ServerTimeLastUpdated);

		return currentTime;
	}

	//////////////////////////////////////////////////////////////////////////

	/// <summary>
	/// Returns whether or not GetDateTimeUTC would return a server based time, or just an estimate.
	/// Note that you can call GetDateTimeUTC regardless, but if this returns false and there is an
	/// active Internet connection, maybe try waiting for a sec?
	/// </summary>
	static public bool HasServerTime()
	{
		if (Instance == null)
			return false;

		return (Instance.m_ServerTimeLastUpdated >= 0);
	}

	/// <summary>
	/// Returns true if there is no server time and the local time couldn't be estimated, probably due to a device reboot.
	/// Time might have been tampered with, cheat-free cannot be guaranteed.
	/// </summary>
	static public bool IsUsingLocalDeviceTime()
	{
		if (Instance == null)
			return true;

		if (HasServerTime())
			return false;

		return Instance.m_UsingDeviceTime;
	}

	/// <summary>
	/// Returns true if there is no time from the Internet, but we have a local, cheat-free estimate.
	/// </summary>
	/// <returns></returns>
	static public bool IsUsingEstimatedTime()
	{
		return !IsUsingLocalDeviceTime() && !HasServerTime() && IsPlatformTimeEstimationSupported();
	}

	//////////////////////////////////////////////////////////////////////////

	static public bool IsPlatformTimeEstimationSupported()
	{
#if UNITY_EDITOR
		return false;
#else
		if (Application.platform == RuntimePlatform.Android
			|| Application.platform == RuntimePlatform.IPhonePlayer)
			return true;

		return false;
#endif
	}

	//////////////////////////////////////////////////////////////////////////
	// Time Stamp Convenience functions
	//////////////////////////////////////////////////////////////////////////

	/// <summary>
	/// Returns the current timezone independent date/time as a string, which can be saved
	/// </summary>
	public string GetTimeStamp()
	{
		if (IsUsingLocalDeviceTime())
			Log("Time Stamp is using device time. It might not be cheat-free.");

		DateTime now = GetDateTimeUTC();
		string timeStamp = now.ToString();

		Log("Creating timestamp: " + timeStamp);

		return timeStamp;
	}

	/// <summary>
	/// Reads the provided timestamp string and converts it back into a date/time.
	/// </summary>
	public DateTime ParseTimeStamp(ref string timeStamp)
	{
		if (timeStamp == null || timeStamp.Length == 0)
		{
			Debug.LogError("[Time Cheat Prevention] Invalid TimeStamp provided (length = 0). Returning current date/time");
			return GetDateTimeUTC();
		}

		DateTime parsedDateTime = new DateTime();
		if (!DateTime.TryParse(timeStamp, out parsedDateTime))
		{
			Debug.LogError("[Time Cheat Prevention] Provided TimeStamp '" + timeStamp + "' cannot be parsed. Returning current date/time");
			return GetDateTimeUTC();
		}

		return parsedDateTime;
	}

	/// <summary>
	/// Returns the number of seconds since the given time stamp, capped to a maximum time span if desired.
	/// capToMaxTime - maximum number of seconds to return (for example 24 * 60 * 60 for 24 hours)
	/// capToMaxTime = -1 will be ignored, returned value is uncapped
	/// </summary>
	public double GetSecondsSinceTimeStamp(ref string timeStamp, int capToMaxTime = -1)
	{
		DateTime timeStampDate = ParseTimeStamp(ref timeStamp);
		TimeSpan deltaTime = GetDateTimeUTC().Subtract(timeStampDate);

		// If more time has passed than we're interested in, cap the value
		if (capToMaxTime > 0 && deltaTime.TotalSeconds >= capToMaxTime)
			return capToMaxTime;

		return deltaTime.TotalSeconds;
	}
}